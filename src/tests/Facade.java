package tests;

import tests.gestorfrotas.menuresponsivo.atendimento.AtendimentoFaleConosco;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroAbastecimento;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroAcidentes;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroAgendamento;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroAjudantesMonitores;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroAreaNaoPermitida;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroCercas;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroChecklist;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroMotorista;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroMultas;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroPontoInteresse;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroRotas;
import tests.gestorfrotas.menuresponsivo.cadastros.CadastroVeiculos;
import tests.gestorfrotas.menuresponsivo.configuracoes.ConfiguracaoAgendamentoRelatorios;
import tests.gestorfrotas.menuresponsivo.configuracoes.ConfiguracaoNotificacoes;
import tests.gestorfrotas.menuresponsivo.configuracoes.ConfiguracaoNotificacoesContatos;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.cercas.EventoCerca;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.cercas.EventoTMS;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.cercas.EventoViolacaoCerca;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.eventosbruscos.EventoAceleracaoBrusca;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.eventosbruscos.EventoCurvaBrusca;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.eventosbruscos.EventoFreadaBrusca;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalAcionamentoPortas;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalComandos;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalCoordenadas;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalEspelhamento;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalHistorico;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalHodometro;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalPerfilMotorista;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalQuantidadeTransmissoes;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalRankingMotoristas;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalSensores;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalStatusAtual;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalTacografo;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalTransmissaoAnalitica;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalVeiculosAtualizados;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico.OperacionalViolacaoBateria;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.jornada.OperacionalDesempenhoExpediente;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.jornada.OperacionalDesempenhoOperacional;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.jornada.OperacionalDetalhamentoMotorista;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.jornada.OperacionalDiarioMotorista;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.jornada.OperacionalJornadaTrabalho;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.jornada.OperacionalPontoMotorista;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.jornada.OperacionalResumoJornada;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.movimentacoes.OperacionalBDE;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.movimentacoes.OperacionalKmRodados;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.movimentacoes.OperacionalVelocidadesExcedidas;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.movimentacoes.OperacionalViagem;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.movimentacoes.OperacionalVisita;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.tempo.OperacionalTempoLigado;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.tempo.OperacionalTempoOcioso;
import tests.gestorfrotas.menuresponsivo.relatorios.operacional.tempo.OperacionalTempoParado;
import tests.gestorfrotas.menuresponsivo.relatorios.pontointeresse.PontoInteresseAvancado;
import tests.gestorfrotas.menuresponsivo.relatorios.pontointeresse.PontoInteresseBasico;
import tests.gestorfrotas.menuresponsivo.relatorios.pontointeresse.PontoInteresseRaioInteresse;
import tests.gestorfrotas.menuresponsivo.relatorios.pontointeresse.PontoInteresseTempoPermanencia;
import tests.gestorfrotas.menuresponsivo.relatorios.rotograma.Rotograma;
import tests.gestorfrotas.menuresponsivo.relatorios.rotograma.RotogramaViolacao;
import tests.gestorfrotas.menuresponsivo.relatorios.saver.FrotasEventosOcorridos;
import tests.gestorfrotas.menuresponsivo.relatorios.sistema.SistemaAcessos;
import tests.gestorfrotas.menuresponsivo.relatorios.sistema.SistemaDisponibilidade;
import tests.gestorfrotas.menuresponsivo.relatorios.sistema.SistemaIndisponibilidadeVeiculo;
import tests.gestorfrotas.menuresponsivo.relatorios.sistema.SistemaSituacaoVeiculo;
import tests.gestorfrotas.menuresponsivo.relatorios.sistema.SistemaTickets;
import tests.gestorfrotas.menuusuario.UsuarioAdministrar;
import tests.gestorfrotas.menuusuario.UsuarioAdministrarGrupos;
import tests.gestorfrotas.menuusuario.UsuarioPerfil;
import tests.gestorpetrobras.menuresponsivo.cadastros.PetrobrasCadastroMultas;
import tests.gestorsafety.menuresponsivo.relatorios.operacional.SafetyViolacaoDeBateria;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.EventoDetalhado;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.EventoIgnicao;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.EventoMovimentacaoIndevida;
import tests.gestorfrotas.menuresponsivo.relatorios.eventos.EventoTemperatura;
import tests.gestorfrotas.menuresponsivo.relatorios.rotas.MapaRota;

public class Facade {
	private static Facade instance;
	private  EventoDetalhado ed;
	private  EventoTemperatura et;
	private  EventoIgnicao ei;
	private  EventoMovimentacaoIndevida emi;
	private  EventoCerca ec;
	private  EventoTMS tms;
	private  EventoViolacaoCerca evc;
	private  EventoAceleracaoBrusca eab;
	private  EventoFreadaBrusca efb;
	private  EventoCurvaBrusca ecb;
	
	private  OperacionalAcionamentoPortas oap;
	private  OperacionalComandos oc;
	private  OperacionalCoordenadas ocoo;
	private  OperacionalEspelhamento oe;
	private  OperacionalHistorico oh;
	private  OperacionalHodometro oho;
	private  OperacionalPerfilMotorista opm;
	private  OperacionalQuantidadeTransmissoes oqt;
	private  OperacionalRankingMotoristas orm;
	private  OperacionalSensores os;
	private  OperacionalStatusAtual osa;
	private  OperacionalTacografo ot;
	private  OperacionalTransmissaoAnalitica ota;
	private  OperacionalVeiculosAtualizados ova;
	private  OperacionalViolacaoBateria ovb;
	private  OperacionalDesempenhoOperacional odo;
	private  OperacionalDesempenhoExpediente ode;
	private  OperacionalDetalhamentoMotorista odm;
	private  OperacionalJornadaTrabalho ojt;
	private  OperacionalPontoMotorista opom;
	private  OperacionalResumoJornada orj;
	private  OperacionalDiarioMotorista odim;
	private  OperacionalBDE obde;
	private  OperacionalVelocidadesExcedidas ove;
	private  OperacionalKmRodados okr;
	private  OperacionalViagem ov;
	private  OperacionalVisita ovis;
	private  OperacionalTempoLigado otl;
	private  OperacionalTempoOcioso oto;
	private  OperacionalTempoParado otp;

	private  PontoInteresseAvancado pia;
	private  PontoInteresseBasico pib;
	private  PontoInteresseRaioInteresse piri;
	private  PontoInteresseTempoPermanencia pitp;

	private  MapaRota mr;
	
	private  Rotograma r;
	private  RotogramaViolacao rv;
	
	private  FrotasEventosOcorridos feo;

	private  SistemaAcessos sa;
	private  SistemaDisponibilidade sd;
	private  SistemaIndisponibilidadeVeiculo siv;
	private  SistemaSituacaoVeiculo ssv;
	private  SistemaTickets st;
	
	private  CadastroAbastecimento ca;
	private  CadastroAcidentes cac;
	private  CadastroAjudantesMonitores cam;
	private  CadastroCercas cc;
	private  CadastroRotas cr;
	private  CadastroPontoInteresse cpi;
	private  CadastroAreaNaoPermitida canp;
	private  CadastroChecklist cch;
	private  CadastroAgendamento cag;
	private  CadastroMotorista cm;
	private  CadastroMultas cmu;
	private  CadastroVeiculos cv;
	
	private  ConfiguracaoAgendamentoRelatorios coar;
	private  ConfiguracaoNotificacoes con;
	private  ConfiguracaoNotificacoesContatos conc;
	
	private  AtendimentoFaleConosco afc;
	
	private  UsuarioPerfil up;
	private  UsuarioAdministrar ua;
	private  UsuarioAdministrarGrupos uag;
	
	private SafetyViolacaoDeBateria svdb;
	
	private PetrobrasCadastroMultas pcm;

	public  EventoCerca getEc() {
		return ec;
	}

	public  void setEc(EventoCerca ec) {
		this.ec = ec;
	}
	
	public  EventoDetalhado getEd() {
		return ed;
	}

	public  void setEd(EventoDetalhado ed) {
		this.ed = ed;
	}

	public  EventoTemperatura getEt() {
		return et;
	}

	public  void setEt(EventoTemperatura et) {
		this.et = et;
	}

	public  EventoIgnicao getEi() {
		return ei;
	}

	public  void setEi(EventoIgnicao ei) {
		this.ei = ei;
	}

	public  EventoMovimentacaoIndevida getEmi() {
		return emi;
	}

	public  void setEmi(EventoMovimentacaoIndevida emi) {
		this.emi = emi;
	}

	public  EventoTMS getTms() {
		return tms;
	}

	public  void setTms(EventoTMS tms) {
		this.tms = tms;
	}

	public  EventoViolacaoCerca getEvc() {
		return evc;
	}

	public  void setEvc(EventoViolacaoCerca evc) {
		this.evc = evc;
	}

	public  EventoAceleracaoBrusca getEab() {
		return eab;
	}

	public  void setEab(EventoAceleracaoBrusca eab) {
		this.eab = eab;
	}

	public  EventoFreadaBrusca getEfb() {
		return efb;
	}

	public  void setEfb(EventoFreadaBrusca efb) {
		this.efb = efb;
	}

	public  EventoCurvaBrusca getEcb() {
		return ecb;
	}

	public  void setEcb(EventoCurvaBrusca ecb) {
		this.ecb = ecb;
	}

	public  OperacionalAcionamentoPortas getOap() {
		return oap;
	}

	public  void setOap(OperacionalAcionamentoPortas oap) {
		this.oap = oap;
	}

	public  OperacionalComandos getOc() {
		return oc;
	}

	public  void setOc(OperacionalComandos oc) {
		this.oc = oc;
	}

	public  OperacionalCoordenadas getOcoo() {
		return ocoo;
	}

	public  void setOcoo(OperacionalCoordenadas ocoo) {
		this.ocoo = ocoo;
	}

	public  OperacionalEspelhamento getOe() {
		return oe;
	}

	public  void setOe(OperacionalEspelhamento oe) {
		this.oe = oe;
	}

	public  OperacionalHistorico getOh() {
		return oh;
	}

	public  void setOh(OperacionalHistorico oh) {
		this.oh = oh;
	}

	public  OperacionalHodometro getOho() {
		return oho;
	}

	public  void setOho(OperacionalHodometro oho) {
		this.oho = oho;
	}

	public  OperacionalPerfilMotorista getOpm() {
		return opm;
	}

	public  void setOpm(OperacionalPerfilMotorista opm) {
		this.opm = opm;
	}

	public  OperacionalQuantidadeTransmissoes getOqt() {
		return oqt;
	}

	public  void setOqt(OperacionalQuantidadeTransmissoes oqt) {
		this.oqt = oqt;
	}

	public  OperacionalRankingMotoristas getOrm() {
		return orm;
	}

	public  void setOrm(OperacionalRankingMotoristas orm) {
		this.orm = orm;
	}

	public  OperacionalSensores getOs() {
		return os;
	}

	public  void setOs(OperacionalSensores os) {
		this.os = os;
	}

	public  OperacionalStatusAtual getOsa() {
		return osa;
	}

	public  void setOsa(OperacionalStatusAtual osa) {
		this.osa = osa;
	}

	public  OperacionalTacografo getOt() {
		return ot;
	}

	public  void setOt(OperacionalTacografo ot) {
		this.ot = ot;
	}

	public  OperacionalTransmissaoAnalitica getOta() {
		return ota;
	}

	public  void setOta(OperacionalTransmissaoAnalitica ota) {
		this.ota = ota;
	}

	public  OperacionalVeiculosAtualizados getOva() {
		return ova;
	}

	public  void setOva(OperacionalVeiculosAtualizados ova) {
		this.ova = ova;
	}

	public  OperacionalViolacaoBateria getOvb() {
		return ovb;
	}

	public  void setOvb(OperacionalViolacaoBateria ovb) {
		this.ovb = ovb;
	}

	public  OperacionalDesempenhoOperacional getOdo() {
		return odo;
	}

	public  void setOdo(OperacionalDesempenhoOperacional odo) {
		this.odo = odo;
	}

	public  OperacionalDesempenhoExpediente getOde() {
		return ode;
	}

	public  void setOde(OperacionalDesempenhoExpediente ode) {
		this.ode = ode;
	}

	public  OperacionalDetalhamentoMotorista getOdm() {
		return odm;
	}

	public  void setOdm(OperacionalDetalhamentoMotorista odm) {
		this.odm = odm;
	}

	public  OperacionalJornadaTrabalho getOjt() {
		return ojt;
	}

	public  void setOjt(OperacionalJornadaTrabalho ojt) {
		this.ojt = ojt;
	}

	public  OperacionalPontoMotorista getOpom() {
		return opom;
	}

	public  void setOpom(OperacionalPontoMotorista opom) {
		this.opom = opom;
	}

	public  OperacionalBDE getObde() {
		return obde;
	}

	public  void setObde(OperacionalBDE obde) {
		this.obde = obde;
	}

	public  OperacionalVelocidadesExcedidas getOve() {
		return ove;
	}

	public  void setOve(OperacionalVelocidadesExcedidas ove) {
		this.ove = ove;
	}

	public  OperacionalKmRodados getOkr() {
		return okr;
	}

	public  void setOkr(OperacionalKmRodados okr) {
		this.okr = okr;
	}

	public  OperacionalResumoJornada getOrj() {
		return orj;
	}

	public  void setOrj(OperacionalResumoJornada orj) {
		this.orj = orj;
	}

	public  OperacionalDiarioMotorista getOdim() {
		return odim;
	}

	public  void setOdim(OperacionalDiarioMotorista odim) {
		this.odim = odim;
	}

	public  OperacionalViagem getOv() {
		return ov;
	}

	public  void setOv(OperacionalViagem ov) {
		this.ov = ov;
	}

	public  OperacionalVisita getOvis() {
		return ovis;
	}

	public  void setOvis(OperacionalVisita ovis) {
		this.ovis = ovis;
	}

	public  OperacionalTempoLigado getOtl() {
		return otl;
	}

	public  void setOtl(OperacionalTempoLigado otl) {
		this.otl = otl;
	}

	public  OperacionalTempoOcioso getOto() {
		return oto;
	}

	public  void setOto(OperacionalTempoOcioso oto) {
		this.oto = oto;
	}

	public  OperacionalTempoParado getOtp() {
		return otp;
	}

	public  void setOtp(OperacionalTempoParado otp) {
		this.otp = otp;
	}

	public  PontoInteresseAvancado getPia() {
		return pia;
	}

	public  void setPia(PontoInteresseAvancado pia) {
		this.pia = pia;
	}

	public  PontoInteresseBasico getPib() {
		return pib;
	}

	public  void setPib(PontoInteresseBasico pib) {
		this.pib = pib;
	}

	public  PontoInteresseRaioInteresse getPiri() {
		return piri;
	}

	public  void setPiri(PontoInteresseRaioInteresse piri) {
		this.piri = piri;
	}

	public  PontoInteresseTempoPermanencia getPitp() {
		return pitp;
	}

	public  void setPitp(PontoInteresseTempoPermanencia pitp) {
		this.pitp = pitp;
	}

	public  Rotograma getR() {
		return r;
	}

	public  void setR(Rotograma r) {
		this.r = r;
	}

	public  RotogramaViolacao getRv() {
		return rv;
	}

	public  void setRv(RotogramaViolacao rv) {
		this.rv = rv;
	}

	public  SistemaAcessos getSa() {
		return sa;
	}

	public  void setSa(SistemaAcessos sa) {
		this.sa = sa;
	}

	public  SistemaDisponibilidade getSd() {
		return sd;
	}

	public  void setSd(SistemaDisponibilidade sd) {
		this.sd = sd;
	}

	public  SistemaSituacaoVeiculo getSsv() {
		return ssv;
	}

	public  void setSsv(SistemaSituacaoVeiculo ssv) {
		this.ssv = ssv;
	}

	public  SistemaIndisponibilidadeVeiculo getSiv() {
		return siv;
	}

	public  void setSiv(SistemaIndisponibilidadeVeiculo siv) {
		this.siv = siv;
	}

	public  SistemaTickets getSt() {
		return st;
	}

	public  void setSt(SistemaTickets st) {
		this.st = st;
	}

	public  CadastroAbastecimento getCa() {
		return ca;
	}

	public  void setCa(CadastroAbastecimento ca) {
		this.ca = ca;
	}

	public  CadastroAcidentes getCac() {
		return cac;
	}

	public  void setCac(CadastroAcidentes cac) {
		this.cac = cac;
	}

	public  CadastroAjudantesMonitores getCam() {
		return cam;
	}

	public  void setCam(CadastroAjudantesMonitores cam) {
		this.cam = cam;
	}

	public  CadastroCercas getCc() {
		return cc;
	}

	public  void setCc(CadastroCercas cc) {
		this.cc = cc;
	}

	public  CadastroRotas getCr() {
		return cr;
	}

	public  void setCr(CadastroRotas cr) {
		this.cr = cr;
	}

	public  CadastroPontoInteresse getCpi() {
		return cpi;
	}

	public  void setCpi(CadastroPontoInteresse cpi) {
		this.cpi = cpi;
	}

	public  CadastroAreaNaoPermitida getCanp() {
		return canp;
	}

	public  void setCanp(CadastroAreaNaoPermitida canp) {
		this.canp = canp;
	}

	public  CadastroChecklist getCch() {
		return cch;
	}

	public  void setCch(CadastroChecklist cch) {
		this.cch = cch;
	}

	public  CadastroAgendamento getCag() {
		return cag;
	}

	public  void setCag(CadastroAgendamento cag) {
		this.cag = cag;
	}

	public  CadastroMotorista getCm() {
		return cm;
	}

	public  void setCm(CadastroMotorista cm) {
		this.cm = cm;
	}

	public  CadastroMultas getCmu() {
		return cmu;
	}

	public  void setCmu(CadastroMultas cmu) {
		this.cmu = cmu;
	}

	public  ConfiguracaoAgendamentoRelatorios getCoar() {
		return coar;
	}

	public  void setCoar(ConfiguracaoAgendamentoRelatorios coar) {
		this.coar = coar;
	}

	public  ConfiguracaoNotificacoes getCon() {
		return con;
	}

	public  void setCon(ConfiguracaoNotificacoes con) {
		this.con = con;
	}

	public  ConfiguracaoNotificacoesContatos getConc() {
		return conc;
	}

	public  void setConc(ConfiguracaoNotificacoesContatos conc) {
		this.conc = conc;
	}

	public  AtendimentoFaleConosco getAfc() {
		return afc;
	}

	public  void setAfc(AtendimentoFaleConosco afc) {
		this.afc = afc;
	}

	public  UsuarioPerfil getP() {
		return up;
	}

	public  void setP(UsuarioPerfil p) {
		this.up = p;
	}

	public  UsuarioAdministrar getUa() {
		return ua;
	}

	public  void setUa(UsuarioAdministrar ua) {
		this.ua = ua;
	}

	public  UsuarioAdministrarGrupos getUag() {
		return uag;
	}

	public  void setUag(UsuarioAdministrarGrupos uag) {
		this.uag = uag;
	}

	public  CadastroVeiculos getCv() {
		return cv;
	}

	public  void setCv(CadastroVeiculos cv) {
		this.cv = cv;
	}

	public  MapaRota getMr() {
		return mr;
	}

	public void setMr(MapaRota mr) {
		this.mr = mr;
	}

	public SafetyViolacaoDeBateria getSvdb() {
		return svdb;
	}

	public SafetyViolacaoDeBateria setSvdb(SafetyViolacaoDeBateria svdb) {
		this.svdb = svdb;
		return svdb;
	}

	public static Facade getInstance() {
		if(instance == null)
			instance = new Facade();
		return instance;
	}

	public static void setInstance(Facade instance) {
		Facade.instance = instance;
	}

	public PetrobrasCadastroMultas getPcm() {
		return pcm;
	}

	public void setPcm(PetrobrasCadastroMultas pcm) {
		this.pcm = pcm;
	}

	public FrotasEventosOcorridos getFeo() {
		return feo;
	}

	public FrotasEventosOcorridos setFeo(FrotasEventosOcorridos feo) {
		this.feo = feo;
		return feo;
	}
}
