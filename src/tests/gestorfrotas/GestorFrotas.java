package tests.gestorfrotas;

import java.io.FileWriter;
import java.util.ArrayList;

import javax.swing.JFileChooser;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import tests.Facade;
import tests.gestorfrotas.menuresponsivo.relatorios.saver.FrotasEventosOcorridos;
import tests.gestorfrotas.menuresponsivo.relatorios.Relatorio;
import tests.util.Comandos;
import tests.util.Variaveis;

public class GestorFrotas {
	
	private static boolean flag = true;
	private static GestorFrotas instance;

	private GestorFrotas() {
		
	}
	
	public static void testarRelatorios(WebDriver driver) {
		// testes relatórios operacionais
		ArrayList<Relatorio> relatorios = new ArrayList<>();
				
		relatorios.add(Facade.getInstance().setFeo(new FrotasEventosOcorridos(driver))); //testes de Eventos Ocorridos
        
		// salvando testes em arquivo
        for(Relatorio r: relatorios) {
            salvarRelatoriosArquivo(r);
        }
        	
	}
	
	public static void salvarRelatoriosArquivo(Relatorio relatorio) {
		JFileChooser fc = new JFileChooser();
        fc.requestFocus();
        if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
        	try (FileWriter f = new FileWriter(fc.getSelectedFile()+".txt")){
				f.write(relatorio.getDadosArquivo().toString());
			} catch(Exception e) {
				e.printStackTrace();
			}
        }
	}
	
	public static void fecharChatterBot(WebDriver driver){
		new Thread() {
		    @Override
		    public void run() {
			    JavascriptExecutor js = ((JavascriptExecutor) driver);
			    
	    		while(flag){
	    			try {
		    			WebElement chat = driver.findElement((By.xpath("//*[@id=\"intercom-container\"]/div/div[1]/iframe")));
			    		js.executeScript("arguments[0].style='display: none'", chat);
	    			}
			    	catch(Exception e) {
			    		e.printStackTrace();
			    	}
		    		
	    		}
		    	
		    }
		}.start();
	 
	}
	
	public static void testarGestorFrotas() {
		// inicia as ferramentas utilizadas
        WebDriver driver = new ChromeDriver(); //Driver do Selenium no Chrome

        // configurações        
        driver.manage().window().maximize(); //maximiza tela
        driver.get(Variaveis.getDefaulturl()); //acessa o site do sistema
        
        // executa comandos no sistema
        Comandos.login(driver,Variaveis.getDefaultuserfrotas(),Variaveis.getDefaultpasswordfrotas()); //faz login
        
        // testes
        try {
	        testarRelatorios(driver);
	        	        	        
	        Comandos.logout(driver); //faz logout no sistema
	        driver.quit(); //fecha o browser
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
	       
	}

	public static GestorFrotas getInstance() {
		if(instance == null)
			instance = new GestorFrotas();
		return instance;
	}

	public static void setInstance(GestorFrotas instance) {
		GestorFrotas.instance = instance;
	}
	
}
