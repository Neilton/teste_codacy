package tests.gestorfrotas.menuresponsivo.atendimento;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class Atendimento {
	private static WebDriverWait wait;
	private static String xpath = "//*[@id=\"sidebar-items\"]/li[6]/";
	
	public static String abrirAtendimento(WebDriver driver) {
		String xpath = Atendimento.xpath;
        wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		Comandos.menuResponsivo(driver); //abre menu responsivo

        WebElement cadastros = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath+"a")));
        cadastros.click();
        return xpath;
	}
}
