package tests.gestorfrotas.menuresponsivo.atendimento;

import java.util.ArrayList;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class AtendimentoFaleConosco extends Atendimento{
	private static final String placaTela = "Atendimento de Fale Conosco";
	
	private static String assunto = "teste do teste testado";
	private static String departamento = "MANUTEN��O";
	private static String placa = "OKZ-3598";
	private static int indexPrioridade = 1;
	private static String descricao = "teste do teste testado do teste testado no teste";
	private static String arquivo = "C:/Users/Jos�Neilton/Desktop/183516818_2GG.jpg";

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		assunto = "teste do teste testado";
		departamento = "MANUTEN��O";
		placa = "OKZ-3598";
		indexPrioridade = 1;
		descricao = "teste do teste testado do teste testado no teste";
		arquivo = "C:/Users/Jos�Neilton/Desktop/183516818_2GG.jpg";
	}

	public AtendimentoFaleConosco(WebDriver driver) {
		wait = new WebDriverWait(driver, 60);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+placaTela+"\n");
		
		clear();
				
		errorBuffer.append(atenderPerfeito(driver)+"\n"); //teste perfeito sem nenhum checkbox		
		errorBuffer.append(atenderCampoAssuntoVazio(driver)+"\n"); //teste com campo assunto vazio
		errorBuffer.append(atenderCampoDepartamentoVazio(driver)+"\n"); //teste com campo departamento vazio
		errorBuffer.append(atenderCampoPrioridadeVazio(driver)+"\n"); //teste com campo prioridade vazio
		errorBuffer.append(atenderCampoPlacaVazio(driver)+"\n"); //teste com campo placa vazio
		errorBuffer.append(atenderCampoDescricaoVazio(driver)+"\n"); //teste com campo descri��o vazio
		errorBuffer.append(atenderCampoArquivoVazio(driver)+"\n"); //teste com campo arquivo vazio

		
		setDadosArquivo(errorBuffer);

	}
	
	public static boolean escolherAtendimento(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de atendimento
        String xpath = abrirAtendimento(driver)+"ul/li[3]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  

        return atender(driver);
	}
	
	public static boolean atender(WebDriver driver) {
		boolean tudoCerto = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em atender novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/button")));
        novo.click();
                
        WebElement assuntoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("assunto")));
        js.executeScript("arguments[0].value=arguments[1]",assuntoE,assunto);
        
        if(!departamento.equals("")) {
	        WebElement departamentoE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"departamento_chosen\"]")));
	        departamentoE.click();
	        departamentoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"departamento_chosen\"]/div/div/input")));
	        departamentoE.sendKeys(departamento);
	        departamentoE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"departamento_chosen\"]/div/ul/li")));
	        departamentoE.click();
        }
        
        WebElement baseE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("prioridade")));
        if(indexPrioridade >= 0) {
		    Select selectBaseE = new Select(baseE);
		    selectBaseE.selectByIndex(indexPrioridade);
        }
        
        if(!placa.equals("")) {
	        WebElement placaE = wait.until(ExpectedConditions.elementToBeClickable(By.id("placa_chosen")));
	        placaE.click();
	        placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placa_chosen\"]/div/div/input")));
	        placaE.sendKeys(placa);
	        placaE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"placa_chosen\"]/div/ul/li")));
	        placaE.click();
        }
        
        WebElement descricaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("descricao")));
        descricaoE.clear();
        descricaoE.sendKeys(descricao);
        
        WebElement arquivoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("arquivo")));
        if(!arquivo.equals(""))
        	arquivoE.sendKeys(arquivo);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("salvar-ticket")));
        botaoSubmit.click();
                
        clear();
        while(true) {
		    try {
		    	wait.until(ExpectedConditions.alertIsPresent());
		        Alert alert = driver.switchTo().alert();
		        if(alert.getText().equals("Ticket cadastrado com sucesso!"))
		        	tudoCerto = true;
		        alert.accept();
		    }
		    catch(NoAlertPresentException | TimeoutException e) {
		    	
		    	e.printStackTrace();
		    	break;
		    }
        }
        return tudoCerto;
	}
	
	public static StringBuffer atenderPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "atenderPerfeito";
		
		
				
		try {
			Assert.assertTrue(escolherAtendimento(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "ATENDIMENTO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));

	    	WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalNewTicket\"]/div/div/div[1]/button")));
	        fechar.click();
		}
		
        return errorBuffer;
        
	}
	
	public static StringBuffer atenderCampoArquivoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "atenderCampoArquivoVazio";
		
		arquivo = "";
				
		try {
			Assert.assertTrue(escolherAtendimento(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "ATENDIMENTO N�O EFETUADO MESMO SEM ARQUIVO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));

	    	WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalNewTicket\"]/div/div/div[1]/button")));
	        fechar.click();
		}
		
        return errorBuffer;
        
	}
	
	public static StringBuffer atenderCampoPrioridadeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "atenderCampoPrioridadeVazio";
				
		indexPrioridade = -1;
		
		
		
		try {
			Assert.assertTrue(escolherAtendimento(driver));
			
			String motivo = "ATENDIMENTO � EFETUADO MESMO COM CAMPO PRIORIDADE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalNewTicket\"]/div/div/div[1]/button")));
	        fechar.click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer atenderCampoAssuntoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "atenderCampoAssuntoVazio";
				
		assunto = "";
		
		
		
		try {
			Assert.assertTrue(escolherAtendimento(driver));
			
			String motivo = "ATENDIMENTO � EFETUADO MESMO COM CAMPO ASSUNTO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalNewTicket\"]/div/div/div[1]/button")));
	        fechar.click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer atenderCampoDepartamentoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "atenderCampoDepartamentoVazio";
				
		departamento = "";
		
		
		
		try {
			Assert.assertTrue(escolherAtendimento(driver));
			
			String motivo = "ATENDIMENTO � EFETUADO MESMO COM CAMPO DEPARTAMENTO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalNewTicket\"]/div/div/div[1]/button")));
	        fechar.click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer atenderCampoPlacaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "atenderCampoPlacaVazio";
				
		placa = "";
		
		
		
		try {
			Assert.assertTrue(escolherAtendimento(driver));
			
			String motivo = "ATENDIMENTO � EFETUADO MESMO COM CAMPO PLACA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalNewTicket\"]/div/div/div[1]/button")));
	        fechar.click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer atenderCampoDescricaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "atenderCampoDescricaoVazio";
				
		descricao = "";
		
		
		
		try {
			Assert.assertTrue(escolherAtendimento(driver));
			
			String motivo = "ATENDIMENTO � EFETUADO MESMO COM CAMPO DESCRI��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalNewTicket\"]/div/div/div[1]/button")));
	        fechar.click();
		}
        return errorBuffer;
	}
	
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		AtendimentoFaleConosco.dadosArquivo = dadosArquivo;
	}
}
