package tests.gestorfrotas.menuresponsivo.cadastros;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;
import tests.util.Comandos;
import tests.util.Variaveis;

public class CadastroAbastecimento extends Cadastros{
	private static final String nomeTela = "Cadastro Abastecimento";
	
	private static String placa; 
	private static String combustivel;
	private static String hodometro;
	private static String qLitros;
	private static String valor;
	private static String data;
	private static String local;
	
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		placa = "ACK0588"; 
		combustivel = "Gasolina";
		hodometro = "10";
		qLitros = "10";
		valor = "1,50";
		data = "01/01/2020";
		local = "TESTE DO TESTE TESTANDO O TESTEIRO";
	}
	
	public static void clearEdicao() {
		placa = null; 
		combustivel = null;
		hodometro = null;
		qLitros = null;
		valor = null;
		data = null;
		local = null;
	}

	public CadastroAbastecimento(WebDriver driver) {
		wait = new WebDriverWait(driver, 20);
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		//errorBuffer.append(testeCadastro(driver));
		errorBuffer.append(testeEdicao(driver));
		
		setDadosArquivo(errorBuffer);

	}
	
	public static StringBuffer testeCadastro(WebDriver driver) {
		clear();
		escolherCadastro(driver);
		StringBuffer errorBuffer = new StringBuffer();

		errorBuffer.append("\nCADASTROS\n");

		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		errorBuffer.append(cadastrarCampoPlacaVazio(driver)+"\n"); //teste com campo placa vazio
		errorBuffer.append(cadastrarPlacaInvalida(driver)+"\n"); //teste com placa inv�lida
		errorBuffer.append(cadastrarCampoCombustivelVazio(driver)+"\n"); //teste com campo combustivel vazio
		errorBuffer.append(cadastrarCombustivelInvalido(driver)+"\n"); //teste com combustivel invalido
		errorBuffer.append(cadastrarCampoHodometroVazio(driver)+"\n"); //teste com campo hodometro vazio
		errorBuffer.append(cadastrarHodometroInvalido(driver)+"\n"); //teste com hodometro invalido
		errorBuffer.append(cadastrarHodometroNegativo(driver)+"\n"); //teste com hodometro negativo
		errorBuffer.append(cadastrarCampoQLitrosVazio(driver)+"\n"); //teste com campo quantidade de litros vazio
		errorBuffer.append(cadastrarQLitrosInvalido(driver)+"\n"); //teste com quantidade de litros inv�lido
		errorBuffer.append(cadastrarQLitrosNegativo(driver)+"\n"); //teste com quantidade de litros negativo
		errorBuffer.append(cadastrarCampoValorVazio(driver)+"\n"); //teste com campo valor vazio
		errorBuffer.append(cadastrarValorInvalido(driver)+"\n"); //teste com valor inv�lido
		errorBuffer.append(cadastrarValorNegativo(driver)+"\n"); //teste com valor negativo
		errorBuffer.append(cadastrarCampoDataVazio(driver)+"\n"); //teste com campo data vazio
		errorBuffer.append(cadastrarDataInvalida(driver)+"\n"); //teste com data inv�lida
		errorBuffer.append(cadastrarDataMaiorQueAtual(driver)+"\n"); //teste com data maior que data atual
		
		return errorBuffer;
	}
	
	public static StringBuffer testeEdicao(WebDriver driver) {
		clearEdicao();
		escolherCadastro(driver);
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\nEDI��O\n");

		errorBuffer.append(editarCampoPlaca(driver)+"\n"); //teste com placa
		errorBuffer.append(editarCampoCombustivel(driver)+"\n"); //teste com campo combustivel
		errorBuffer.append(editarCampoHodometro(driver)+"\n"); //teste perfeito
		errorBuffer.append(editarCampoHodometroVazio(driver)+"\n"); //teste com campo hodometro vazio
		errorBuffer.append(editarHodometroInvalido(driver)+"\n"); //teste com hodometro invalido
		errorBuffer.append(editarHodometroNegativo(driver)+"\n"); //teste com hodometro negativo
		errorBuffer.append(editarCampoQLitros(driver)+"\n"); //teste com campo quantidade de litros
		errorBuffer.append(editarCampoQLitrosVazio(driver)+"\n"); //teste com campo quantidade de litros vazio
		errorBuffer.append(editarQLitrosInvalido(driver)+"\n"); //teste com quantidade de litros inv�lido
		errorBuffer.append(editarQLitrosNegativo(driver)+"\n"); //teste com quantidade de litros negativo
		errorBuffer.append(editarCampoValor(driver)+"\n"); //teste com campo valor
		errorBuffer.append(editarCampoValorVazio(driver)+"\n"); //teste com campo valor vazio
		errorBuffer.append(editarValorInvalido(driver)+"\n"); //teste com valor inv�lido
		errorBuffer.append(editarValorNegativo(driver)+"\n"); //teste com valor negativo
		errorBuffer.append(editarCampoData(driver)+"\n"); //teste campo data
		errorBuffer.append(editarDataInvalido(driver)+"\n"); //teste com data inv�lida
		
		return errorBuffer;
	}
	
	
	
	public static void escolherCadastro(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[1]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
	}
	
	public static void cadastrar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas

        //Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_abs")));
        novo.click();
        
        WebElement placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placa_chosen\"]/div/div/input")));
        placaE.clear();
        placaE.sendKeys(placa,Keys.ENTER);
        
        WebElement combustivelE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"fuel_chosen\"]/div/div/input")));
        combustivelE.clear();
        combustivelE.sendKeys(combustivel,Keys.ENTER);
        
        WebElement hodometroE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hodometro")));
        hodometroE.clear();
        hodometroE.sendKeys(hodometro);
        
        WebElement litrosE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("litros")));
        litrosE.clear();
        litrosE.sendKeys(qLitros);
        
        WebElement valorE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("valor")));
        valorE.clear();
        valorE.sendKeys(valor);
        
        WebElement dataE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data")));
        dataE.clear();
        dataE.sendKeys(data);
        
        WebElement localE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("local")));
        localE.clear();
        localE.sendKeys(local);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/form/div[2]/button")));
        botaoSubmit.click();
        
        clear();
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void editar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Clica em editar primeiro elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
        novo.click();

        if(placa != null) {
            WebElement placaE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[1]/div/div")));
        	placaE.click();
        	placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[1]/div/div/a/span")));
        	placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[1]/div/div/div/div/input")));
        	placaE.clear();
        	placaE.sendKeys(placa,Keys.ENTER);
        }
        
        if(combustivel != null) {
            WebElement combustivelE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[2]/div/div")));
        	combustivelE.click();
        	combustivelE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[2]/div/div/a/span")));
        	combustivelE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[2]/div/div/div/div/input")));
        	combustivelE.clear();
        	combustivelE.sendKeys(combustivel,Keys.ENTER);
        }
        
        if(hodometro != null) {
            WebElement hodometroE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[3]/div/input")));
            hodometroE.clear();
            hodometroE.sendKeys(hodometro);
        }
        
        if(qLitros != null) {
            WebElement litrosE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[4]/div/input")));
        	litrosE.clear();
        	litrosE.sendKeys(qLitros);
        }
        
        if(valor != null) {
            WebElement valorE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[5]/div/input")));
        	valorE.clear();
        	valorE.sendKeys(valor);
        }
        
        if(data != null) {
            WebElement dataE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[6]/div/input")));
            dataE.clear();
            dataE.sendKeys(data);
        }
        
        if(local != null) {
            WebElement localE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("l//*[@id=\"EditarAbastecimento229\"]/div/div/div[2]/div/div[7]/div/textareaocal")));
            localE.clear();
            localE.sendKeys(local);

        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[3]/button")));
        botaoSubmit.click();
        
        clearEdicao();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	
			e.printStackTrace();
		}

	}
	
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoPlacaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoPlacaVazio";
				
		placa = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO PLACA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
		
	public static StringBuffer editarCampoPlaca(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPlaca";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO PLACA! ";		
		
		placa = "OKZ-3598";
		String novoValor = placa;
		editar(driver);
				
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();
			WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[1]/div/div/a/span")));
 
	        
	        Assert.assertEquals(novoValor,elemento.getText());
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarPlacaInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPlacaInvalida";
				
		placa = "AHAHAHA";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM PLACA INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCombustivelVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCombustivelVazio";
				
		combustivel = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO COMBUST�VEL VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
        return errorBuffer;
	}

	public static StringBuffer cadastrarCombustivelInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCombustivelInvalido";
				
		combustivel = "AHAHAHA";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM COMBUST�VEL INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
		
	public static StringBuffer editarCampoCombustivel(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoCombustivel";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO COMBUST�VEL! ";		
		
		combustivel = "GNV";
		String novoValor = combustivel;
		editar(driver);
				
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();
	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[2]/div/div/a/span")));

	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoHodometroVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoHodometroVazio";
				
		hodometro = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO HOD�METRO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarHodometroInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarHodometroInvalido";
				
		hodometro = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM HOD�METRO INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarHodometroNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarHodometroNegativo";
				
		hodometro = "-5";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM HOD�METRO NEGATIVO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoHodometroVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoHodometroVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO HOD�METRO VAZIO! ";
		
		hodometro = "";
		String novoValor = hodometro;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[3]/div/input")));
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarHodometroInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarHodometroInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO HOD�METRO INV�LIDO! ";
		
		hodometro = "abc!";
		String novoValor = hodometro;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[3]/div/input")));
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarHodometroNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarHodometroNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM HOD�METRO NEGATIVO! ";
		
		hodometro = "-5";
		String novoValor = hodometro;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[3]/div/input")));
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoHodometro(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoHodometro";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO HOD�METRO! ";		
		
		hodometro = "56";
		String novoValor = hodometro;
		editar(driver);
				
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[3]/div/input")));

	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoQLitrosVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoQLitrosVazio";
				
		qLitros = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO QUANTIDADE DE LITROS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarQLitrosInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarQLitrosInvalido";
				
		qLitros = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM QUANTIDADE DE LITROS INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarQLitrosNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarQLitrosNegativo";
				
		qLitros = "-5";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM QUANTIDADE DE LITROS NEGATIVO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoQLitrosVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoQLitrosVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO QUANTIDADE DE LITROS VAZIO! ";
		
		qLitros = "";
		String novoValor = qLitros;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[4]/div/input")));
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarQLitrosInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarQLitrosInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO QUANTIDADE DE LITROS INV�LIDO! ";
		
		qLitros = "abc!";
		String novoValor = qLitros;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[4]/div/input")));
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarQLitrosNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarQLitrosNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM QUANTIDADE DE LITROS NEGATIVO! ";
		
		qLitros = "-5";
		String novoValor = qLitros;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[4]/div/input")));
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoQLitros(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoQLitros";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO QUANTIDADE DE LITROS! ";		
		
		qLitros = "56";
		String novoValor = qLitros;
		editar(driver);
				
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[4]/div/input")));

	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoValorVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoValorVazio";
				
		valor = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO VALOR VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarValorInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarValorInvalido";
				
		valor = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM VALOR INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarValorNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarValorNegativo";
				
		valor = "-7";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM VALOR NEGATIVO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoValorVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoValorVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO VALOR VAZIO! ";
		
		valor = "";
		String novoValor = valor;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[5]/div/input")));
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarValorInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarValorInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO VALOR INV�LIDO! ";
		
		valor = "abc!";
		String novoValor = valor;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[5]/div/input")));
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarValorNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarValorNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM VALOR NEGATIVO! ";
		
		valor = "-5";
		String novoValor = valor;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[5]/div/input")));
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoValor(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoValor";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO VALOR! ";		
		
		valor = "25";
		String novoValor = valor;
		editar(driver);
				
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[5]/div/input")));

	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDataVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDataVazio";
				
		data = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataInvalida";
				
		data = "abc";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataMaiorQueAtual(WebDriver driver) {
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
		
		String dataN = formatoData.format(calendario.getTime());
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataMaiorQueAtual";
		
        data = dataN;
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA MAIOR QUE A ATUAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoData(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoData";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO DATA! ";		
		
		data = "03/06/2020";
		String novoValor = data;
		editar(driver);
				
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[6]/div/input")));
	        Date elementoData = Variaveis.getFormatodata().parse(elemento.getAttribute("value"));
	        
	        Assert.assertEquals(novoValor,Variaveis.getFormatodata().format(elementoData));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();
        return errorBuffer;
	}
	
	public static StringBuffer editarDataInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarDataInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO DATA INV�LIDA! ";
		
		data = "abc!";
		String novoValor = data;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[6]/div/input")));
	        Date elementoData = Variaveis.getFormatodata().parse(elemento.getAttribute("value"));
	        
	        Assert.assertEquals(novoValor,Variaveis.getFormatodata().format(elementoData));
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoLocalVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoLocalVazio";
				
		local = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO LOCAL VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAbastecimento\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}

	public static StringBuffer editarCampoLocal(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoLocal";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO LOCAL! ";		
		
		local = "AHAHAHA HAHAHAH";
		String novoValor = local;
		editar(driver);
				
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[7]/div/textarea")));
	        
	        Assert.assertEquals(novoValor,elemento.getText());
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoLocalVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoLocalVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO LOCAL VAZIO! ";
		
		data = "abc!";
		String novoValor = data;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[1]/td[9]/a[1]")));
	        novo.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[2]/div/div[7]/div/textarea")));
	        
	        Assert.assertEquals(novoValor,elemento.getText());
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"EditarAbastecimento22\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroAbastecimento.dadosArquivo = dadosArquivo;
	}
}
