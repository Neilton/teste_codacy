package tests.gestorfrotas.menuresponsivo.cadastros;

import static org.junit.Assert.assertFalse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class CadastroAcidentes extends Cadastros{
	private static final String nomeTela = "Cadastro Acidentes";
	
	private static String placa = "ACK0588"; 
	private static String arquivo = "C:/Users/Jos�Neilton/Desktop/183516818_2GG.jpg";
	private static String data = "01/01/2020";
	private static String descricao = "TESTE DO TESTE TESTANDO O TESTEIRO";
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		placa = "ACK0588"; 
		arquivo = "C:/Users/Jos�Neilton/Desktop/183516818_2GG.jpg";
		descricao = "TESTE DO TESTE TESTANDO O TESTEIRO";
		data = "01/01/2020";
	}

	public CadastroAcidentes(WebDriver driver) {
		wait = new WebDriverWait(driver, 25);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		clear();
		escolherCadastro(driver);
		
		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		errorBuffer.append(cadastrarCampoPlacaVazio(driver)+"\n"); //teste com campo placa vazio
		errorBuffer.append(cadastrarPlacaInvalida(driver)+"\n"); //teste com placa inv�lida
		errorBuffer.append(cadastrarCampoDataVazio(driver)+"\n"); //teste com campo data vazio
		errorBuffer.append(cadastrarDataInvalida(driver)+"\n"); //teste com data inv�lida
		errorBuffer.append(cadastrarDataMaiorQueAtual(driver)+"\n"); //teste com data maior que atual
		errorBuffer.append(cadastrarBotaoLimpar(driver)+"\n"); //teste com bot�o limpar
		errorBuffer.append(cadastrarCampoDescricaoVazio(driver)+"\n"); //teste com campo descri��o vazio
		
		setDadosArquivo(errorBuffer);

	}
	
	public static void escolherCadastro(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[2]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
	}
	
	public static void cadastrar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas

        //Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/div[1]/a")));
        novo.click();
        
        WebElement placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placa_chosen\"]")));
        placaE.click();
        placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placa_chosen\"]/div/div/input")));
        placaE.clear();
        placaE.sendKeys(placa,Keys.ENTER);
        
        WebElement dataE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data")));
        dataE.clear();
        dataE.sendKeys(data);
        
        WebElement arquivoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("img")));
        arquivoE.clear();
        arquivoE.sendKeys(arquivo);
        
        WebElement descricaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("descricao")));
        descricaoE.clear();
        descricaoE.sendKeys(descricao);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static StringBuffer cadastrarBotaoLimpar(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarBotaoLimpar";
		
		StringBuffer motivo = new StringBuffer();
		
		//Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/div[1]/a")));
        novo.click();
		
		WebElement arquivoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("img")));
	    arquivoE.clear();
	    arquivoE.sendKeys(arquivo);
	    
	    WebElement descricaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("descricao")));
	    descricaoE.clear();
	    descricaoE.sendKeys(descricao);		
	    
	    clear();
	    
	    String arquivoAntes = arquivoE.getText();	
	    String descricaoAntes = descricaoE.getText();
	    
	    WebElement limpar = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"form_acidente\"]/div[2]/button[1]")));
	    limpar.click();
	    
	    String arquivoDepois = arquivoE.getText();	
	    String descricaoDepois = descricaoE.getText();
	    
		try {
			boolean error = false;
			if(arquivoAntes.equals(arquivoDepois)) {
				motivo.append("BOT�O LIMPAR N�O LIMPA ARQUIVO DE FOTO || ");
				error = true;
			}
			if(descricaoAntes.equals(descricaoDepois)) {
				motivo.append("BOT�O LIMPAR N�O LIMPA DESCRI��O");
				error = true;
			}
			assertFalse(error);
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo.toString()));
		}
		catch(AssertionError e) {
			Comandos.printTesteSucesso(teste);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAcidente\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"table_acidente_filter\"]/label/input")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAcidente\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoPlacaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoPlacaVazio";
				
		placa = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"table_acidente_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO PLACA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAcidente\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarPlacaInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPlacaInvalida";
				
		placa = "AHAHAHA";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"table_acidente_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM PLACA INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAcidente\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoDataVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDataVazio";
				
		data = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"table_acidente_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAcidente\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataInvalida";
				
		data = "abc";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"table_acidente_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM DATA INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAcidente\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataMaiorQueAtual(WebDriver driver) {
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
		
		String dataN = formatoData.format(calendario.getTime());
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataMaiorQueAtual";
		
        data = dataN;
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"table_acidente_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA MAIOR QUE A ATUAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAcidente\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDescricaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDescricaoVazio";
				
		descricao = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"table_acidente_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DESCRI��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroAcidente\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroAcidentes.dadosArquivo = dadosArquivo;
	}
}
