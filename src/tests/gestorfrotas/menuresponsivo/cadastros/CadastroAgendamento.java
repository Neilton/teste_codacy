package tests.gestorfrotas.menuresponsivo.cadastros;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class CadastroAgendamento extends Cadastros{
	private static final String nomeTela = "Cadastro Agendamentos";
	
	private static String placa = "ACK0588"; 
	private static String hodometroMan = "10";
	private static String hodoProxMan = "15";
	private static String hodometroNot = "25";
	private static String dataMan = "01/01/2020";
	private static String dataProxMan = "01/02/2020";
	private static String dataNot = "15/01/2020";
	private static String descricao = "TESTE DO TESTE TESTANDO O TESTEIRO";
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		placa = "ACK0588"; 
		dataProxMan = "01/02/2020";
		dataNot = "15/01/2020";
		hodometroMan = "10";
		hodoProxMan = "15";
		hodometroNot = "25";
		dataMan = "01/01/2020";
		descricao = "TESTE DO TESTE TESTANDO O TESTEIRO";
	}

	public CadastroAgendamento(WebDriver driver) {
		wait = new WebDriverWait(driver, 40);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		clear();
		escolherCadastro(driver);
		
		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		errorBuffer.append(cadastrarCampoHodometroManVazio(driver)+"\n"); //teste com campo hodometro da Manuten��o vazio
		errorBuffer.append(cadastrarHodometroManInvalido(driver)+"\n"); //teste com hodometro da Manuten��o invalido
		errorBuffer.append(cadastrarHodometroManNegativo(driver)+"\n"); //teste com hodometro da Manuten��o negativo
		errorBuffer.append(cadastrarCampoHodoProxManVazio(driver)+"\n"); //teste com campo hod�metro da pr�xima manuten��o vazio
		errorBuffer.append(cadastrarHodoProxManInvalido(driver)+"\n"); //teste com hod�metro da pr�xima manuten��o inv�lido
		errorBuffer.append(cadastrarHodoProxManNegativo(driver)+"\n"); //teste com hod�metro da pr�xima manuten��o negativo
		errorBuffer.append(cadastrarCampoHodometroNotVazio(driver)+"\n"); //teste com campo hodometro de Notifica��o vazio
		errorBuffer.append(cadastrarHodometroNotInvalido(driver)+"\n"); //teste com hodometro de Notifica��o inv�lido
		errorBuffer.append(cadastrarHodometroNotNegativo(driver)+"\n"); //teste com hodometro de Notifica��o negativo
		errorBuffer.append(cadastrarCampoDataManVazio(driver)+"\n"); //teste com campo data da Manuten��o vazio
		errorBuffer.append(cadastrarDataManInvalida(driver)+"\n"); //teste com data da Manuten��o inv�lida
		errorBuffer.append(cadastrarCampoDataNotVazio(driver)+"\n"); //teste com campo data da notifica��o vazio
		errorBuffer.append(cadastrarDataNotInvalida(driver)+"\n"); //teste com data da notifica�� inv�lida
		errorBuffer.append(cadastrarCampoDataProxManVazio(driver)+"\n"); //teste com campo data da pr�xima Manuten��o vazio
		errorBuffer.append(cadastrarDataProxManInvalida(driver)+"\n"); //teste com data da pr�xima Manuten��o inv�lida
		
		errorBuffer.append(cadastrarDataNotMaiorQueDataProxMan(driver)+"\n"); //teste com data da notifica��o maior que data da pr�xima Manuten��o
		errorBuffer.append(cadastrarDataNotMenorQueDataMan(driver)+"\n"); //teste com data da notifica��o menor que data da Manuten��o
		errorBuffer.append(cadastrarDataManMaiorQueDataProxMan(driver)+"\n"); //teste com data da manuten��o maior que data da pr�xima Manuten��o

		setDadosArquivo(errorBuffer);

	}
	
	public static void escolherCadastro(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[11]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
	}
	
	public static boolean cadastrar(WebDriver driver) {
		boolean erro = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas

        //Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"home\"]/div[1]/button")));
        novo.click();
        
        WebElement placaE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"placa_chosen\"]")));
        placaE.click();
        placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placa_chosen\"]/div/div/input")));
        placaE.clear();
        placaE.sendKeys(placa);
        placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placa_chosen\"]/div/ul/li")));
        placaE.click();
        
        WebElement hodometroManE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("odo_manutencao")));
        hodometroManE.clear();
        hodometroManE.sendKeys(hodometroMan);
        if(!hodometroMan.equals(hodometroManE.getText()))
        	erro = true;
        
        WebElement dataManE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("dt_manutencao")));
        dataManE.clear();
        dataManE.sendKeys(dataMan);
        
        WebElement hodoProxManE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ref_man_odo")));
        hodoProxManE.clear();
        hodoProxManE.sendKeys(hodoProxMan);
        if(!hodoProxMan.equals(hodometroManE.getText()))
        	erro = true;
        
        WebElement dataProxManE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("dt_ref")));
        dataProxManE.clear();
        dataProxManE.sendKeys(dataProxMan);
        
        WebElement descricaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("descricao")));
        descricaoE.clear();
        descricaoE.sendKeys(descricao);
        
        WebElement hodometroNotE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("odometro_notification")));
        hodometroNotE.clear();
        hodometroNotE.sendKeys(hodometroNot);
        if(!hodometroNot.equals(hodometroNotE.getText()))
        	erro = true;
        
        WebElement dataNotE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("dt_notification")));
        dataNotE.clear();
        dataNotE.sendKeys(dataNot);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("salvar")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return erro;
	}
	
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoHodometroManVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoHodometroManVazio";
				
		hodometroMan = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO HOD�METRO DA MANUTEN��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarHodometroManInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarHodometroManInvalido";
				
		hodometroMan = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM HOD�METRO DA MANUTEN��O INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarHodometroManNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarHodometroManNegativo";
		String motivo = "CADASTRO � EFETUADO MESMO COM HOD�METRO DA MANUTEN��O NEGATIVO";

		hodometroMan = "-5";
		
		if(cadastrar(driver)) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
			return errorBuffer;
		}

		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoHodoProxManVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoHodoProxManVazio";
				
		hodoProxMan = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO HOD�METRO PR�XIMA MANUTEN��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarHodoProxManInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarHodoProxManInvalido";
				
		hodoProxMan = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM HOD�METRO PR�XIMA MANUTEN��O INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarHodoProxManNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarHodoProxManNegativo";
				
		hodoProxMan = "-5";
		
		if(cadastrar(driver)) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            return errorBuffer;
		}
			
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM HOD�METRO PR�XIMA MANUTEN��O NEGATIVO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoHodometroNotVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoHodometroNotVazio";
				
		hodometroNot = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO HOD�METRO NOTIFICA��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarHodometroNotInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarHodometroNotInvalido";
				
		hodometroNot = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM HOD�METRO NOTIFICA��O INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarHodometroNotNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarHodometroNotNegativo";
				
		hodometroNot = "-7";
		
		if(cadastrar(driver)) {
        	errorBuffer.append(Comandos.printTesteSucesso(teste));
        	return errorBuffer;
		}
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM HOD�METRO NOTIFICA��O NEGATIVO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDataManVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDataManVazio";
				
		dataMan = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA DA MANUTEN��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataManInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataManInvalida";
				
		dataMan = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DA MANUTEN��O INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDataProxManVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDataProxManVazio";
				
		dataProxMan = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA DA PR�XIMA MANUTEN��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataProxManInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataProxManInvalida";
				
		dataProxMan = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DA PR�XIMA MANUTEN��O INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDataNotVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDataNotVazio";
				
		dataNot = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA DA NOTIFICA��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataNotInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataNotInvalida";
				
		dataNot = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DA NOTIFICA��O INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataNotMenorQueDataMan(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataNotMenorQueDataMan";
				
		dataNot = "01/01/2020";
		dataMan = "15/01/2020";
		
		try {
			cadastrar(driver);
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DA NOTIFICA��O MENOR QUE DATA DA MANUTEN��O";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			try {
				wait.until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
			}
			catch(Exception e1) {
				e1.printStackTrace();
			}
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataNotMaiorQueDataProxMan(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataNotMaiorQueDataProxMan";
				
		dataNot = "01/02/2020";
		dataProxMan = "15/01/2020";
		
		try {
			cadastrar(driver);
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DA NOTIFICA��O MAIOR QUE DATA DA PR�XIMA MANUTEN��O";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			try {
				wait.until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
			}
			catch(Exception e1) {
				e1.printStackTrace();
			}
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataManMaiorQueDataProxMan(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataManMaiorQueDataProxMan";
				
		dataProxMan = "01/12/2019";
		
		
		try {
			cadastrar(driver);
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DA MANUTEN��O MAIOR QUE DATA DA PR�XIMA MANUTEN��O";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			try {
				wait.until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
			}
			catch(Exception e1) {
				e1.printStackTrace();
			}
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoDescricaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDescricaoVazio";
				
		descricao = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DESCRI��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalAdicionarRegra\"]/div/button"))).click();
		}
        return errorBuffer;
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroAgendamento.dadosArquivo = dadosArquivo;
	}
}
