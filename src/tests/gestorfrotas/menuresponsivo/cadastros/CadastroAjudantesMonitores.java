package tests.gestorfrotas.menuresponsivo.cadastros;

import static org.junit.Assert.assertFalse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;
import tests.util.Comandos;

public class CadastroAjudantesMonitores extends Cadastros{
	private static final String nomeTela = "Cadastro Acidentes";
	
	private static String ajudante = "TESTE DO TESTE TESTANDO O TESTEIRO";
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static String dataNome = null;
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		ajudante = "TESTE DO TESTE TESTANDO O TESTEIRO";
	}

	public static void clearEdicao() {
		ajudante = null;
	}
	
	public static StringBuffer testarCadastro(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\nCADASTROS\n");
		
		clear();
		escolherCadastro(driver);
		
		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		errorBuffer.append(cadastrarCampoAjudanteVazio(driver)+"\n"); //teste com campo ajudante vazio
		errorBuffer.append(cadastrarBotaoFechar(driver)+"\n"); //teste com bot�o fechar
		
		return errorBuffer;
	}
	
	public static StringBuffer testarEdicao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\nEDI��O\n");
		
		clear();
		escolherCadastro(driver);
		
		errorBuffer.append(editarCampoAjudante(driver)+"\n"); //teste campo ajudante
		errorBuffer.append(editarCampoAjudanteVazio(driver)+"\n"); //teste com campo ajudante vazio
		
		return errorBuffer;
	}

	public CadastroAjudantesMonitores(WebDriver driver) {
		wait = new WebDriverWait(driver, 25);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		errorBuffer.append(testarCadastro(driver));
		errorBuffer.append(testarEdicao(driver));
		
		setDadosArquivo(errorBuffer);

	}
	
	public static void escolherCadastro(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[3]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
	}
	
	public static void cadastrar(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas

        //Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div/div[1]/a")));
        novo.click();
        
        WebElement ajudanteE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"NovoAjudante\"]/div/div/form/div[1]/input")));
        js.executeScript("arguments[0].value=arguments[1];",ajudanteE,ajudante);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"NovoAjudante\"]/div/div/form/div[2]/button[2]")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void editar(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas

        //Clica em editar primeiro elemento da tabela
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[2]/td[4]/a[1]")));
        novo.click();
        
        if(ajudante != null) {
	        WebElement ajudanteE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"formEditar\"]/div[1]/input")));
	        js.executeScript("arguments[0].value=arguments[1];",ajudanteE,ajudante);
        }
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
        
        clearEdicao();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"NovoAjudante\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoAjudanteVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoAjudanteVazio";
				
		ajudante = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO AJUDANTE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"NovoAjudante\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAjudante(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAjudante";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO AJUDANTE! ";		
		
		ajudante = "CHIMBINHA";
		String novoValor = ajudante;
		editar(driver);
				
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[2]/td[4]/a[1]")));
	        dataNome = novo.getAttribute("data-nome");
	        
	        Assert.assertEquals(novoValor,dataNome);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAjudanteVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAjudanteVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO AJUDANTE VAZIO! ";
		
		ajudante = "";
		String novoValor = ajudante;
		editar(driver);
		
		try {
			WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio\"]/tbody/tr[2]/td[4]/a[1]")));
	        dataNome = novo.getAttribute("data-nome");
        	
	        if(!novoValor.equals(dataNome))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalEditar\"]/div/div/div/button"))).click();

		}

        return errorBuffer;
	}
	
	public static StringBuffer cadastrarBotaoFechar(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarBotaoFechar";
						
		WebElement fecharE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"NovoAjudante\"]/div/div/form/div[2]/button[1]")));
		fecharE.click();
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"relatorio_filter\"]/label/input")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
            String motivo = "MODAL N�O � FECHADO AO SE CLICAR NO BOT�O FECHAR";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"NovoAjudante\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroAjudantesMonitores.dadosArquivo = dadosArquivo;
	}
}
