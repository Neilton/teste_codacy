package tests.gestorfrotas.menuresponsivo.cadastros;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class CadastroCercas extends Cadastros{
	private static final String nomeTela = "Cadastro �reas de Interesse - Cercas";
	
	private static String endereco = "Rua Lamartine Babo"; 
	private static String alcance = "500";
	private static String descricao = "ROTA NOVA";
	private static String limiteVel = "50";
	private static String cor = "#FF00FF"; //mangenta
	private static int indexMedicao = 1;
	private static int indexCerca = 1;
	
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
    static String windowHandle;

	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		endereco = "Rua Lamartine Babo"; 
		alcance = "500";
		descricao = "ROTA NOVA";
		limiteVel = "50";
		indexMedicao = 1;
		indexCerca = 1;
		cor = "#FF00FF";
	}

	public CadastroCercas(WebDriver driver) {
		wait = new WebDriverWait(driver, 10);
	    windowHandle = driver.getWindowHandle();
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		clear();
		escolherCadastro(driver);
		
		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		errorBuffer.append(cadastrarCampoEnderecoVazio(driver)+"\n"); //teste com campo endereco vazio
		errorBuffer.append(cadastrarCampoAlcanceVazio(driver)+"\n"); //teste com campo alcance vazio
		errorBuffer.append(cadastrarAlcanceInvalido(driver)+"\n"); //teste com alcance invalido
		errorBuffer.append(cadastrarAlcanceNegativo(driver)+"\n"); //teste com alcance negativo
		errorBuffer.append(cadastrarCampoDescricaoVazio(driver)+"\n"); //teste com campo descricao vazio
		errorBuffer.append(cadastrarCampoLimiteVelVazio(driver)+"\n"); //teste com campo quantidade de litros vazio
		errorBuffer.append(cadastrarLimiteVelInvalido(driver)+"\n"); //teste com quantidade de litros inv�lido
		errorBuffer.append(cadastrarLimiteVelNegativo(driver)+"\n"); //teste com quantidade de litros negativo
		errorBuffer.append(cadastrarTipoCercaInvalido(driver)+"\n"); //teste com tipo de Cerca invalido

		setDadosArquivo(errorBuffer);

	}
	
	public static void escolherCadastro(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[4]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
        
        //Abre a aba escolhida
        WebElement abaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tabCerca")));
        abaE.click();
	}
	
	public static void cadastrar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cercas\"]/div[1]/a[1]")));
        novo.click();
        
        //Vai para nova aba
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1)); 
        
        //Clica em cadastrar cerca por endere�o
        WebElement cerca = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"ende\"]/a")));
        //js.executeScript("arguments[0].click()", cerca);
        cerca.click();
        
        WebElement alcanceE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("raio")));
        alcanceE.clear();
        alcanceE.sendKeys(alcance);
        
        Comandos.escolherRadioButton(driver,"constanteRaio",indexMedicao);
        
        WebElement enderecoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("address_cerca")));
        enderecoE.clear();
        enderecoE.sendKeys(endereco);
        
        WebElement botaoE = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnAddressSearch")));
        botaoE.click(); 
        
        WebElement corE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("corCerca")));
        js.executeScript("arguments[0].value=arguments[1]", corE,cor);
        
        WebElement descricaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nome_rota")));
        js.executeScript("arguments[0].value=arguments[1];",descricaoE,descricao);
        
        WebElement limiteVelE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("limite_vel")));
        js.executeScript("arguments[0].value=arguments[1];",limiteVelE,limiteVel);
        
        Comandos.escolherRadioButton(driver,"optionsRadios2",indexCerca);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("save")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoEnderecoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoEnderecoVazio";
				
		endereco = "";
		
		
		try {
			cadastrar(driver);

			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO ENDERECO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			clear();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoAlcanceVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoAlcanceVazio";
				
		alcance = "";

		try {
			cadastrar(driver);

			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO ALCANCE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}

	public static StringBuffer cadastrarAlcanceInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarAlcanceInvalido";
				
		alcance = "abc!";
		
		try {
			cadastrar(driver);
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM ALCANCE INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			clear();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarAlcanceNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarAlcanceNegativo";
				
		alcance = "-500";
		
		
		try {
			cadastrar(driver);

			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM ALCANCE NEGATIVO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			clear();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
	    return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDescricaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDescricaoVazio";
				
		descricao = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DESCRICAO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoLimiteVelVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoLimiteVelVazio";
				
		limiteVel = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO LIMITE DE VELOCIDADE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarLimiteVelInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarLimiteVelInvalido";
				
		limiteVel = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM LIMITE DE VELOCIDADE INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarLimiteVelNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarLimiteVelNegativo";
				
		limiteVel = "-5";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM LIMITE DE VELOCIDADE NEGATIVO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroCercas.dadosArquivo = dadosArquivo;
	}
	
	public static StringBuffer cadastrarTipoCercaInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarTipoCercaInvalido";
				
		indexCerca = 0;
		
		try {
			cadastrar(driver);

			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tabela-cercas_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM TIPO DE CERCA INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
}
