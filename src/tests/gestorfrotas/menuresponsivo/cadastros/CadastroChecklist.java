package tests.gestorfrotas.menuresponsivo.cadastros;

import static org.junit.Assert.assertFalse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class CadastroChecklist extends Cadastros{
	private static final String nomeTela = "Cadastro Checklists";
	
	private static String placa = "ACK0588"; 
	private static String arquivo = "C:/Users/Jos�Neilton/Desktop/183516818_2GG.jpg";
	private static String data = "01/01/2020";
	private static String hora = "01:01";
	private static String observacoes = "TESTE DO TESTE TESTANDO O TESTEIRO";
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static int antMotor = 1;
	private static int posMotor = 1;
	private static int antFarois = 1;
	private static int posFarois = 1;
	private static int antBuzina = 1;
	private static int posBuzina = 1;
	private static int antPainel = 1;
	private static int posPainel = 1;
	private static int antPisca = 1;
	private static int posPisca = 1;
	private static int antFreio = 1;
	private static int posFreio = 1;
	private static int antMarcha = 1;
	private static int posMarcha = 1;
	private static int antLimpadores = 1;
	private static int posLimpadores = 1;
	private static int antSom = 1;
	private static int posSom = 1;
	private static int antBateria = 1;
	private static int posBateria = 1;
	private static int antEletro = 1;
	private static int posEletro = 1;
	private static int antExtintor = 1;
	private static int posExtintor = 1;
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		placa = "ACK0588"; 
		hora = "01:01";
		arquivo = "C:/Users/Jos�Neilton/Desktop/183516818_2GG.jpg";
		observacoes = "TESTE DO TESTE TESTANDO O TESTEIRO";
		data = "01/01/2020";
		
		antMotor = 1;
		posMotor = 1;
		antFarois = 1;
		posFarois = 1;
		antBuzina = 1;
		posBuzina = 1;
		antPainel = 1;
		posPainel = 1;
		antPisca = 1;
		posPisca = 1;
		antFreio = 1;
		posFreio = 1;
		antMarcha = 1;
		posMarcha = 1;
		antLimpadores = 1;
		posLimpadores = 1;
		antSom = 1;
		posSom = 1;
		antBateria = 1;
		posBateria = 1;
		antEletro = 1;
		posEletro = 1;
		antExtintor = 1;
		posExtintor = 1;
	}

	public CadastroChecklist(WebDriver driver) {
		wait = new WebDriverWait(driver, 25);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		clear();
		escolherCadastro(driver);
		
		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		errorBuffer.append(cadastrarCampoPlacaVazio(driver)+"\n"); //teste com campo placa vazio
		errorBuffer.append(cadastrarPlacaInvalida(driver)+"\n"); //teste com placa inv�lida
		errorBuffer.append(cadastrarCampoDataVazio(driver)+"\n"); //teste com campo data vazio
		errorBuffer.append(cadastrarDataInvalida(driver)+"\n"); //teste com data inv�lida
		errorBuffer.append(cadastrarCampoHoraVazio(driver)+"\n"); //teste com campo hora vazio
		errorBuffer.append(cadastrarHoraInvalida(driver)+"\n"); //teste com hora inv�lida
		errorBuffer.append(cadastrarDataMaiorQueAtual(driver)+"\n"); //teste com data maior que atual
		errorBuffer.append(cadastrarCampoObservacoesVazio(driver)+"\n"); //teste com campo observa��es vazio
		
		setDadosArquivo(errorBuffer);

	}
	
	public static void escolherCadastro(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[5]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
	}
	
	public static void cadastrar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas

        //Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.id("novo_checklist")));
        novo.click();
        
        WebElement placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placa_chosen\"]")));
        placaE.click();
        placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placa_chosen\"]/div/div/input")));
        placaE.clear();
        placaE.sendKeys(placa,Keys.ENTER);
        
        WebElement dataE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_serv")));
        dataE.clear();
        dataE.sendKeys(data);
        
        WebElement horaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora_serv")));
        horaE.clear();
        horaE.sendKeys(hora);
        
        WebElement arquivoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("img")));
        arquivoE.clear();
        arquivoE.sendKeys(arquivo);
        
        WebElement observacoesE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("obs")));
        observacoesE.clear();
        observacoesE.sendKeys(observacoes);
        
        Comandos.escolherRadioButtonNome(driver, "antmotor", antMotor);
        Comandos.escolherRadioButtonNome(driver, "posmotor", posMotor);
        Comandos.escolherRadioButtonNome(driver, "antfarois", antFarois);
        Comandos.escolherRadioButtonNome(driver, "posfarois", posFarois);
        Comandos.escolherRadioButtonNome(driver, "antbuzina", antBuzina);
        Comandos.escolherRadioButtonNome(driver, "posbuzina", posBuzina);
        Comandos.escolherRadioButtonNome(driver, "antpainel", antPainel);
        Comandos.escolherRadioButtonNome(driver, "pospainel", posPainel);
        Comandos.escolherRadioButtonNome(driver, "antpisca", antPisca);
        Comandos.escolherRadioButtonNome(driver, "pospisca", posPisca);
        Comandos.escolherRadioButtonNome(driver, "antfreio", antFreio);
        Comandos.escolherRadioButtonNome(driver, "posfreio", posFreio);
        Comandos.escolherRadioButtonNome(driver, "antmarcha", antMarcha);
        Comandos.escolherRadioButtonNome(driver, "posmarcha", posMarcha);
        Comandos.escolherRadioButtonNome(driver, "antlimpadores", antLimpadores);
        Comandos.escolherRadioButtonNome(driver, "poslimpadores", posLimpadores);
        Comandos.escolherRadioButtonNome(driver, "antsom", antSom);
        Comandos.escolherRadioButtonNome(driver, "possom", posSom);
        Comandos.escolherRadioButtonNome(driver, "antbateria", antBateria);
        Comandos.escolherRadioButtonNome(driver, "posbateria", posBateria);
        Comandos.escolherRadioButtonNome(driver, "anteletro", antEletro);
        Comandos.escolherRadioButtonNome(driver, "poseletro", posEletro);
        Comandos.escolherRadioButtonNome(driver, "antextintor", antExtintor);
        Comandos.escolherRadioButtonNome(driver, "posextintor", posExtintor);

        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("salvarChecklist")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"MyChecklists_filter\"]/label/input")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroChecklist\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoPlacaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoPlacaVazio";
				
		placa = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"MyChecklists_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO PLACA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroChecklist\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarPlacaInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPlacaInvalida";
				
		placa = "AHAHAHA";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"MyChecklists_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM PLACA INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroChecklist\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoDataVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDataVazio";
				
		data = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"MyChecklists_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroChecklist\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataInvalida";
				
		data = "abc";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"MyChecklists_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM DATA INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroChecklist\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataMaiorQueAtual(WebDriver driver) {
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
		
		String dataN = formatoData.format(calendario.getTime());
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataMaiorQueAtual";
		
        data = dataN;
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"MyChecklists_filter\"]/label/input")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA MAIOR QUE A ATUAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroChecklist\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoHoraVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoHoraVazio";
				
		hora = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"MyChecklists_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO HORA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroChecklist\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarHoraInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarHoraInvalida";
				
		hora = "abc";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"MyChecklists_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM HORA INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroChecklist\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoObservacoesVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoObservacoesVazio";
				
		observacoes = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"MyChecklists_filter\"]/label/input")));
			cadastrar.click();
            String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO OBSERVA��ES VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cadastroChecklist\"]/div/div/div/button"))).click();
		}
        return errorBuffer;
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroChecklist.dadosArquivo = dadosArquivo;
	}
}
