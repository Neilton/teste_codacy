package tests.gestorfrotas.menuresponsivo.cadastros;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class CadastroMotorista extends Cadastros{
	private static final String nomeTela = "Cadastro Motoristas";
	
	private static int count = 0;
	
	private static String foto = "C:/Users/Jos�Neilton/Desktop/183516818_2GG.jpg";
	private static String nome = "TESTE DO TESTE TESTANDO O TESTEIRO";
	private static String matricula = "10101010101";
	private static String nascimento = "01/01/2020";
	private static String lotacaoMot = "TESTE TESTE 123!"; 
	private static String intervalo = "02:00:00";
	private static String prestacaoContas = "15:00:00";
	private static String ferias = "01/02/2020";
	private static String plantao1 = "13:00:00";
	private static String plantao2 = "01:00:00";
	private static int indexAtivo = 1;
	
	private static String rg = "2654685";
	private static String cpf = "928.046.645-35";
	private static String cnh = "15151515151";
	private static String cnhEmissao = "15/01/2020";
	private static String cnhVencimento = "15/12/2020";
	private static String cnhLetras = "AB";

	private static String cep = "555295-410"; 
	private static String rua = "Rua Lamartine Babo"; 
	private static String numero = "153"; 
	private static String bairro = "magano"; 
	private static String cidade = "garanhuns"; 
	private static String estado = "PE"; 
	
	private static String telefone = "(51)5151-5151"; 
	private static String celular = "(66)15616-1616"; 
	private static String email = "teste@teste.com"; 

	private static int indexTipo = 1;
	private static String codigo = "52a1f5a1sf5sa1faa"+count; 
	private static String senha = "123456"; 

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		foto = "C:/Users/Jos�Neilton/Desktop/183516818_2GG.jpg";
		nome = "TESTE DO TESTE TESTANDO O TESTEIRO";
		lotacaoMot = "TESTE TESTE 123!"; 
		ferias = "01/02/2020";
		matricula = "10101010101";
		intervalo = "02:00:00";
		plantao1 = "13:00:00";
		plantao2 = "01:00:00";
		prestacaoContas = "15:00:00";
		nascimento = "01/01/2020";
		indexAtivo = 1;
		
		rg = "2654685";
		cpf = "928.046.645-35";
		cnh = "15151515151";
		cnhEmissao = "15/01/2020";
		cnhVencimento = "15/12/2020";
		cnhLetras = "AB";
		
		cep = "555295-410"; 
		rua = "Rua Lamartine Babo"; 
		numero = "153"; 
		bairro = "magano"; 
		cidade = "garanhuns"; 
		estado = "PE"; 
		
		telefone = "(51)5151-5151"; 
		celular = "(66)15616-1616"; 
		email = "teste@teste.com"; 

		indexTipo = 1;
		codigo = "52a1f5a1sf5sa1faa"+count; 
		senha = "123456"; 
	}

	public CadastroMotorista(WebDriver driver) {
		wait = new WebDriverWait(driver, 60);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		clear();
		
		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		
		errorBuffer.append(cadastrarCampoNomeVazio(driver)+"\n"); //teste com campo nome vazio
		
		errorBuffer.append(cadastrarCampoMatriculaVazio(driver)+"\n"); //teste com campo matricula vazio
		errorBuffer.append(cadastrarMatriculaInvalido(driver)+"\n"); //teste com matricula invalido
		
		errorBuffer.append(cadastrarCampoNascimentoVazio(driver)+"\n"); //teste com campo data de nascimento vazio
		errorBuffer.append(cadastrarNascimentoInvalida(driver)+"\n"); //teste com data de nascimento inv�lida
		
		errorBuffer.append(cadastrarCampoLotacaoMotVazio(driver)+"\n"); //teste com campo lota��o do motorista vazio
		
		errorBuffer.append(cadastrarCampoIntervaloVazio(driver)+"\n"); //teste com campo intervalo vazio
		errorBuffer.append(cadastrarIntervaloInvalido(driver)+"\n"); //teste com intervalo inv�lido
		
		errorBuffer.append(cadastrarCampoPrestacaoContasVazio(driver)+"\n"); //teste com campo presta��o de contas vazio
		errorBuffer.append(cadastrarPrestacaoContasInvalido(driver)+"\n"); //teste com presta��o de contas inv�lido
		
		errorBuffer.append(cadastrarCampoFeriasVazio(driver)+"\n"); //teste com campo f�rias vazio
		errorBuffer.append(cadastrarFeriasInvalida(driver)+"\n"); //teste com f�rias inv�lida

		errorBuffer.append(cadastrarCampoPlantao1Vazio(driver)+"\n"); //teste com campo plant�o 1 vazio
		errorBuffer.append(cadastrarPlantao1Invalido(driver)+"\n"); //teste com plant�o 1 inv�lida

		errorBuffer.append(cadastrarCampoPlantao2Vazio(driver)+"\n"); //teste com campo plant�o 2 vazio
		errorBuffer.append(cadastrarPlantao2Invalido(driver)+"\n"); //teste com plant�o 2 inv�lida
		
		errorBuffer.append(cadastrarCampoRgVazio(driver)+"\n"); //teste com campo rg vazio
		errorBuffer.append(cadastrarRgInvalido(driver)+"\n"); //teste com rg inv�lida
		
		errorBuffer.append(cadastrarCampoCpfVazio(driver)+"\n"); //teste com campo cpf vazio
		errorBuffer.append(cadastrarCpfInvalido(driver)+"\n"); //teste com cpf inv�lida
		
		errorBuffer.append(cadastrarCampoCnhVazio(driver)+"\n"); //teste com campo cnh vazio
		errorBuffer.append(cadastrarCnhInvalido(driver)+"\n"); //teste com cnh inv�lida
		
		errorBuffer.append(cadastrarCampoCnhEmissaoVazio(driver)+"\n"); //teste com campo cnh emiss�o vazio
		errorBuffer.append(cadastrarCnhEmissaoInvalida(driver)+"\n"); //teste com cnh emiss�o inv�lida
		
		errorBuffer.append(cadastrarCampoCnhVencimentoVazio(driver)+"\n"); //teste com campo cnh vencimento vazio
		errorBuffer.append(cadastrarCnhVencimentoInvalida(driver)+"\n"); //teste com cnh vencimento inv�lida
		
		errorBuffer.append(cadastrarCampoCnhLetrasVazio(driver)+"\n"); //teste com campo cnh letras vazio
		errorBuffer.append(cadastrarCnhLetrasInvalida(driver)+"\n"); //teste com cnh letras inv�lida
		
		errorBuffer.append(cadastrarCampoCepVazio(driver)+"\n"); //teste com campo cep vazio
		errorBuffer.append(cadastrarCepInvalida(driver)+"\n"); //teste com cep inv�lida
		
		errorBuffer.append(cadastrarCampoRuaVazio(driver)+"\n"); //teste com campo rua vazio
		
		errorBuffer.append(cadastrarCampoNumeroVazio(driver)+"\n"); //teste com campo numero vazio
		errorBuffer.append(cadastrarNumeroInvalida(driver)+"\n"); //teste com numero inv�lida
		
		errorBuffer.append(cadastrarCampoBairroVazio(driver)+"\n"); //teste com campo bairro vazio
		
		errorBuffer.append(cadastrarCampoCidadeVazio(driver)+"\n"); //teste com campo cidade vazio

		errorBuffer.append(cadastrarCampoEstadoVazio(driver)+"\n"); //teste com campo estado vazio
		
		errorBuffer.append(cadastrarCampoTelefoneVazio(driver)+"\n"); //teste com campo telefone vazio
		errorBuffer.append(cadastrarTelefoneInvalida(driver)+"\n"); //teste com telefone inv�lida
		
		errorBuffer.append(cadastrarCampoCelularVazio(driver)+"\n"); //teste com campo celular vazio
		errorBuffer.append(cadastrarCelularInvalida(driver)+"\n"); //teste com celular inv�lida
		
		errorBuffer.append(cadastrarCampoEmailVazio(driver)+"\n"); //teste com campo email vazio
		errorBuffer.append(cadastrarEmailInvalida(driver)+"\n"); //teste com email inv�lida
		
		errorBuffer.append(cadastrarCampoCodigoVazio(driver)+"\n"); //teste com campo codigo vazio

		errorBuffer.append(cadastrarCampoSenhaVazio(driver)+"\n"); //teste com campo senha vazio

		setDadosArquivo(errorBuffer);

	}
	
	public static void escolherCadastro(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[15]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  

        cadastrar(driver);
	}
	
	public static boolean cadastrar(WebDriver driver) {
		boolean erro = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas

        //Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/div/a")));
        novo.click();
        
        WebElement fotoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("img")));
        fotoE.sendKeys(foto);
        
        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nome")));
        nomeE.clear();
        nomeE.sendKeys(nome);
        
        WebElement matriculaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("matricula")));
        matriculaE.clear();
        matriculaE.sendKeys(matricula);
        
        WebElement nascimentoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nascimento")));
        nascimentoE.clear();
        nascimentoE.sendKeys(nascimento);
        
        WebElement lotacaoMotE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("lotacao")));
        lotacaoMotE.clear();
        lotacaoMotE.sendKeys(lotacaoMot);
          
        WebElement intervaloE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tempo_intervalo_refeicao")));
        intervaloE.clear();
        intervaloE.sendKeys(intervalo);
        
        WebElement prestacaoContasE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tempo_prestacao_contas")));
        prestacaoContasE.clear();
        prestacaoContasE.sendKeys(prestacaoContas);

        WebElement feriasE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ferias")));
        feriasE.clear();
        feriasE.sendKeys(ferias);
                
        WebElement plantao1E = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("plant1")));
        plantao1E.clear();
        plantao1E.sendKeys(plantao1);
        
        WebElement plantao2E = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("plant2")));
        plantao2E.clear();
        plantao2E.sendKeys(plantao2);
                
        Comandos.escolherRadioButtonNome(driver, "ativo", indexAtivo);
        
        WebElement rgE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("rg")));
        rgE.clear();
        rgE.sendKeys(rg);
        
        WebElement cpfE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cpf")));
        cpfE.clear();
        cpfE.sendKeys(cpf);
        
        WebElement cnhE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cnh")));
        cnhE.clear();
        cnhE.sendKeys(cnh);
        
        WebElement cnhEmissaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cnh_emissao")));
        cnhEmissaoE.clear();
        cnhEmissaoE.sendKeys(cnhEmissao);
        
        WebElement cnhVencimentoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cnh_vencimento")));
        cnhVencimentoE.clear();
        cnhVencimentoE.sendKeys(cnhVencimento);
        
        WebElement cnhLetrasE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cnh_letra")));
        cnhLetrasE.clear();
        cnhLetrasE.sendKeys(cnhLetras);
        
        WebElement cepE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cep")));
        cepE.clear();
        cepE.sendKeys(cep);
        
        WebElement ruaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("rua")));
        ruaE.clear();
        ruaE.sendKeys(rua);
        
        WebElement numeroE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("numero")));
        numeroE.clear();
        numeroE.sendKeys(numero);
        
        WebElement bairroE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("bairro")));
        bairroE.clear();
        bairroE.sendKeys(bairro);
        
        WebElement cidadeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cidade")));
        cidadeE.clear();
        cidadeE.sendKeys(cidade);
        
        WebElement estadoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("uf")));
        estadoE.click();
        estadoE.sendKeys(estado,Keys.ENTER);
        
        WebElement telefoneE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("telefone[numero]")));
        telefoneE.clear();
        telefoneE.sendKeys(telefone);
        
        WebElement celularE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("celular[numero]")));
        celularE.clear();
        celularE.sendKeys(celular);
        
        WebElement emailE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("email")));
        emailE.clear();
        emailE.sendKeys(email);
        
        Comandos.escolherRadioButtonNome(driver, "chaveiro_tipo", indexTipo);

        WebElement codigoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("chaveiro_serial")));
        codigoE.clear();
        codigoE.sendKeys(codigo);
        
        WebElement senhaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("senha_teclado")));
        senhaE.clear();
        senhaE.sendKeys(senha);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btSalvar")));
        botaoSubmit.click();
        
        count++;
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return erro;
	}
	
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoMatriculaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoMatriculaVazio";
				
		matricula = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO MATRICULA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarMatriculaInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarMatriculaInvalido";
				
		matricula = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM MATRICULA INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoIntervaloVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoIntervaloVazio";
				
		intervalo = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO INTERVALO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarIntervaloInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarIntervaloInvalido";
				
		intervalo = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM INTERVALO INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoPlantao1Vazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoPlantao1Vazio";
				
		plantao1 = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO PLANT�O 1 VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarPlantao1Invalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPlantao1Invalido";
				
		plantao1 = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM PLANT�O 1 INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoPlantao2Vazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoPlantao2Vazio";
				
		plantao2 = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO PLANT�O 2 VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarPlantao2Invalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPlantao2Invalido";
				
		plantao2 = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM PLANT�O 2 INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoRgVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoRgVazio";
				
		rg = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO RG VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarRgInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarRgInvalido";
				
		rg = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM RG INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCpfVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCpfVazio";
				
		cpf = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CPF VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCpfInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCpfInvalido";
				
		cpf = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CPF INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCnhVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCnhVazio";
				
		cnh = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CNH VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCnhInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCnhInvalido";
				
		cnh = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CNH INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoLotacaoMotVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoLotacaoMotVazio";
				
		lotacaoMot = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO LOTA��O DO MOTORISTA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoPrestacaoContasVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoPrestacaoContasVazio";
				
		prestacaoContas = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO PRESTA��O DE CONTAS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarPrestacaoContasInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPrestacaoContasInvalido";
				
		prestacaoContas = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM PRESTA��O DE CONTAS INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoNascimentoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoNascimentoVazio";
				
		nascimento = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA DE NASCIMENTO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarNascimentoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarNascimentoInvalida";
				
		nascimento = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DE NASCIMENTO INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoFeriasVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoFeriasVazio";
				
		ferias = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO F�RIAS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarFeriasInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarFeriasInvalida";
				
		ferias = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM F�RIAS INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCnhEmissaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCnhEmissaoVazio";
				
		cnhEmissao = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CHN EMISS�O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCnhEmissaoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCnhEmissaoInvalida";
				
		cnhEmissao = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CHN EMISS�O INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCnhVencimentoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCnhVencimentoVazio";
				
		cnhVencimento = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CHN VENCIMENTO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCnhVencimentoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCnhVencimentoInvalida";
				
		cnhVencimento = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CHN VENCIMENTO INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCnhLetrasVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCnhLetrasVazio";
				
		cnhLetras = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CHN LETRAS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCnhLetrasInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCnhLetrasInvalida";
				
		cnhLetras = "123!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CHN LETRAS INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoEstadoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoEstadoVazio";
				
		estado = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO ESTADO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoNumeroVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoNumeroVazio";
				
		numero = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO N�MERO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarNumeroInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarNumeroInvalida";
				
		numero = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM N�MERO INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoRuaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoRuaVazio";
				
		rua = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO RUA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoCidadeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCidadeVazio";
				
		cidade = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CIDADE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoBairroVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoBairroVazio";
				
		bairro = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO BAIRRO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoCepVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCepVazio";
				
		cep = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CEP VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCepInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCepInvalida";
				
		cep = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CEP INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoSenhaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoSenhaVazio";
				
		senha = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO SENHA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCodigoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCodigoVazio";
				
		codigo = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO C�DIGO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoEmailVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoEmailVazio";
				
		email = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO EMAIL VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarEmailInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarEmailInvalida";
				
		email = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM EMAIL INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCelularVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCelularVazio";
				
		celular = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CELULAR VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCelularInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCelularInvalida";
				
		celular = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CELULAR INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoTelefoneVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoTelefoneVazio";
				
		telefone = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO TELEFONE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarTelefoneInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarTelefoneInvalida";
				
		telefone = "abc!";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM TELEFONE INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoNomeVazio";
				
		nome = "";
		
		escolherCadastro(driver);
		
		try {
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.id("alert"))).getText().equals("Cadastro de motorista realizado com sucesso!"));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO NOME VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroMotorista.dadosArquivo = dadosArquivo;
	}
}
