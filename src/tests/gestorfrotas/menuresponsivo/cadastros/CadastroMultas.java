package tests.gestorfrotas.menuresponsivo.cadastros;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;
import tests.util.Variaveis;

public class CadastroMultas extends Cadastros{
	private static final String placaTela = "Cadastro Motoristas";
	
	private static int count = 0;
	
	private static String placa = "ACK0588";
	private static String motorista = "teste gustavo";
	
	private static String dataInfracao = "01/01/2020";
	private static String enquadramento = "TESTE TESTE 123!"; 
	private static String dataVerificacao = "01/02/2020";
	
	private static String ait = "2654685";
	private static String idEquipamento = "92804664535";
	private static String nAgente = "15151515151";
	private static String dataEmissao = "15/01/2020";
	private static String velPermitida = "123";

	private static String local = "Local Lamartine Babo"; 
	private static String velAferida = "153"; 
	private static String tipoInfracao = "magano"; 
	private static String baseLegal = "garanhuns"; 
	
	private static String observacao = "teste@teste.com"; 

	private static String descricao = "52a1f5a1sf5sa1faa"+count; 
	private static String pontos = "12"; 

	private static JavascriptExecutor js = null; //Executor de Javascript
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		placa = "ACK0588";
		enquadramento = "TESTE TESTE 123!"; 
		dataVerificacao = "01/02/2020";
		motorista = "teste gustavo";
		dataInfracao = "01/01/2020";
		
		ait = "2654685";
		idEquipamento = "92804664535";
		nAgente = "15151515151";
		dataEmissao = "15/01/2020";
		velAferida = "123";
		
		local = "Local Lamartine Babo"; 
		velPermitida = "153"; 
		tipoInfracao = "magano"; 
		baseLegal = "garanhuns"; 
		
		observacao = "teste@teste.com"; 

		descricao = "52a1f5a1sf5sa1faa"+count; 
		pontos = "12"; 
	}
	
	public static void clearEdicao() {
		placa = null;
		enquadramento = null; 
		dataVerificacao = null;
		motorista = null;
		dataInfracao = null;
		
		ait = null;
		idEquipamento = null;
		nAgente = null;
		dataEmissao = null;
		velAferida = null;
		
		local = null; 
		velPermitida = null; 
		tipoInfracao = null; 
		baseLegal = null; 
		
		observacao = null; 

		descricao = null; 
		pontos = null; 
	}
	
	public static StringBuffer testarCadastro(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\nCADASTRO\n");
		
		clear();
		
		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		
		errorBuffer.append(cadastrarCampoPlacaVazio(driver)+"\n"); //teste com campo placa vazio
		
		errorBuffer.append(cadastrarCampoMotoristaVazio(driver)+"\n"); //teste com campo motorista vazio
		
		errorBuffer.append(cadastrarCampoDataInfracaoVazio(driver)+"\n"); //teste com campo data de dataInfracao vazio
		errorBuffer.append(cadastrarDataInfracaoInvalida(driver)+"\n"); //teste com data de dataInfracao inv�lida
		
		errorBuffer.append(cadastrarCampoEnquadramentoVazio(driver)+"\n"); //teste com campo enquadramento vazio
		
		errorBuffer.append(cadastrarCampoDataVerificacaoVazio(driver)+"\n"); //teste com campo f�rias vazio
		errorBuffer.append(cadastrarDataVerificacaoInvalida(driver)+"\n"); //teste com f�rias inv�lida

		errorBuffer.append(cadastrarCampoAitVazio(driver)+"\n"); //teste com campo ait vazio
		
		errorBuffer.append(cadastrarCampoIdEquipamentoVazio(driver)+"\n"); //teste com campo idEquipamento vazio
		errorBuffer.append(cadastrarIdEquipamentoInvalido(driver)+"\n"); //teste com idEquipamento inv�lida
		errorBuffer.append(cadastrarIdEquipamentoNegativo(driver)+"\n"); //teste com idEquipamento negativo

		errorBuffer.append(cadastrarCampoEnquadramentoVazio(driver)+"\n"); //teste com campo nAgente vazio
		
		errorBuffer.append(cadastrarCampoDataEmissaoVazio(driver)+"\n"); //teste com campo nAgente emiss�o vazio
		errorBuffer.append(cadastrarDataEmissaoInvalida(driver)+"\n"); //teste com nAgente emiss�o inv�lida
		
		errorBuffer.append(cadastrarCampoVelAferidaVazio(driver)+"\n"); //teste com campo velocidade aferida vazio
		errorBuffer.append(cadastrarVelAferidaInvalida(driver)+"\n"); //teste com velocidade aferida inv�lida
		errorBuffer.append(cadastrarVelAferidaNegativa(driver)+"\n"); //teste com velocidade aferida negativa

		errorBuffer.append(cadastrarCampoLocalVazio(driver)+"\n"); //teste com campo local vazio
		
		errorBuffer.append(cadastrarCampoVelPermitidaVazio(driver)+"\n"); //teste com campo velPermitida vazio
		errorBuffer.append(cadastrarVelPermitidaInvalida(driver)+"\n"); //teste com velPermitida inv�lida
		errorBuffer.append(cadastrarVelPermitidaNegativa(driver)+"\n"); //teste com velPermitida negativa

		errorBuffer.append(cadastrarCampoTipoInfracaoVazio(driver)+"\n"); //teste com campo tipoInfracao vazio

		errorBuffer.append(cadastrarCampoBaseLegalVazio(driver)+"\n"); //teste com campo baseLegal vazio

		errorBuffer.append(cadastrarCampoObservacaoVazio(driver)+"\n"); //teste com campo observacao vazio
		
		errorBuffer.append(cadastrarCampoDescricaoVazio(driver)+"\n"); //teste com campo descricao vazio

		errorBuffer.append(cadastrarCampoPontosVazio(driver)+"\n"); //teste com campo pontos vazio
		errorBuffer.append(cadastrarPontosInvalido(driver)+"\n"); //teste com pontos invalido
		
		return errorBuffer;
	}
	
	public static StringBuffer testarEdicao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\nEDI��O\n");
		
		clearEdicao();
		
		escolherCadastroEdicao(driver);
		
		errorBuffer.append(editarCampoPlaca(driver)+"\n"); //teste com campo placa 
		errorBuffer.append(editarCampoPlacaVazio(driver)+"\n"); //teste com campo placa vazio
		
		errorBuffer.append(editarCampoMotorista(driver)+"\n"); //teste com campo motorista 
		errorBuffer.append(editarCampoMotoristaVazio(driver)+"\n"); //teste com campo motorista vazio
		
		errorBuffer.append(editarCampoDataInfracao(driver)+"\n"); //teste com campo data de dataInfracao 
		errorBuffer.append(editarCampoDataInfracaoVazio(driver)+"\n"); //teste com campo data de dataInfracao vazio
		errorBuffer.append(editarDataInfracaoInvalida(driver)+"\n"); //teste com data de dataInfracao inv�lida
		
		errorBuffer.append(editarCampoEnquadramento(driver)+"\n"); //teste com campo enquadramento 
		errorBuffer.append(editarCampoEnquadramentoVazio(driver)+"\n"); //teste com campo enquadramento vazio
		
		errorBuffer.append(editarCampoDataVerificacao(driver)+"\n"); //teste com campo f�rias 
		errorBuffer.append(editarCampoDataVerificacaoVazio(driver)+"\n"); //teste com campo f�rias vazio
		errorBuffer.append(editarDataVerificacaoInvalida(driver)+"\n"); //teste com f�rias inv�lida

		errorBuffer.append(editarCampoAit(driver)+"\n"); //teste com campo ait 
		errorBuffer.append(editarCampoAitVazio(driver)+"\n"); //teste com campo ait vazio
		
		errorBuffer.append(editarCampoIdEquipamento(driver)+"\n"); //teste com campo idEquipamento 
		errorBuffer.append(editarCampoIdEquipamentoVazio(driver)+"\n"); //teste com campo idEquipamento vazio
		errorBuffer.append(editarIdEquipamentoInvalido(driver)+"\n"); //teste com idEquipamento inv�lida
		errorBuffer.append(editarIdEquipamentoNegativo(driver)+"\n"); //teste com idEquipamento negativo

		errorBuffer.append(editarCampoNAgente(driver)+"\n"); //teste com campo nAgente 
		errorBuffer.append(editarCampoNAgenteVazio(driver)+"\n"); //teste com campo nAgente vazio
		
		errorBuffer.append(editarCampoDataEmissao(driver)+"\n"); //teste com campo nAgente emiss�o 
		errorBuffer.append(editarCampoDataEmissaoVazio(driver)+"\n"); //teste com campo nAgente emiss�o vazio
		errorBuffer.append(editarDataEmissaoInvalida(driver)+"\n"); //teste com nAgente emiss�o inv�lida
		
		errorBuffer.append(editarCampoVelAferida(driver)+"\n"); //teste com campo velocidade aferida 
		errorBuffer.append(editarCampoVelAferidaVazio(driver)+"\n"); //teste com campo velocidade aferida vazio
		errorBuffer.append(editarVelAferidaInvalida(driver)+"\n"); //teste com velocidade aferida inv�lida
		errorBuffer.append(editarVelAferidaNegativa(driver)+"\n"); //teste com velocidade aferida negativa

		errorBuffer.append(editarCampoLocal(driver)+"\n"); //teste com campo local 
		errorBuffer.append(editarCampoLocalVazio(driver)+"\n"); //teste com campo local vazio
		
		errorBuffer.append(editarCampoVelPermitida(driver)+"\n"); //teste com campo velPermitida 
		errorBuffer.append(editarCampoVelPermitidaVazio(driver)+"\n"); //teste com campo velPermitida vazio
		errorBuffer.append(editarVelPermitidaInvalida(driver)+"\n"); //teste com velPermitida inv�lida
		errorBuffer.append(editarVelPermitidaNegativa(driver)+"\n"); //teste com velPermitida negativa

		errorBuffer.append(editarCampoTipoInfracao(driver)+"\n"); //teste com campo tipoInfracao 
		errorBuffer.append(editarCampoTipoInfracaoVazio(driver)+"\n"); //teste com campo tipoInfracao vazio

		errorBuffer.append(editarCampoBaseLegal(driver)+"\n"); //teste com campo baseLegal 
		errorBuffer.append(editarCampoBaseLegalVazio(driver)+"\n"); //teste com campo baseLegal vazio

		errorBuffer.append(editarCampoObservacao(driver)+"\n"); //teste com campo observacao 
		errorBuffer.append(editarCampoObservacaoVazio(driver)+"\n"); //teste com campo observacao vazio
		
		errorBuffer.append(editarCampoDescricao(driver)+"\n"); //teste com campo descricao 
		errorBuffer.append(editarCampoDescricaoVazio(driver)+"\n"); //teste com campo descricao vazio

		errorBuffer.append(editarCampoPontos(driver)+"\n"); //teste com campo pontos 
		errorBuffer.append(editarCampoPontosVazio(driver)+"\n"); //teste com campo pontos vazio
		errorBuffer.append(editarPontosInvalido(driver)+"\n"); //teste com pontos invalido
		
		return errorBuffer;
	}

	public CadastroMultas(WebDriver driver) {
		wait = new WebDriverWait(driver, 70);
		js = ((JavascriptExecutor) driver);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+placaTela+"\n");
		
		//errorBuffer.append(testarCadastro(driver));
		errorBuffer.append(testarEdicao(driver));

		
		setDadosArquivo(errorBuffer);

	}
	
	public static boolean escolherCadastro(WebDriver driver) {
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[16]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  

        return cadastrar(driver);
	}
	
public static void escolherCadastroEdicao(WebDriver driver) {
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[16]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  

	}
	
	public static boolean cadastrar(WebDriver driver) {
		boolean tudoCerto = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
               
        WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[1]/a")));
        aba.click();
        
        WebElement placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placas_chosen\"]/ul/li/input")));
        placaE.clear();
        placaE.sendKeys(placa,Keys.ENTER);
        
        WebElement motoristaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"motoristas_chosen\"]/ul/li/input")));
        motoristaE.clear();
        motoristaE.sendKeys(motorista,Keys.ENTER);
        
        WebElement dataInfracaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_infracao")));
        dataInfracaoE.clear();
        dataInfracaoE.sendKeys(dataInfracao);
        
        WebElement enquadramentoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("enquadramento")));
        enquadramentoE.clear();
        enquadramentoE.sendKeys(enquadramento);
          
        WebElement dataVerificacaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_verific_eqp")));
        dataVerificacaoE.clear();
        dataVerificacaoE.sendKeys(dataVerificacao);
                
        WebElement rgE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ait")));
        rgE.clear();
        rgE.sendKeys(ait);
        
        WebElement idEquipamentoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("id_eqp")));
        idEquipamentoE.clear();
        idEquipamentoE.sendKeys(idEquipamento);
        
        WebElement nAgenteE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("agente")));
        nAgenteE.clear();
        nAgenteE.sendKeys(nAgente);
        
        WebElement dataEmissaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_emissao")));
        dataEmissaoE.clear();
        dataEmissaoE.sendKeys(dataEmissao);
        
        WebElement velAferidaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("vel_aferida")));
        velAferidaE.clear();
        velAferidaE.sendKeys(velAferida);
        
        WebElement ruaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("local_infracao")));
        ruaE.clear();
        ruaE.sendKeys(local);
        
        WebElement velPermitidaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("vel_permitida")));
        velPermitidaE.clear();
        velPermitidaE.sendKeys(velPermitida);
        
        WebElement tipoInfracaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tipo")));
        tipoInfracaoE.clear();
        tipoInfracaoE.sendKeys(tipoInfracao);
        
        WebElement baseLegalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("base_legal")));
        baseLegalE.clear();
        baseLegalE.sendKeys(baseLegal);
                
        WebElement observacaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("observacao")));
        observacaoE.clear();
        observacaoE.sendKeys(observacao);
        
        WebElement descricaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("infracao")));
        descricaoE.clear();
        descricaoE.sendKeys(descricao);
        
        WebElement pontosE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("pontos")));
        pontosE.clear();
        pontosE.sendKeys(pontos);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btSalvar")));
        botaoSubmit.click();
        
        count++;
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        if(alert.getText().equals("Multa cadastrada com sucesso!"))
	        	tudoCerto = true;
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	e.printStackTrace();
        }
        return tudoCerto;
	}
	
	public static void editar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas

        // clica na aba escolhida
        WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
        aba.click();
        
        // clica em editar multa
        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
        editar.click();
        
        if(placa != null) {
        	WebElement placaE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"placas_chosen\"]/ul/li[1]/a")));
            placaE.click();
            
            placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placas_chosen\"]/ul/li/input")));
            
            placaE.sendKeys("              ",placa,"            ",Keys.ENTER);
            try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        //n�o funciona, portanto sempre envio um motorista
        //if(motorista != null) {
	        WebElement motoristaE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"placas_chosen\"]/ul/li[1]/a")));
	        //motoristaE.click();
	        
	        motoristaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"motoristas_chosen\"]/ul/li/input")));
	        
	        //motoristaE.sendKeys("            ",motorista,Keys.ENTER);
	        motoristaE.sendKeys("            ","teste gustavo","       ",Keys.ENTER);
	        try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        //}
        
        if(dataInfracao != null) {
	        WebElement dataInfracaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_infracao")));
	        dataInfracaoE.clear();
	        dataInfracaoE.sendKeys(dataInfracao);
        }
        
        if(enquadramento != null) {
	        WebElement enquadramentoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("enquadramento")));
	        enquadramentoE.clear();
	        enquadramentoE.sendKeys(enquadramento);
        }
        
        if(dataVerificacao != null) {
	        WebElement dataVerificacaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_verific_eqp")));
	        dataVerificacaoE.clear();
	        dataVerificacaoE.sendKeys(dataVerificacao);
        }
        
        if(ait != null) {
	        WebElement rgE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ait")));
	        rgE.clear();
	        rgE.sendKeys(ait);
        }
        
        if(idEquipamento != null) {
	        WebElement idEquipamentoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("id_eqp")));
	        idEquipamentoE.clear();
	        idEquipamentoE.sendKeys(idEquipamento);
        }
        
        if(nAgente != null) {
	        WebElement nAgenteE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("agente")));
	        nAgenteE.clear();
	        nAgenteE.sendKeys(nAgente);
        }
        
        if(dataEmissao != null) {
	        WebElement dataEmissaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_emissao")));
	        dataEmissaoE.clear();
	        dataEmissaoE.sendKeys(dataEmissao);
        }
        
        if(velAferida != null) {
	        WebElement velAferidaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("vel_aferida")));
	        velAferidaE.clear();
	        velAferidaE.sendKeys(velAferida);
        }
        
        if(local != null) {
	        WebElement ruaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("local_infracao")));
	        ruaE.clear();
	        ruaE.sendKeys(local);
        }
        
        if(velPermitida != null) {
	        WebElement velPermitidaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("vel_permitida")));
	        velPermitidaE.clear();
	        velPermitidaE.sendKeys(velPermitida);
        }
        
        if(tipoInfracao != null) {
	        WebElement tipoInfracaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tipo")));
	        tipoInfracaoE.clear();
	        tipoInfracaoE.sendKeys(tipoInfracao);
        }
        
        if(baseLegal != null) {
	        WebElement baseLegalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("base_legal")));
	        baseLegalE.clear();
	        baseLegalE.sendKeys(baseLegal);
        }
        
        if(observacao != null) {
	        WebElement observacaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("observacao")));
	        observacaoE.clear();
	        observacaoE.sendKeys(observacao);
        }
        
        if(descricao != null) {
	        WebElement descricaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("infracao")));
	        descricaoE.clear();
	        descricaoE.sendKeys(descricao);
        }
        
        if(pontos != null) {
	        WebElement pontosE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("pontos")));
	        pontosE.clear();
	        pontosE.sendKeys(pontos);
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btSalvar")));
        botaoSubmit.click();
        
        count++;
        
        clearEdicao();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	e.printStackTrace();
        }
	}
	
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		
		
				
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoMotoristaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoMotoristaVazio";
				
		motorista = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO MOTORISTA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	// Nenhuma edi��o de motorista funciona
	public static StringBuffer editarCampoMotorista(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoMotorista";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO MOTORISTA! ";		
		
		motorista = "SAULO SILVA MENDES";
		String novoValor = motorista;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"motoristas_chosen\"]/ul/li[1]/span")));
	        
	        Assert.assertEquals(novoValor,elemento.getText());
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoMotoristaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoMotoristaVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO MOTORISTA VAZIO! ";
		
		motorista = "";
		String novoValor = motorista;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"motoristas_chosen\"]/ul/li[1]/span")));
	        
        	
	        if(!novoValor.equals(elemento.getText()))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoAitVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoAitVazio";
				
		ait = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO AIT VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAit(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAit";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO AIT! ";		
		
		ait = "580tj";
		String novoValor = ait;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ait")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAitVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAitVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO AIT VAZIO! ";
		
		ait = "";
		String novoValor = ait;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ait")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoIdEquipamentoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoIdEquipamentoVazio";
				
		idEquipamento = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO ID DO EQUIPAMENTO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarIdEquipamentoInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarIdEquipamentoInvalido";
				
		idEquipamento = "abc!";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM ID DO EQUIPAMENTO INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarIdEquipamentoNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarIdEquipamentoNegativo";
				
		idEquipamento = "-95145";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM ID DO EQUIPAMENTO NEGATIVO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoIdEquipamento(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoIdEquipamento";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO ID DO EQUIPAMENTO! ";		
		
		idEquipamento = "117";
		String novoValor = idEquipamento;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("id_eqp")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoIdEquipamentoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoIdEquipamentoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO ID DO EQUIPAMENTO VAZIO! ";
		
		idEquipamento = "";
		String novoValor = idEquipamento;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("id_eqp")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarIdEquipamentoInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarIdEquipamentoInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM ID DO EQUIPAMENTO INV�LIDO! ";
		
		idEquipamento = "abc!";
		String novoValor = idEquipamento;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("id_eqp")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarIdEquipamentoNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarIdEquipamentoNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM ID DO EQUIPAMENTO NEGATIVO! ";
		
		idEquipamento = "-117";
		String novoValor = idEquipamento;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("id_eqp")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoNAgenteVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoNAgenteVazio";
				
		nAgente = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO N. DO AGENTE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoNAgente(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoNAgente";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO N. DO AGENTE! ";		
		
		nAgente = "117";
		String novoValor = nAgente;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("agente")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoNAgenteVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoNAgenteVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO N. DO AGENTE VAZIO! ";
		
		nAgente = "";
		String novoValor = nAgente;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("agente")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoEnquadramentoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoEnquadramentoVazio";
				
		enquadramento = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO ENQUADRAMENTO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoEnquadramento(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoEnquadramento";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO ENQUADRAMENTO! ";		
		
		enquadramento = "enquadramento";
		String novoValor = enquadramento;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("enquadramento")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoEnquadramentoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoEnquadramentoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO ENQUADRAMENTO VAZIO! ";
		
		enquadramento = "";
		String novoValor = enquadramento;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("enquadramento")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDataInfracaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDataInfracaoVazio";
				
		dataInfracao = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA DE DATA DA INFRA��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataInfracaoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataInfracaoInvalida";
				
		dataInfracao = "abc!";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DE DATA DA INFRA��O INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDataInfracao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataInfracao";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO DATA DA INFRA��O! ";		
		
		dataInfracao = "02/04/2020";
		String novoValor = dataInfracao;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_infracao")));
	        
	        Assert.assertEquals(novoValor,Variaveis.getFormatodata().parse(elemento.getAttribute("value")));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDataInfracaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataInfracaoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO DATA DA INFRA��O VAZIO! ";
		
		dataInfracao = "";
		String novoValor = dataInfracao;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("data_infracao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarDataInfracaoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataInfracaoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM DATA DA INFRA��O INV�LIDA! ";
		
		dataInfracao = "abc!";
		String novoValor = dataInfracao;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("data_infracao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDataVerificacaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDataVerificacaoVazio";
				
		dataVerificacao = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA DE VERIFICA��O DE EQUIPAMENTO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataVerificacaoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataVerificacaoInvalida";
				
		dataVerificacao = "abc!";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DE VERIFICA��O DE EQUIPAMENTO INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDataVerificacao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataVerificacao";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO DATA DE VERIFICA��O DE EQUIPAMENTO! ";		
		
		dataVerificacao = "04/04/2020";
		String novoValor = dataVerificacao;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_verific_eqp")));
	        
	        Assert.assertEquals(novoValor,Variaveis.getFormatodata().parse(elemento.getAttribute("value")));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDataVerificacaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataVerificacaoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO DATA DE VERIFICA��O DE EQUIPAMENTO VAZIO! ";
		
		dataVerificacao = "";
		String novoValor = dataVerificacao;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("data_verific_eqp")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarDataVerificacaoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataVerificacaoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM DATA DE VERIFICA��O DE EQUIPAMENTO INV�LIDA! ";
		
		dataVerificacao = "abc!";
		String novoValor = dataVerificacao;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("data_verific_eqp")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDataEmissaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDataEmissaoVazio";
				
		dataEmissao = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DATA DE EMISS�O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarDataEmissaoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarDataEmissaoInvalida";
				
		dataEmissao = "abc!";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM DATA DE EMISS�O EMISS�O INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDataEmissao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataEmissao";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO DATA DE EMISS�O! ";		
		
		dataEmissao = "04/04/2020";
		String novoValor = dataEmissao;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_emissao")));
	        
	        Assert.assertEquals(novoValor,Variaveis.getFormatodata().parse(elemento.getAttribute("value")));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDataEmissaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataEmissaoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO DATA DE EMISS�O VAZIO! ";
		
		dataEmissao = "";
		String novoValor = dataEmissao;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("data_emissao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarDataEmissaoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataEmissaoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM DATA DA EMISS�O INV�LIDA! ";
		
		dataEmissao = "abc!";
		String novoValor = dataEmissao;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("data_emissao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoVelAferidaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoVelAferidaVazio";
				
		velAferida = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO VELOCIDADE AFERIDA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarVelAferidaInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarVelAferidaInvalida";
				
		velAferida = "abc!";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM VELOCIDADE AFERIDA INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarVelAferidaNegativa(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarVelAferidaNegativa";
				
		velAferida = "-56";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM VELOCIDADE AFERIDA NEGATIVA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoVelAferida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoVelAferida";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO VELOCIDADE AFERIDA! ";		
		
		velAferida = "117";
		String novoValor = velAferida;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("vel_aferida")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoVelAferidaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoVelAferidaVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO VELOCIDADE AFERIDA VAZIO! ";
		
		velAferida = "";
		String novoValor = velAferida;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("vel_aferida")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarVelAferidaInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarVelAferidaInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM VELOCIDADE AFERIDA INV�LIDA! ";
		
		velAferida = "abc!";
		String novoValor = velAferida;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("vel_aferida")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarVelAferidaNegativa(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarVelAferidaNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM VELOCIDADE AFERIDA NEGATIVA! ";
		
		velAferida = "-117";
		String novoValor = velAferida;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("vel_aferida")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoVelPermitidaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoVelPermitidaVazio";
				
		velPermitida = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO VELOCIDADE PERMITIDA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarVelPermitidaInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarVelPermitidaInvalida";
				
		velPermitida = "abc!";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM VELOCIDADE PERMITIDA INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarVelPermitidaNegativa(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarVelPermitidaInvalida";
				
		velPermitida = "-65";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM VELOCIDADE PERMITIDA NEGATIVA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoVelPermitida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoVelPermitida";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO VELOCIDADE PERMITIDA! ";		
		
		velPermitida = "117";
		String novoValor = velPermitida;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("vel_permitida")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoVelPermitidaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoVelPermitidaVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO VELOCIDADE PERMITIDA VAZIO! ";
		
		velPermitida = "";
		String novoValor = velPermitida;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("vel_permitida")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarVelPermitidaInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarVelPermitidaInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM VELOCIDADE PERMITIDA INV�LIDA! ";
		
		velPermitida = "abc!";
		String novoValor = velPermitida;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("vel_permitida")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarVelPermitidaNegativa(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarVelPermitidaNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM VELOCIDADE PERMITIDA NEGATIVA! ";
		
		velPermitida = "-117";
		String novoValor = velPermitida;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("vel_permitida")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoLocalVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoLocalVazio";
				
		local = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO LOCAL VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoLocal(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoLocal";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO LOCAL! ";		
		
		local = "local desconhecido";
		String novoValor = local;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("local_infracao")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoLocalVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoLocalVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO LOCAL VAZIO! ";
		
		local = "";
		String novoValor = local;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("local_infracao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoBaseLegalVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoBaseLegalVazio";
				
		baseLegal = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO BASE LEGAL VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoBaseLegal(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoBaseLegal";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO BASE LEGAL! ";		
		
		baseLegal = "local desconhecido";
		String novoValor = baseLegal;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("base_legal")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoBaseLegalVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoBaseLegalVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO BASE LEGAL VAZIO! ";
		
		baseLegal = "";
		String novoValor = baseLegal;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("base_legal")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoTipoInfracaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoTipoInfracaoVazio";
				
		tipoInfracao = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO TIPO DE INFRA��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTipoInfracao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTipoInfracao";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO TIPO DE INFRA��O! ";		
		
		tipoInfracao = "local desconhecido";
		String novoValor = tipoInfracao;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tipo")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTipoInfracaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTipoInfracaoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO TIPO DE INFRA��O VAZIO! ";
		
		tipoInfracao = "";
		String novoValor = tipoInfracao;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("tipo")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoPontosVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoPontosVazio";
				
		pontos = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO PONTOS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarPontosInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPontosInvalido";
				
		pontos = "abc!";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM PONTOS INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPontos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPontos";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO PONTOS! ";		
		
		pontos = "117";
		String novoValor = pontos;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("pontos")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPontosVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPontosVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO PONTOS VAZIO! ";
		
		pontos = "";
		String novoValor = pontos;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("pontos")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarPontosInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPontosVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO PONTOS VAZIO! ";
		
		pontos = "abc!";
		String novoValor = pontos;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("pontos")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoDescricaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoDescricaoVazio";
				
		descricao = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO DESCRI��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDescricao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDescricao";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO DESCRI��O DA INFRA��O! ";		
		
		descricao = "teste testando do teste testado testinho";
		String novoValor = descricao;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("infracao")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDescricaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDescricaoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO DESCRI��O DA INFRA��O VAZIO! ";
		
		descricao = "";
		String novoValor = descricao;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("infracao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoObservacaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoObservacaoVazio";
				
		observacao = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO OBSERVA��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoObservacao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoObservacao";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO OBSERVA��O! ";		
		
		observacao = "teste testando do teste testado testinho";
		String novoValor = observacao;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("observacao")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoObservacaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoObservacaoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO OBSERVA��O VAZIO! ";
		
		observacao = "";
		String novoValor = observacao;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("observacao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoPlacaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoPlacaVazio";
				
		placa = "";
		
		
		
		try {
			Assert.assertTrue(escolherCadastro(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO PLACA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPlaca(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPlaca";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO PLACA! ";		
		
		placa = "EEF-3680";
		String novoValor = placa;
		editar(driver);
				
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"placas_chosen\"]/ul/li[1]/span")));
	        
	        Assert.assertEquals(novoValor,elemento.getText());
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPlacaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPlacaVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO PLACA VAZIO! ";
		
		placa = "";
		String novoValor = placa;
		editar(driver);
		
		try {
			WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[2]/a")));
	        aba.click();
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaMultas\"]/tbody/tr[1]/td[10]/a")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"placas_chosen\"]/ul/li[1]/span")));
	        
        	
	        if(!novoValor.equals(elemento.getText()))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		
            
		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroMultas.dadosArquivo = dadosArquivo;
	}
}
