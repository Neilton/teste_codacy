package tests.gestorfrotas.menuresponsivo.cadastros;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class CadastroPontoInteresse extends Cadastros{
	private static final String nomeTela = "Cadastro �reas de Interesse - Pontos de Interesse";
	
	private static String latitude = "-8.8703787";
	private static String longitude = "-36.4607839";
	
	private static boolean botaoEnd = true;
	private static boolean botaoCoo = true;
	
	private static String nome = "ROTA NOVA";	
	private static String endereco = "Rua Paranatama";
	private static String bairro = "Novo Heli�polis";
	private static String cidade = "Garanhuns";
	private static String estado = "Pernambuco";
	private static String cep = "55298-140";
	private static String observacao = "TESTE DO TESTE TESTANDO";
	private static int indexCategoria = 0;
	
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
    static String windowHandle;

	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		latitude = "-8.8703787";
		longitude = "-36.4607839";
		botaoEnd = false;
		botaoCoo = false;

		nome = "ROTA NOVA";	
		endereco = "Rua Paranatama";
		bairro = "Novo Heli�polis";
		cidade = "Garanhuns";
		estado = "Pernambuco";
		cep = "55298-140";
		observacao = "TESTE DO TESTE TESTANDO";
		indexCategoria = 0;
	}

	public CadastroPontoInteresse(WebDriver driver) {
		wait = new WebDriverWait(driver, 10);
	    windowHandle = driver.getWindowHandle();
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		clear();
		escolherCadastro(driver);
		
		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		errorBuffer.append(cadastrarCampoLatitudeVazio(driver)+"\n"); //teste com campo latitude vazio
		errorBuffer.append(cadastrarLatitudeInvalida(driver)+"\n"); //teste com latitude invalida
		errorBuffer.append(cadastrarCampoLongitudeVazio(driver)+"\n"); //teste com campo longitude vazio
		errorBuffer.append(cadastrarLongitudeInvalida(driver)+"\n"); //teste com longitude invalida
		errorBuffer.append(cadastrarCampoNomeVazio(driver)+"\n"); //teste com campo nome vazio
		errorBuffer.append(cadastrarCampoEnderecoVazio(driver)+"\n"); //teste com campo endere�o vazio
		errorBuffer.append(cadastrarCampoBairroVazio(driver)+"\n"); //teste com campo bairro vazio
		errorBuffer.append(cadastrarCampoCidadeVazio(driver)+"\n"); //teste com campo cidade vazio
		errorBuffer.append(cadastrarCampoEstadoVazio(driver)+"\n"); //teste com campo estado vazio
		errorBuffer.append(cadastrarCampoCepVazio(driver)+"\n"); //teste com campo cep vazio
		errorBuffer.append(cadastrarCampoObservacaoVazio(driver)+"\n"); //teste com campo observacao vazio
		errorBuffer.append(cadastrarBotaoEndereco(driver)+"\n"); //teste utilizando o bot�o de buscar endere�o
		errorBuffer.append(cadastrarBotaoEnderecoSemLatitude(driver)+"\n"); //teste utilizando o bot�o de buscar endere�o faltando valores
		errorBuffer.append(cadastrarBotaoCoordenadas(driver)+"\n"); //teste utilizando bot�o de buscar coordenadas
		errorBuffer.append(cadastrarBotaoCoordenadasSemCampos(driver)+"\n"); //teste utilizando bot�o de buscar coordenadas faltando valores
		
		setDadosArquivo(errorBuffer);

	}
		
	public static void escolherCadastro(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[4]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
        
        //Abre a aba escolhida
        WebElement abaE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/ul/li[3]/a")));
        js.executeScript("arguments[0].click();", abaE);
	}
	
	public static void cadastrar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"interesses\"]/div[1]/a")));
        novo.click();
        
        //Vai para nova aba
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1)); 
        
    	WebElement latitudeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("latitude")));
    	latitudeE.clear();
    	latitudeE.sendKeys(latitude);
         
        WebElement longitudeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("longitude")));
        longitudeE.clear();
        longitudeE.sendKeys(longitude);
        
    	WebElement enderecoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("rua")));
    	enderecoE.clear();
    	enderecoE.sendKeys(endereco);
    
        WebElement bairroE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("bairro")));
        bairroE.clear();
        bairroE.sendKeys(bairro);
    
        WebElement cidadeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cidade")));
        cidadeE.clear();
        cidadeE.sendKeys(cidade);
    
        WebElement estadoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("estado")));
        estadoE.clear();
        estadoE.sendKeys(estado);
    
        WebElement cepE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cep")));
        cepE.clear();
        cepE.sendKeys(cep);
    
        if(botaoCoo) {
		    WebElement botaoCoo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/form/div[2]/div[12]/div/button")));
		    js.executeScript("arguments[0].click()", botaoCoo);
        }
 
	    if(botaoEnd) {
	        WebElement botaoEnd = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/form/div[2]/div[3]/div/button")));
	        js.executeScript("arguments[0].click()", botaoEnd);
        }
                
        WebElement baseE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("categoria")));
        if(indexCategoria >= 0) {
		    Select selectBaseE = new Select(baseE);
		    selectBaseE.selectByIndex(indexCategoria);
        }
        
        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nome")));
        nomeE.clear();
        nomeE.sendKeys(nome);
        
        WebElement obsE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("descricao")));
        obsE.clear();
        obsE.sendKeys(observacao);
                
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/form/div[2]/div[15]/button[1]")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
	        alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoLatitudeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoLatitudeVazio";
				
		latitude = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO LATITUDE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarLatitudeInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarLatitudeInvalida";
				
		latitude = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM LATITUDE INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoLongitudeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoLongitudeVazio";
				
		longitude = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO LONGITUDE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarLongitudeInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarLatitudeInvalida";
				
		longitude = "abc!";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM LATITUDE INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoNomeVazio";
				
		nome = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO NOME VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoEnderecoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoEnderecoVazio";
				
		endereco = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO ENDERE�O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoBairroVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoBairroVazio";
				
		bairro = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO BAIRRO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCidadeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCidadeVazio";
				
		cidade = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CIDADE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoEstadoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoEstadoVazio";
				
		estado = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO ESTADO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoCepVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoCepVazio";
				
		cep = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO CEP VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoObservacaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoObservacaoVazio";
				
		observacao = "";
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO OBSERVA��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarBotaoEndereco(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarBotaoEndereco";
				
		endereco = "";
		cidade = "";
		estado = "";
		bairro = "";
		cep = "";

		botaoEnd = true;
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
            String motivo = "CADASTRO N�O � EFETUADO CLICANDO-SE NO BOT�O BUSCAR ENDERE�O MESMO COM CAMPOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarBotaoEnderecoSemLatitude(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarBotaoEndereco";
				
		endereco = "";
		cidade = "";
		estado = "";
		bairro = "";
		cep = "";
		latitude = "";

		botaoEnd = true;
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO CLICANDO-SE NO BOT�O BUSCAR ENDERE�O MESMO SEM O CAMPO LATITUDE";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarBotaoCoordenadas(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarBotaoCoordenadas";
				
		longitude = "";
		latitude = "";

		botaoCoo = true;
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(Exception e) {
            String motivo = "CADASTRO N�O � EFETUADO CLICANDO-SE NO BOT�O BUSCAR COORDENADAS MESMO COM CAMPOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarBotaoCoordenadasSemCampos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarBotaoEndereco";
				
		endereco = "";
		cidade = "";
		estado = "";
		bairro = "";
		cep = "";
		latitude = "";
		longitude="";
		botaoCoo = true;
		
		cadastrar(driver);
		
		try {
			WebElement cadastrar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/button")));
			cadastrar.click();
			String motivo = "CADASTRO � EFETUADO CLICANDO-SE NO BOT�O BUSCAR ENDERE�O MESMO SEM O CAMPO LATITUDE";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

		}
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
			
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroPontoInteresse.dadosArquivo = dadosArquivo;
	}
	
}
