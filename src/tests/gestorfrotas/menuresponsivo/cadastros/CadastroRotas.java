package tests.gestorfrotas.menuresponsivo.cadastros;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class CadastroRotas extends Cadastros{
	private static final String nomeTela = "Cadastro �reas de Interesse - Rotas";
	
	private static String raioEscape = "100";
	private static String nome = "ROTA NOVA";
	private static String velMax = "50";
	private static int indexContatos = 1;
	private static ArrayList<String> rotas = new ArrayList<String>();
	
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
    static String windowHandle;

	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		raioEscape = "100";
		nome = "ROTA NOVA";
		velMax = "50";
		indexContatos = 1;
		rotas = new ArrayList<String>();
		rotas.add("Rua Lamartine Babo, Garanhuns");
		rotas.add("Avenida Doutor Jos� de S� Benevides, Ju�");
	}

	public CadastroRotas(WebDriver driver) {
		wait = new WebDriverWait(driver, 10);
	    windowHandle = driver.getWindowHandle();
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		clear();
		escolherCadastro(driver);
		
		errorBuffer.append(cadastrarPerfeito(driver)+"\n"); //teste perfeito
		errorBuffer.append(cadastrarCampoRaioEscapeVazio(driver)+"\n"); //teste com campo raioEscape vazio
		errorBuffer.append(cadastrarRaioEscapeInvalido(driver)+"\n"); //teste com raioEscape invalido
		errorBuffer.append(cadastrarRaioEscapeNegativo(driver)+"\n"); //teste com raioEscape negativo
		errorBuffer.append(cadastrarCampoNomeVazio(driver)+"\n"); //teste com campo nome vazio
		errorBuffer.append(cadastrarCampoVelMaxVazio(driver)+"\n"); //teste com campo quantidade de litros vazio
		errorBuffer.append(cadastrarVelMaxInvalido(driver)+"\n"); //teste com quantidade de litros inv�lido
		errorBuffer.append(cadastrarVelMaxNegativo(driver)+"\n"); //teste com quantidade de litros negativo
		errorBuffer.append(cadastrarRotasVazias(driver)+"\n"); //teste com rotas vazias
		errorBuffer.append(cadastrarContatoVazio(driver)+"\n"); //teste com contato vazio
		
		setDadosArquivo(errorBuffer);

	}
		
	public static void escolherCadastro(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[4]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
        
        //Abre a aba escolhida
        WebElement abaE = wait.until(ExpectedConditions.elementToBeClickable(By.id("tabRota")));
        js.executeScript("arguments[0].click();", abaE);
	}
	
	public static void cadastrar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em cadastrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"rotas\"]/a[1]")));
        novo.click();
        
        //Vai para nova aba
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1)); 
        
        WebElement raioEscapeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("escape_rota")));
        raioEscapeE.clear();
        raioEscapeE.sendKeys(raioEscape);
              
        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nome_rota")));
        //js.executeScript("arguments[0].value=arguments[1];",nomeE,nome);
        nomeE.clear();
        nomeE.sendKeys(nome);
        
        WebElement velMaxE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("velmax_rota")));
        //js.executeScript("arguments[0].value=arguments[1];",velMaxE,velMax);
        velMaxE.clear();
        velMaxE.sendKeys(velMax);
        
        //tratando as rotas
        if(rotas.size() <2) {
        	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"map\"]/div[2]/div[2]/div/div[1]/div[2]/input"))).clear();
        	if(rotas.size() < 1)
        		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"map\"]/div[2]/div[2]/div/div[1]/div[1]/input"))).clear();
        }
        int i = 1;
        for(String o:rotas) {
        	if(i > 2) 
        		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"map\"]/div[2]/div[2]/div/div[1]/button"))).click();
        	
        	WebElement placasE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"map\"]/div[2]/div[2]/div/div[1]/div["+i+"]/input")));
            placasE.clear();
        	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	i++;
        	placasE.sendKeys(o,Keys.ENTER,Keys.ENTER);
        }
        
        WebElement baseE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("categoria_rota")));
        if(indexContatos >= 0) {
		    Select selectBaseE = new Select(baseE);
		    selectBaseE.selectByIndex(indexContatos);
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
        
        clear();
          

	}
	
	public static StringBuffer cadastrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarPerfeito";
		String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
		
		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertTrue(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
	            e1.printStackTrace();
			}
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		clear();
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
        
	}
	
	public static StringBuffer cadastrarCampoRaioEscapeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoRaioEscapeVazio";
		String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO RAIO DE ESCAPE VAZIO";

		raioEscape = "";

		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertFalse(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
			}
            
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		clear();
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}

	public static StringBuffer cadastrarRaioEscapeInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarRaioEscapeInvalido";
		String motivo = "CADASTRO � EFETUADO MESMO COM RAIO DE ESCAPE INV�LIDO";

		raioEscape = "abc!";
		
		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertFalse(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
			}
            
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		clear();
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarRaioEscapeNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarRaioEscapeNegativo";
		String motivo = "CADASTRO � EFETUADO MESMO COM RAIO DE ESCAPE NEGATIVO";

		raioEscape = "-500";
		
		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertFalse(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
			}
            
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		clear();
		driver.close();
		driver.switchTo().window(windowHandle);
	    return errorBuffer;
	}
	
	public static StringBuffer cadastrarCampoNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoNomeVazio";
		String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO NOME VAZIO";

		nome = "";
		
		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertFalse(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
			}
            
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		clear();
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
		
	public static StringBuffer cadastrarCampoVelMaxVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarCampoVelMaxVazio";
		String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO VELOCIDADE MAXIMA VAZIO";
	
		velMax = "";
		
		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertFalse(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
			}
            
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		clear();		
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarVelMaxInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarVelMaxInvalido";
		String motivo = "CADASTRO � EFETUADO MESMO COM VELOCIDADE MAXIMA INV�LIDO";

		velMax = "abc!";
		
		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertFalse(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
			}
            
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		clear();
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarVelMaxNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarVelMaxNegativo";
		String motivo = "CADASTRO � EFETUADO MESMO COM VELOCIDADE MAXIMA NEGATIVO";
	
		velMax = "-5";
		
		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertFalse(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
			}
            
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		clear();		
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarRotasVazias(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarRotasVazias";
		String motivo = "CADASTRO � EFETUADO MESMO COM ROTAS VAZIAS";

		rotas = new ArrayList<String>();
		
		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertFalse(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
			}
            
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		clear();		
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public static StringBuffer cadastrarContatoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "cadastrarContatoVazio";
		String motivo = "CADASTRO � EFETUADO MESMO COM CONTATO VAZIO";

		indexContatos = 0;
		
		try {
			cadastrar(driver);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
			}
			catch(TimeoutException e) {
				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			String alerta = alert.getText();
			alert.accept();
			try {
				Assert.assertFalse(alerta.equals("Salvo com sucesso!"));
	            errorBuffer.append(Comandos.printTesteSucesso(teste));
			}
			catch(AssertionError e1) {
	            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
			}
            
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		clear();		
		driver.close();
		driver.switchTo().window(windowHandle);
        return errorBuffer;
	}
	
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroRotas.dadosArquivo = dadosArquivo;
	}
	
}
