package tests.gestorfrotas.menuresponsivo.cadastros;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;
import tests.util.Variaveis;

public class CadastroVeiculos extends Cadastros{
	private static final String chassiTela = "Cadastro Veiculos";
		
	private static String veiculo = "teste gustavo";
	private static String hodometro = "92804664535";
	private static String horimetro = "123";
	private static String equipamento = "2654685";
	private static String marca = "15151515151";
	private static String modelo = "Modelo Lamartine Babo"; 
	private static String prefixo = "magano"; 
	private static String renavam = "garanhuns"; 
	private static String anoLicen = "153"; 
	private static String limiteVel = "12"; 
	private static String consumoMedio = "12"; 
	
	private static String capacidadeT = "120"; 
	private static String pda = "teste@teste.com"; 
	private static String limiteTem = "50"; 
	private static String chassi = "ACK0588";
	private static String dataVencimento = "01/01/2020";
	private static String contrato = "teste@teste.com"; 
	private static String regime = "52a1f5a1sf5sa1faa"; 
	private static String imei = "52a1f5a1sf5sa1faa"; 
	private static String baseVeiculo = "52a1f5a1sf5sa1faa"; 

	private static String dataInstalacao = "01/02/2020";
	private static String anoModelo = "2010"; 
	private static String anoFabri = "2009"; 
	private static String patrimonio = "1515161"; 
	private static String expedienteIni = "10:00:00"; 
	private static String expedienteFin = "15:00:00"; 
	
	private static String tipoComb = "TESTE TESTE 123!"; 
	private static int indexTeclado = 1;
	private static String gmt = "-5"; 
	private static String ajusteTemp = "-5"; 
	private static int indexTipoVei = 1;
	private static String transmissaoMov = "50";
	private static String transmissaoPar = "2000";
	private static int indexIcone = 2;

	private static JavascriptExecutor js = null; //Executor de Javascript
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clearEdicao() {
		veiculo = null;
		hodometro = null;
		horimetro = null;
		equipamento = null;
		marca = null;
		modelo = null; 
		prefixo = null; 
		renavam = null; 
		anoLicen = null; 
		limiteVel = null; 
		consumoMedio = null; 
		
		capacidadeT = null; 
		pda = null; 
		limiteTem = null; 
		chassi = null;
		dataVencimento = null;
		contrato = null; 
		regime = null; 
		imei = null; 
		baseVeiculo = null; 

		dataInstalacao = null;
		anoModelo = null; 
		anoFabri = null; 
		patrimonio = null; 
		expedienteIni = null; 
		expedienteFin = null; 
		
		tipoComb = null; 
		indexTeclado = -1;
		gmt = null; 
		ajusteTemp = null; 
		indexTipoVei = -1;
		transmissaoMov = null;
		transmissaoPar = null;
		indexIcone = -1; 
	}
	
	public static StringBuffer testarEdicao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\nEDI��O\n");
		
		clearEdicao();
		
	
		errorBuffer.append(editarCampoChassi(driver)+"\n"); //teste com campo chassi 
		errorBuffer.append(editarCampoChassiVazio(driver)+"\n"); //teste com campo chassi vazio
		
		errorBuffer.append(editarCampoVeiculo(driver)+"\n"); //teste com campo veiculo 
		errorBuffer.append(editarCampoVeiculoVazio(driver)+"\n"); //teste com campo veiculo vazio
		
		errorBuffer.append(editarCampoDataVencimento(driver)+"\n"); //teste com campo data de dataVencimento 
		errorBuffer.append(editarCampoDataVencimentoVazio(driver)+"\n"); //teste com campo data de dataVencimento vazio
		errorBuffer.append(editarDataVencimentoInvalida(driver)+"\n"); //teste com data de dataVencimento inv�lida
		
		errorBuffer.append(editarCampoTipoComb(driver)+"\n"); //teste com campo tipoComb 
		errorBuffer.append(editarCampoTipoCombVazio(driver)+"\n"); //teste com campo tipoComb vazio
		
		errorBuffer.append(editarCampo(driver)+"\n"); //teste com campo f�rias 
		errorBuffer.append(editarCampoVazio(driver)+"\n"); //teste com campo f�rias vazio
		errorBuffer.append(editarInvalida(driver)+"\n"); //teste com f�rias inv�lida

		errorBuffer.append(editarCampoEquipamento(driver)+"\n"); //teste com campo equipamento 
		errorBuffer.append(editarCampoEquipamentoVazio(driver)+"\n"); //teste com campo equipamento vazio
		
		errorBuffer.append(editarCampoHodometro(driver)+"\n"); //teste com campo hodometro 
		errorBuffer.append(editarCampoHodometroVazio(driver)+"\n"); //teste com campo hodometro vazio
		errorBuffer.append(editarHodometroInvalido(driver)+"\n"); //teste com hodometro inv�lida
		errorBuffer.append(editarHodometroNegativo(driver)+"\n"); //teste com hodometro negativo

		errorBuffer.append(editarCampoMarca(driver)+"\n"); //teste com campo marca 
		errorBuffer.append(editarCampoMarcaVazio(driver)+"\n"); //teste com campo marca vazio
		
		errorBuffer.append(editarCampoAnoLicen(driver)+"\n"); //teste com campo anoLicen
		errorBuffer.append(editarCampoAnoLicenVazio(driver)+"\n"); //teste com campo anoLicen vazio
		errorBuffer.append(editarAnoLicenInvalida(driver)+"\n"); //teste com anoLicen inv�lida
		errorBuffer.append(editarAnoLicenNegativa(driver)+"\n"); //teste com anoLicen negativa

		errorBuffer.append(editarCampoAnoModelo(driver)+"\n"); //teste com campo anoModelo
		errorBuffer.append(editarCampoAnoModeloVazio(driver)+"\n"); //teste com campo anoModelo vazio
		errorBuffer.append(editarAnoModeloInvalida(driver)+"\n"); //teste com anoModelo inv�lida
		errorBuffer.append(editarAnoModeloNegativa(driver)+"\n"); //teste com anoModelo negativa
		
		errorBuffer.append(editarCampoAnoFabri(driver)+"\n"); //teste com campo anoFabri
		errorBuffer.append(editarCampoAnoFabriVazio(driver)+"\n"); //teste com campo anoFabri vazio
		errorBuffer.append(editarAnoFabriInvalida(driver)+"\n"); //teste com anoFabri inv�lida
		errorBuffer.append(editarAnoFabriNegativa(driver)+"\n"); //teste com anoFabri negativa
		
		errorBuffer.append(editarCampoPatrimonio(driver)+"\n"); //teste com campo patrimonio
		errorBuffer.append(editarCampoPatrimonioVazio(driver)+"\n"); //teste com campo patrimonio vazio
		errorBuffer.append(editarPatrimonioInvalida(driver)+"\n"); //teste com patrimonio inv�lida
		errorBuffer.append(editarPatrimonioNegativa(driver)+"\n"); //teste com patrimonio negativa
		
		errorBuffer.append(editarCampoExpedienteIni(driver)+"\n"); //teste com campo expedienteIni
		errorBuffer.append(editarCampoExpedienteIniVazio(driver)+"\n"); //teste com campo expedienteIni vazio
		errorBuffer.append(editarExpedienteIniInvalida(driver)+"\n"); //teste com expedienteIni inv�lida
		
		errorBuffer.append(editarCampoExpedienteFin(driver)+"\n"); //teste com campo expedienteFin
		errorBuffer.append(editarCampoExpedienteFinVazio(driver)+"\n"); //teste com campo expedienteFin vazio
		errorBuffer.append(editarExpedienteFinInvalida(driver)+"\n"); //teste com expedienteFin inv�lida
		
		errorBuffer.append(editarCampoModelo(driver)+"\n"); //teste com campo modelo 
		errorBuffer.append(editarCampoModeloVazio(driver)+"\n"); //teste com campo modelo vazio
		
		errorBuffer.append(editarCampoHorimetro(driver)+"\n"); //teste com campo horimetro 
		errorBuffer.append(editarCampoHorimetroVazio(driver)+"\n"); //teste com campo horimetro vazio
		errorBuffer.append(editarHorimetroInvalida(driver)+"\n"); //teste com horimetro inv�lida
		errorBuffer.append(editarHorimetroNegativa(driver)+"\n"); //teste com horimetro negativa

		errorBuffer.append(editarCampoPrefixo(driver)+"\n"); //teste com campo prefixo 
		errorBuffer.append(editarCampoPrefixoVazio(driver)+"\n"); //teste com campo prefixo vazio

		errorBuffer.append(editarCampoRenavam(driver)+"\n"); //teste com campo renavam 
		errorBuffer.append(editarCampoRenavamVazio(driver)+"\n"); //teste com campo renavam vazio

		errorBuffer.append(editarCampoPda(driver)+"\n"); //teste com campo pda 
		errorBuffer.append(editarCampoPdaVazio(driver)+"\n"); //teste com campo pda vazio
		
		errorBuffer.append(editarCampoRegime(driver)+"\n"); //teste com campo regime 
		errorBuffer.append(editarCampoRegimeVazio(driver)+"\n"); //teste com campo regime vazio

		errorBuffer.append(editarCampoImei(driver)+"\n"); //teste com campo imei 
		errorBuffer.append(editarCampoImeiVazio(driver)+"\n"); //teste com campo imei vazio
		
		errorBuffer.append(editarCampoBaseVeiculo(driver)+"\n"); //teste com campo baseVeiculo 
		errorBuffer.append(editarCampoBaseVeiculoVazio(driver)+"\n"); //teste com campo baseVeiculo vazio
		
		errorBuffer.append(editarCampoContrato(driver)+"\n"); //teste com campo contrato 
		errorBuffer.append(editarCampoContratoVazio(driver)+"\n"); //teste com campo contrato vazio
		
		errorBuffer.append(editarCampoLimiteVel(driver)+"\n"); //teste com campo limiteVel 
		errorBuffer.append(editarCampoLimiteVelVazio(driver)+"\n"); //teste com campo limiteVel vazio
		errorBuffer.append(editarLimiteVelInvalido(driver)+"\n"); //teste com limiteVel invalido
		errorBuffer.append(editarLimiteVelNegativo(driver)+"\n"); //teste com limiteVel negativo

		errorBuffer.append(editarCampoConsumoMedio(driver)+"\n"); //teste com campo consumoMedio 
		errorBuffer.append(editarCampoConsumoMedioVazio(driver)+"\n"); //teste com campo consumoMedio vazio
		errorBuffer.append(editarConsumoMedioInvalido(driver)+"\n"); //teste com consumoMedio invalido
		errorBuffer.append(editarConsumoMedioNegativo(driver)+"\n"); //teste com consumoMedio negativo

		errorBuffer.append(editarCampoCapacidadeT(driver)+"\n"); //teste com campo capacidadeT 
		errorBuffer.append(editarCampoCapacidadeTVazio(driver)+"\n"); //teste com campo capacidadeT vazio
		errorBuffer.append(editarCapacidadeTInvalido(driver)+"\n"); //teste com capacidadeT invalido
		errorBuffer.append(editarCapacidadeTNegativo(driver)+"\n"); //teste com capacidadeT negativo
		
		errorBuffer.append(editarCampoLimiteTem(driver)+"\n"); //teste com campo limiteTem 
		errorBuffer.append(editarCampoLimiteTemVazio(driver)+"\n"); //teste com campo limiteTem vazio
		errorBuffer.append(editarLimiteTemInvalido(driver)+"\n"); //teste com limiteTem invalido
		
		errorBuffer.append(editarCampoGmt(driver)+"\n"); //teste com campo gmt 
		errorBuffer.append(editarCampoGmtVazio(driver)+"\n"); //teste com campo gmt vazio
		errorBuffer.append(editarGmtInvalido(driver)+"\n"); //teste com gmt invalido
		
		errorBuffer.append(editarCampoAjusteTemp(driver)+"\n"); //teste com campo ajusteTemp 
		errorBuffer.append(editarCampoAjusteTempVazio(driver)+"\n"); //teste com campo ajusteTemp vazio
		errorBuffer.append(editarAjusteTempInvalido(driver)+"\n"); //teste com ajusteTemp invalido
		
		errorBuffer.append(editarCampoTransmissaoMov(driver)+"\n"); //teste com campo transmissaoMov 
		errorBuffer.append(editarCampoTransmissaoMovVazio(driver)+"\n"); //teste com campo transmissaoMov vazio
		errorBuffer.append(editarTransmissaoMovInvalido(driver)+"\n"); //teste com transmissaoMov invalido
		
		errorBuffer.append(editarCampoTransmissaoPar(driver)+"\n"); //teste com campo transmissaoPar 
		errorBuffer.append(editarCampoTransmissaoParVazio(driver)+"\n"); //teste com campo transmissaoPar vazio
		errorBuffer.append(editarTransmissaoParInvalido(driver)+"\n"); //teste com transmissaoPar invalido
		
		return errorBuffer;
	}

	public CadastroVeiculos(WebDriver driver) {
		wait = new WebDriverWait(driver, 70);
		js = ((JavascriptExecutor) driver);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+chassiTela+"\n");
		
		//errorBuffer.append(testarCadastro(driver));
		errorBuffer.append(testarEdicao(driver));

		
		setDadosArquivo(errorBuffer);

	}
		
	public static void escolherCadastro(WebDriver driver) {
        
        //Abre menu de cadastros
        String xpath = abrirCadastro(driver)+"ul/li[19]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
        
        editar(driver);

	}
		
	public static void editar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas

        // clica em editar veiculo
        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
        editar.click();
        
        if(chassi != null) {
        	WebElement chassiE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("chassi")));
            chassiE.clear();
            chassiE.sendKeys(chassi);
        }
        
        if(veiculo != null) {
	        WebElement veiculoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("veiculo")));
	        veiculoE.clear();
	        veiculoE.sendKeys(veiculo);
	        
        }
        
        if(dataVencimento != null) {
	        WebElement dataVencimentoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("vencimento")));
	        dataVencimentoE.clear();
	        dataVencimentoE.sendKeys(dataVencimento);
        }
        
        if(consumoMedio != null) {
	        WebElement consumoMedioE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("consumo_medio")));
	        consumoMedioE.clear();
	        consumoMedioE.sendKeys(consumoMedio);
        }
        
        if(tipoComb != null) {
	        WebElement tipoCombE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tipo_combustivel")));
	        tipoCombE.clear();
	        tipoCombE.sendKeys(tipoComb);
        }
        
        if(indexTeclado >= 0) {
            WebElement tecladoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("teclado")));
		    Select selectTecladoE = new Select(tecladoE);
		    selectTecladoE.selectByIndex(indexTeclado);
        }
        
        if(indexTipoVei >= 0) {
            WebElement tipoVeiE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tipo")));
		    Select selectTipoVeiE = new Select(tipoVeiE);
		    selectTipoVeiE.selectByIndex(indexTipoVei);
        }
        
        if(indexIcone >= 0) {
            WebElement iconeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("imagem")));
		    Select selectIconeE = new Select(iconeE);
		    selectIconeE.selectByIndex(indexIcone);
        }
        
        if(dataInstalacao != null) {
	        WebElement dataInstalacaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_instalacao")));
	        dataInstalacaoE.clear();
	        dataInstalacaoE.sendKeys(dataInstalacao);
        }
        
        if(equipamento != null) {
	        WebElement rgE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("equipamento")));
	        rgE.clear();
	        rgE.sendKeys(equipamento);
        }
        
        if(hodometro != null) {
	        WebElement hodometroE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hodometro")));
	        hodometroE.clear();
	        hodometroE.sendKeys(hodometro);
        }
        
        if(marca != null) {
	        WebElement marcaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("marca")));
	        marcaE.clear();
	        marcaE.sendKeys(marca);
        }
        
        
        if(anoLicen != null) {
	        WebElement anoLicenE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ano_crvl")));
	        anoLicenE.clear();
	        anoLicenE.sendKeys(anoLicen);
        }
        
        if(anoModelo != null) {
	        WebElement anoModeloE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ano")));
	        anoModeloE.clear();
	        anoModeloE.sendKeys(anoModelo);
        }
        
        if(anoFabri != null) {
	        WebElement anoFabriE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ano_fabricacao")));
	        anoFabriE.clear();
	        anoFabriE.sendKeys(anoFabri);
        }
        
        if(patrimonio != null) {
	        WebElement patrimonioE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("id_patrimonial")));
	        patrimonioE.clear();
	        patrimonioE.sendKeys(patrimonio);
        }
        
        if(expedienteIni != null) {
	        WebElement expedienteIniE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora_expediente_inicial")));
	        expedienteIniE.clear();
	        expedienteIniE.sendKeys(expedienteIni);
        }
        
        if(expedienteFin != null) {
	        WebElement expedienteFinE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora_expediente_inicial")));
	        expedienteFinE.clear();
	        expedienteFinE.sendKeys(expedienteFin);
        }
        
        if(modelo != null) {
	        WebElement modeloE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("modelo")));
	        modeloE.clear();
	        modeloE.sendKeys(modelo);
        }
        
        if(imei != null) {
	        WebElement imeiE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("imei")));
	        imeiE.clear();
	        imeiE.sendKeys(imei);
        }

        if(baseVeiculo != null) {
	        WebElement baseVeiculoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("base_veiculo")));
	        baseVeiculoE.clear();
	        baseVeiculoE.sendKeys(baseVeiculo);
        }
        
        if(capacidadeT != null) {
	        WebElement capacidadeTE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("capacidade_tanque")));
	        capacidadeTE.clear();
	        capacidadeTE.sendKeys(capacidadeT);
        }
        
        if(horimetro != null) {
	        WebElement horimetroE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("horimetro")));
	        horimetroE.clear();
	        horimetroE.sendKeys(horimetro);
        }
        
        if(prefixo != null) {
	        WebElement prefixoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("prefixo_veiculo")));
	        prefixoE.clear();
	        prefixoE.sendKeys(prefixo);
        }
        
        if(renavam != null) {
	        WebElement renavamE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("renavam")));
	        renavamE.clear();
	        renavamE.sendKeys(renavam);
        }
        
        if(pda != null) {
	        WebElement pdaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("pda")));
	        pdaE.clear();
	        pdaE.sendKeys(pda);
        }
        
        if(regime != null) {
	        WebElement regimeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("regime_veiculo")));
	        regimeE.clear();
	        regimeE.sendKeys(regime);
        }
        
        if(limiteVel != null) {
	        WebElement limiteVelE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("limite_velocidade")));
	        limiteVelE.clear();
	        limiteVelE.sendKeys(limiteVel);
        }
        
        if(limiteTem != null) {
	        WebElement limiteTemE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("limite_temperatura")));
	        limiteTemE.clear();
	        limiteTemE.sendKeys(limiteTem);
        }
        
        if(gmt != null) {
	        WebElement gmtE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("fuso_horario")));
	        gmtE.clear();
	        gmtE.sendKeys(gmt);
        }
        
        if(ajusteTemp != null) {
	        WebElement ajusteTempE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ajuste_temperatura")));
	        ajusteTempE.clear();
	        ajusteTempE.sendKeys(ajusteTemp);
        }
        
        if(transmissaoMov != null) {
	        WebElement transmissaoMovE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("temp_mov")));
	        transmissaoMovE.clear();
	        transmissaoMovE.sendKeys(transmissaoMov);
        }
        
        if(transmissaoPar != null) {
	        WebElement transmissaoParE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("temp_par")));
	        transmissaoParE.clear();
	        transmissaoParE.sendKeys(transmissaoPar);
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btSalvar")));
        botaoSubmit.click();
                
        clearEdicao();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	e.printStackTrace();
        }
        js.executeScript("window.history.go(-1);");
	}
	
	// Nenhuma edi��o de veiculo funciona
	public static StringBuffer editarCampoVeiculo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoVeiculo";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO VEICULO! ";		
		
		veiculo = "SAULO SILVA MENDES";
		String novoValor = veiculo;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("veiculo")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoVeiculoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoVeiculoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO VEICULO VAZIO! ";
		
		veiculo = "";
		String novoValor = veiculo;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("veiculo")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoEquipamento(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoEquipamento";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO EQUIPAMENTO! ";		
		
		equipamento = "580tj";
		String novoValor = equipamento;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("equipamento")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoEquipamentoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoEquipamentoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO EQUIPAMENTO VAZIO! ";
		
		equipamento = "";
		String novoValor = equipamento;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("equipamento")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
		
	public static StringBuffer editarCampoHodometro(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoHodometro";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO HODOMETRO! ";		
		
		hodometro = "117";
		String novoValor = hodometro;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hodometro")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoHodometroVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoHodometroVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO HODOMETRO VAZIO! ";
		
		hodometro = "";
		String novoValor = hodometro;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("hodometro")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarHodometroInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarHodometroInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM HODOMETRO INV�LIDO! ";
		
		hodometro = "abc!";
		String novoValor = hodometro;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("hodometro")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarHodometroNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarHodometroNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM HODOMETRO NEGATIVO! ";
		
		hodometro = "-117";
		String novoValor = hodometro;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("hodometro")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoMarca(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoMarca";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO MARCA! ";		
		
		marca = "117";
		String novoValor = marca;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("marca")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoMarcaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoMarcaVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO MARCA VAZIO! ";
		
		marca = "";
		String novoValor = marca;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("marca")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTipoComb(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTipoComb";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO TIPO COMBUST�VEL! ";		
		
		tipoComb = "tipoComb";
		String novoValor = tipoComb;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tipo_combustivel")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTipoCombVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTipoCombVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO TIPO COMBUST�VEL VAZIO! ";
		
		tipoComb = "";
		String novoValor = tipoComb;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("tipo_combustivel")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
		
	public static StringBuffer editarCampoDataVencimento(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataVencimento";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO DATA DO VENCIMENTO! ";		
		
		dataVencimento = "02/04/2020";
		String novoValor = dataVencimento;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("vencimento")));
	        
	        Assert.assertEquals(novoValor,Variaveis.getFormatodata().parse(elemento.getAttribute("value")));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDataVencimentoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataVencimentoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO DATA DO VENCIMENTO VAZIO! ";
		
		dataVencimento = "";
		String novoValor = dataVencimento;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("vencimento")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarDataVencimentoInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataVencimentoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM DATA DO VENCIMENTO INV�LIDA! ";
		
		dataVencimento = "abc!";
		String novoValor = dataVencimento;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("vencimento")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampo";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO DATA DE INSTALA��O DE EQUIPAMENTO! ";		
		
		dataInstalacao = "04/04/2020";
		String novoValor = dataInstalacao;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data_instalacao")));
	        
	        Assert.assertEquals(novoValor,Variaveis.getFormatodata().parse(elemento.getAttribute("value")));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO DATA DE INSTALA��O DE EQUIPAMENTO VAZIO! ";
		
		dataInstalacao = "";
		String novoValor = dataInstalacao;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("data_instalacao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM DATA DE INSTALA��O DE EQUIPAMENTO INV�LIDA! ";
		
		dataInstalacao = "abc!";
		String novoValor = dataInstalacao;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("data_instalacao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAnoLicen(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAnoLicen";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO ANO LICENCIAMENTO! ";		
		
		anoLicen = "117";
		String novoValor = anoLicen;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ano_crvl")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAnoLicenVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAnoLicenVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO ANO LICENCIAMENTO VAZIO! ";
		
		anoLicen = "";
		String novoValor = anoLicen;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ano_crvl")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarAnoLicenInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarAnoLicenInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM ANO LICENCIAMENTO INV�LIDA! ";
		
		anoLicen = "abc!";
		String novoValor = anoLicen;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ano_crvl")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarAnoLicenNegativa(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarAnoLicenNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM ANO LICENCIAMENTO NEGATIVA! ";
		
		anoLicen = "-117";
		String novoValor = anoLicen;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ano_crvl")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAnoModelo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAnoModelo";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO ANO MODELO! ";		
		
		anoModelo = "2010";
		String novoValor = anoModelo;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ano")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAnoModeloVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAnoModeloVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO ANO MODELO VAZIO! ";
		
		anoModelo = "";
		String novoValor = anoModelo;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ano")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarAnoModeloInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarAnoModeloInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM ANO MODELO INV�LIDA! ";
		
		anoModelo = "abc!";
		String novoValor = anoModelo;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ano")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarAnoModeloNegativa(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarAnoModeloNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM ANO MODELO NEGATIVA! ";
		
		anoModelo = "-117";
		String novoValor = anoModelo;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ano")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAnoFabri(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAnoFabri";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO ANO FABRICA��O! ";		
		
		anoFabri = "2010";
		String novoValor = anoFabri;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ano_fabricacao")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAnoFabriVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAnoFabriVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO ANO FABRICA��O VAZIO! ";
		
		anoFabri = "";
		String novoValor = anoFabri;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ano_fabricacao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarAnoFabriInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarAnoFabriInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM ANO FABRICA��O INV�LIDA! ";
		
		anoFabri = "abc!";
		String novoValor = anoFabri;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ano_fabricacao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarAnoFabriNegativa(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarAnoFabriNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM ANO FABRICA��O NEGATIVA! ";
		
		anoFabri = "-117";
		String novoValor = anoFabri;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ano_fabricacao")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPatrimonio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPatrimonio";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO IDENTIFICA��O PATRIMONIAL! ";		
		
		patrimonio = "2010";
		String novoValor = patrimonio;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("id_patrimonial")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPatrimonioVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPatrimonioVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO IDENTIFICA��O PATRIMONIAL VAZIO! ";
		
		patrimonio = "";
		String novoValor = patrimonio;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("id_patrimonial")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarPatrimonioInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarPatrimonioInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM IDENTIFICA��O PATRIMONIAL INV�LIDA! ";
		
		patrimonio = "abc!";
		String novoValor = patrimonio;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("id_patrimonial")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarPatrimonioNegativa(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarPatrimonioNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM IDENTIFICA��O PATRIMONIAL NEGATIVA! ";
		
		patrimonio = "-117";
		String novoValor = patrimonio;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("id_patrimonial")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoExpedienteIni(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoExpedienteIni";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO HORA DE EXPEDIENTE INICIAL! ";		
		
		expedienteIni = "10:00:00";
		String novoValor = expedienteIni;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora_expediente_inicial")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoExpedienteIniVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoExpedienteIniVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO HORA DE EXPEDIENTE INICIAL VAZIO! ";
		
		expedienteIni = "";
		String novoValor = expedienteIni;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("hora_expediente_inicial")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarExpedienteIniInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarExpedienteIniInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM HORA DE EXPEDIENTE INICIAL INV�LIDA! ";
		
		expedienteIni = "abc!";
		String novoValor = expedienteIni;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("hora_expediente_inicial")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoExpedienteFin(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoExpedienteFin";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO HORA DE EXPEDIENTE FINAL! ";		
		
		expedienteFin = "15:00:00";
		String novoValor = expedienteFin;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora_expediente_final")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoExpedienteFinVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoExpedienteFinVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO HORA DE EXPEDIENTE FINAL VAZIO! ";
		
		expedienteFin = "";
		String novoValor = expedienteFin;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("hora_expediente_final")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarExpedienteFinInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarExpedienteFinInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM HORA DE EXPEDIENTE FINAL INV�LIDA! ";
		
		expedienteFin = "abc!";
		String novoValor = expedienteFin;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("hora_expediente_final")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoHorimetro(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoHorimetro";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO HORIMETRO! ";		
		
		horimetro = "117";
		String novoValor = horimetro;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("horimetro")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoHorimetroVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoHorimetroVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO HORIMETRO VAZIO! ";
		
		horimetro = "";
		String novoValor = horimetro;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("horimetro")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarHorimetroInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarHorimetroInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM HORIMETRO INV�LIDA! ";
		
		horimetro = "abc!";
		String novoValor = horimetro;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("horimetro")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarHorimetroNegativa(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarHorimetroNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM HORIMETRO NEGATIVA! ";
		
		horimetro = "-117";
		String novoValor = horimetro;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("horimetro")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoModelo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoModelo";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO MODELO! ";		
		
		modelo = "modelo desconhecido";
		String novoValor = modelo;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("modelo")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoModeloVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoModeloVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO MODELO VAZIO! ";
		
		modelo = "";
		String novoValor = modelo;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("modelo")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoRenavam(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoRenavam";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO RENAVAM! ";		
		
		renavam = "modelo desconhecido";
		String novoValor = renavam;
		escolherCadastro(driver);
				
		try {
			
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("renavam")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoRenavamVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoRenavamVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO RENAVAM VAZIO! ";
		
		renavam = "";
		String novoValor = renavam;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("renavam")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPrefixo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPrefixo";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO PREFIXO! ";		
		
		prefixo = "modelo desconhecido";
		String novoValor = prefixo;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("prefixo_veiculo")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPrefixoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPrefixoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO PREFIXO VAZIO! ";
		
		prefixo = "";
		String novoValor = prefixo;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("prefixo_veiculo")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoLimiteVel(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoLimiteVel";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO LIMITE VELOCIDADE! ";		
		
		limiteVel = "117";
		String novoValor = limiteVel;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("limite_velocidade")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoLimiteVelVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoLimiteVelVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO LIMITE VELOCIDADE VAZIO! ";
		
		limiteVel = "";
		String novoValor = limiteVel;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("limite_velocidade")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarLimiteVelInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarLimiteVelInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM LIMITE VELOCIDADE INV�LIDO! ";
		
		limiteVel = "abc!";
		String novoValor = limiteVel;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("limite_velocidade")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarLimiteVelNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarLimiteVelNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM LIMITE VELOCIDADE NEGATIVO! ";
		
		limiteVel = "-500";
		String novoValor = limiteVel;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("limite_velocidade")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoConsumoMedio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoConsumoMedio";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CONSUMO M�DIO! ";		
		
		consumoMedio = "117";
		String novoValor = consumoMedio;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("consumo_medio")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoConsumoMedioVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoConsumoMedioVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO CONSUMO M�DIO VAZIO! ";
		
		consumoMedio = "";
		String novoValor = consumoMedio;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("consumo_medio")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarConsumoMedioInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarConsumoMedioInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CONSUMO M�DIO INV�LIDO! ";
		
		consumoMedio = "abc!";
		String novoValor = consumoMedio;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("consumo_medio")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarConsumoMedioNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarConsumoMedioNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM CONSUMO M�DIO NEGATIVO! ";
		
		consumoMedio = "-56";
		String novoValor = consumoMedio;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("consumo_medio")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoCapacidadeT(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoCapacidadeT";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CAPACIDADE DO TANQUE DE COMBUST�VEL! ";		
		
		capacidadeT = "117";
		String novoValor = capacidadeT;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("capacidade_tanque")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoCapacidadeTVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoCapacidadeTVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO CAPACIDADE DO TANQUE DE COMBUST�VEL VAZIO! ";
		
		capacidadeT = "";
		String novoValor = capacidadeT;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("capacidade_tanque")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCapacidadeTInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCapacidadeTInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAPACIDADE DO TANQUE DE COMBUST�VEL INV�LIDO! ";
		
		capacidadeT = "abc!";
		String novoValor = capacidadeT;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("capacidade_tanque")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCapacidadeTNegativo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCapacidadeTNegativo";
		String motivo = "EDI��O � EFETUADA MESMO COM CAPACIDADE DO TANQUE DE COMBUST�VEL NEGATIVA! ";
		
		capacidadeT = "-56";
		String novoValor = capacidadeT;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("capacidade_tanque")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoLimiteTem(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoLimiteTem";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CAPACIDADE DO LIMITE DE TEMPERATURA! ";		
		
		limiteTem = "117";
		String novoValor = limiteTem;
		escolherCadastro(driver);
				
		try {
			
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("limite_temperatura")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoLimiteTemVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoLimiteTemVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO CAPACIDADE DO LIMITE DE TEMPERATURA VAZIO! ";
		
		limiteTem = "";
		String novoValor = limiteTem;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("limite_temperatura")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarLimiteTemInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarLimiteTemInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAPACIDADE DO LIMITE DE TEMPERATURA INV�LIDO! ";
		
		limiteTem = "abc!";
		String novoValor = limiteTem;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("limite_temperatura")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoGmt(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoGmt";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CAPACIDADE DO GMT! ";		
		
		gmt = "6";
		String novoValor = gmt;
		escolherCadastro(driver);
				
		try {
			
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("fuso_horario")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoGmtVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoGmtVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO CAPACIDADE DO GMT VAZIO! ";
		
		gmt = "";
		String novoValor = gmt;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("fuso_horario")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarGmtInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarGmtInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAPACIDADE DO GMT INV�LIDO! ";
		
		gmt = "abc!";
		String novoValor = gmt;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("fuso_horario")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAjusteTemp(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAjusteTemp";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CAPACIDADE DO AJUSTE DE TEMPERATURA! ";		
		
		ajusteTemp = "6";
		String novoValor = ajusteTemp;
		escolherCadastro(driver);
				
		try {
			
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ajuste_temperatura")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAjusteTempVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAjusteTempVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO CAPACIDADE DO AJUSTE DE TEMPERATURA VAZIO! ";
		
		ajusteTemp = "";
		String novoValor = ajusteTemp;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ajuste_temperatura")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarAjusteTempInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarAjusteTempInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAPACIDADE DO AJUSTE DE TEMPERATURA INV�LIDO! ";
		
		ajusteTemp = "abc!";
		String novoValor = ajusteTemp;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("ajuste_temperatura")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTransmissaoMov(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTransmissaoMov";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CAPACIDADE DO TRANSMISS�O MOVENDO! ";		
		
		transmissaoMov = "80";
		String novoValor = transmissaoMov;
		escolherCadastro(driver);
				
		try {
			
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("temp_mov")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTransmissaoMovVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTransmissaoMovVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO CAPACIDADE DO TRANSMISS�O MOVENDO VAZIO! ";
		
		transmissaoMov = "";
		String novoValor = transmissaoMov;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("temp_mov")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarTransmissaoMovInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarTransmissaoMovInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAPACIDADE DO TRANSMISS�O MOVENDO INV�LIDO! ";
		
		transmissaoMov = "abc!";
		String novoValor = transmissaoMov;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("temp_mov")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTransmissaoPar(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTransmissaoPar";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CAPACIDADE DO TRANSMISS�O PARADO! ";		
		
		transmissaoPar = "2000";
		String novoValor = transmissaoPar;
		escolherCadastro(driver);
				
		try {
			
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("temp_par")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTransmissaoParVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTransmissaoParVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO CAPACIDADE DO TRANSMISS�O PARADO VAZIO! ";
		
		transmissaoPar = "";
		String novoValor = transmissaoPar;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("temp_par")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarTransmissaoParInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarTransmissaoParInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAPACIDADE DO TRANSMISS�O PARADO INV�LIDO! ";
		
		transmissaoPar = "abc!";
		String novoValor = transmissaoPar;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("temp_par")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO";
	        else
	        	motivo += "O VALOR FOI EDITADO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
		
	public static StringBuffer editarCampoRegime(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoRegime";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO REGIME DO VENCIMENTO! ";		
		
		regime = "teste testando do teste testado testinho";
		String novoValor = regime;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("regime_veiculo")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoRegimeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoRegimeVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO REGIME DO VENCIMENTO VAZIO! ";
		
		regime = "";
		String novoValor = regime;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("regime_veiculo")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoImei(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoImei";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO IMEI! ";		
		
		imei = "teste testando do teste testado testinho";
		String novoValor = imei;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("imei")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoImeiVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoImeiVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO IMEI VAZIO! ";
		
		imei = "";
		String novoValor = imei;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("imei")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoBaseVeiculo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoBaseVeiculo";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO BASE VE�CULO! ";		
		
		baseVeiculo = "teste testando do teste testado testinho";
		String novoValor = baseVeiculo;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("base_veiculo")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoBaseVeiculoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoBaseVeiculoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO BASE VE�CULO VAZIO! ";
		
		baseVeiculo = "";
		String novoValor = baseVeiculo;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("base_veiculo")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoContrato(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoContrato";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CONTRATO! ";		
		
		contrato = "teste testando do teste testado testinho";
		String novoValor = contrato;
		escolherCadastro(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("contrato_veiculo")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoContratoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoContratoVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO CONTRATO VAZIO! ";
		
		contrato = "";
		String novoValor = contrato;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("contrato_veiculo")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPda(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPda";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO PDA! ";		
		
		pda = "teste testando do teste testado testinho";
		String novoValor = pda;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("pda")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPdaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPdaVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO PDA VAZIO! ";
		
		pda = "";
		String novoValor = pda;
		escolherCadastro(driver);
		
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("pda")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoChassi(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoChassi";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CHASSI! ";		
		
		chassi = "EEF-3680";
		String novoValor = chassi;
		escolherCadastro(driver);
				
		try {
			
	        
	        
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("chassi")));
	        
	        Assert.assertEquals(novoValor,elemento.getAttribute("value"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoChassiVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoChassiVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO CHASSI VAZIO! ";
		
		chassi = "";
		String novoValor = chassi;
		escolherCadastro(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"example\"]/tbody/tr[1]/td[10]/a[1]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("chassi")));
	                	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		
            
		}
		js.executeScript("window.history.go(-1);");
        return errorBuffer;
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		CadastroVeiculos.dadosArquivo = dadosArquivo;
	}
}
