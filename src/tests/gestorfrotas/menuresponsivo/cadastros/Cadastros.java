package tests.gestorfrotas.menuresponsivo.cadastros;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class Cadastros {
	private static WebDriverWait wait;
	private static String xpath = "//*[@id=\"sidebar-items\"]/li[4]/";
	
	public static String abrirCadastro(WebDriver driver) {
		String xpath = Cadastros.xpath;
        wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		Comandos.menuResponsivo(driver); //abre menu responsivo

        WebElement cadastros = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath+"a")));
        cadastros.click();
        return xpath;
	}
	
}
