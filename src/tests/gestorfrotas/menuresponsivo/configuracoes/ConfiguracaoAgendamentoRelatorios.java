package tests.gestorfrotas.menuresponsivo.configuracoes;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;
import tests.util.Variaveis;

public class ConfiguracaoAgendamentoRelatorios extends Configuracoes{
	private static final String placaTela = "Configura��o de Agendamento de Relat�rios";
	
	private static int indexRelatorio = 0;
	private static int indexPeriodo = 0;
	private static int indexTipo = 1;
	private static int indexGrupo = 0;
	
	private static String dataValidade = "01/01/2020";
	
	private static ArrayList<String> placas = new ArrayList<String>();
	private static ArrayList<String> contatos = new ArrayList<String>();

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		indexRelatorio = 0;
		indexPeriodo = 0;
		indexGrupo = 0;
		indexTipo = 1;
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
		placas.add("AAH3533");
		
		dataValidade = "01/01/2020";

		contatos = new ArrayList<String>();
		contatos.add("thiago Oliveira");
		contatos.add("Saulo Mendes");
	}
	
	public static void clearEdicao() {
		indexRelatorio = -1;
		indexPeriodo = -1;
		indexGrupo = -1;
		indexTipo = -1;
		
		placas = null;
		
		dataValidade = null;

		contatos = null;
	}
	

	public static StringBuffer testarConfiguracao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		clear();
		
		errorBuffer.append("\nCONFIGURA��O\n");
		
		escolherConfiguracao(driver);
		
		errorBuffer.append(configurarPerfeito(driver)+"\n"); //teste perfeito

		errorBuffer.append(configurarCampoContatosVazio(driver)+"\n"); //teste com campo contatos vazio
		errorBuffer.append(configurarCampoPlacasVazio(driver)+"\n"); //teste com campo placas vazio
		
		errorBuffer.append(configurarCampoDataValidadeVazio(driver)+"\n"); //teste com campo data de dataValidade vazio
		errorBuffer.append(configurarDataValidadeInvalida(driver)+"\n"); //teste com data de dataValidade inv�lida
		
		errorBuffer.append(configurarPerfeitoGrupo(driver)+"\n"); //teste perfeito com grupo
		
		return errorBuffer;
	}
	
	public static StringBuffer testarEdicao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		clearEdicao();

		errorBuffer.append("\nEDI��O\n");
		
		escolherConfiguracao(driver);
		
		errorBuffer.append(editarCampoContatos(driver)+"\n"); //teste com campo contatos
		errorBuffer.append(editarCampoContatosVazio(driver)+"\n"); //teste com campo contatos vazio
		
		errorBuffer.append(editarCampoPlacas(driver)+"\n"); //teste perfeito com placas
		errorBuffer.append(editarCampoPlacasVazio(driver)+"\n"); //teste com campo placas vazio
		
		errorBuffer.append(editarCampoDataValidade(driver)+"\n"); //teste com campo data de dataValidade
		errorBuffer.append(editarCampoDataValidadeVazio(driver)+"\n"); //teste com campo data de dataValidade vazio
		errorBuffer.append(editarDataValidadeInvalida(driver)+"\n"); //teste com data de dataValidade inv�lida
		
		errorBuffer.append(editarCampoGrupo(driver)+"\n"); //teste perfeito com grupo
		
		errorBuffer.append(editarCampoPeriodo(driver)+"\n"); //teste perfeito com periodo

		errorBuffer.append(editarCampoRelatorio(driver)+"\n"); //teste perfeito com relatorio

		return errorBuffer;
	}
	
	public ConfiguracaoAgendamentoRelatorios(WebDriver driver) {
		wait = new WebDriverWait(driver, 60);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+placaTela+"\n");
		
		//errorBuffer.append(testarConfiguracao(driver));
		errorBuffer.append(testarEdicao(driver));

		setDadosArquivo(errorBuffer);

	}
	
	public static void escolherConfiguracao(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirConfiguracao(driver)+"ul/li[1]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
	}
	
	public static boolean configurar(WebDriver driver) {
		boolean tudoCerto = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em configurar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[3]/div/button")));
        novo.click();
        
        WebElement relatorioE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("relatorio")));
        if(indexRelatorio >= 0) {
		    Select selectRelatorioE = new Select(relatorioE);
		    selectRelatorioE.selectByIndex(indexRelatorio);
        }
        
        WebElement dataManE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("validade")));
        js.executeScript("arguments[0].value=arguments[1]", dataManE,dataValidade);
        
        WebElement periodoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("periodo")));
        if(indexPeriodo >= 0) {
		    Select selectPeriodoE = new Select(periodoE);
		    selectPeriodoE.selectByIndex(indexPeriodo);
        }
        
        WebElement contatosE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"contatos_chosen\"]/ul/li/input")));
        if(contatos.size()>0) {
        	contatosE.clear();
        }
        for(String o:contatos) {
        	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	contatosE.sendKeys(o,Keys.ENTER);
        }
        
        Comandos.escolherRadioButtonNome(driver, "tipo", indexTipo);
        
        if(indexTipo == 1) {
        	WebElement placasE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"placas_chosen\"]/ul/li/input")));
            if(placas.size()>0) {
            	placasE.clear();
            }
            for(String o:placas) {
            	try {
    				Thread.sleep(1000);
    			} catch (InterruptedException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
            	placasE.sendKeys(o,Keys.ENTER);
            }
        }
        else if(indexTipo == 2) {
        	WebElement grupoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("grupo")));
            if(indexGrupo >= 0) {
    		    Select selectGrupoE = new Select(grupoE);
    		    selectGrupoE.selectByIndex(indexGrupo);
            }
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myForm\"]/div[2]/button[2]")));
        botaoSubmit.click();
                
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        if(alert.getText().equals("Agendamento realizado com sucesso."))
	        	tudoCerto = true;
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	e.printStackTrace();
        }
        
        WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
        
        return tudoCerto;
	}
	
	public static void editar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em editar primeiro elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
        novo.click();													
        
        if(indexRelatorio >= 0) {
            WebElement relatorioE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("relatorio")));
		    Select selectRelatorioE = new Select(relatorioE);										
		    selectRelatorioE.selectByIndex(indexRelatorio);
        }
        
        if(dataValidade != null) {
		    WebElement dataValidadeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("validade")));
		    js.executeScript("arguments[0].value=arguments[1]", dataValidadeE,dataValidade);
        }
        
        if(indexPeriodo >= 0) {
            WebElement periodoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("periodo")));
		    Select selectPeriodoE = new Select(periodoE);
		    selectPeriodoE.selectByIndex(indexPeriodo);
        }
        
        if(contatos != null) {
            WebElement contatosE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"contatos_chosen\"]/ul/li/input")));
        	Comandos.excluirListaElementosXpath(driver, "//*[@id=\"contatos_chosen\"]/ul");
		    for(String o:contatos) {
		    	try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	contatosE.sendKeys("         ",o,"         ",Keys.ENTER);
		    }
        }
        
        Comandos.escolherRadioButtonNome(driver, "tipo", indexTipo);
        
        if(indexTipo == 1) {
        	if(placas != null) {
	        	WebElement placasE = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"placas_chosen\"]/ul/li/input")));
	            Comandos.excluirListaElementosXpath(driver, "//*[@id=\"placas_chosen\"]/ul");
	            for(String o:placas) {
	            	try {
	    				Thread.sleep(1000);
	    			} catch (InterruptedException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}
	            	placasE.sendKeys("         ",o,"         ",Keys.ENTER);	            
	            }
        	}
        }
        else if(indexTipo == 2) {
            if(indexGrupo >= 0) {
            	WebElement grupoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("grupo")));
    		    Select selectGrupoE = new Select(grupoE);
    		    selectGrupoE.selectByIndex(indexGrupo);
            }
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myForm\"]/div[2]/button[2]")));
        botaoSubmit.click();
                
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	e.printStackTrace();
        }
        
        WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
    }
	
	public static StringBuffer configurarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarPerfeito";
		
		
				
		try {
			Assert.assertTrue(configurar(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "CONFIGURA��O N�O EFETUADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer configurarPerfeitoGrupo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarPerfeitoGrupo";
		
		indexTipo = 2;
				
		try {
			Assert.assertTrue(configurar(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "CONFIGURA��O N�O EFETUADA MESMO COM DADOS CORRETOS EM GRUPO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer configurarCampoPlacasVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarCampoPlacasVazio";
				
		placas = new ArrayList<String>();
		
		
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM CAMPO PLACAS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPlacas(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPlacas";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO PLACAS! ";		
		
		indexTipo = 1;
		placas = new ArrayList<String>();
		placas.add("BVZ-6570");
		placas.add("EEF-3680");
		
		List<String> novoValor = placas;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();
	        
	        Assert.assertTrue(Comandos.compararListaElementosXpath(driver,"//*[@id=\"placas_chosen\"]/ul",novoValor));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPlacasVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPlacasVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO PLACAS VAZIO! ";
		
		placas = new ArrayList<String>();
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();
	        
	        if(Comandos.listarListaElementosXpath(driver, "//*[@id=\"placas_chosen\"]/ul").size() > 0) {
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
	        }
	        else
	        	errorBuffer.append(Comandos.printTesteSucesso(teste));		
            
		}
		catch(Exception e) {
            e.printStackTrace();            
		}
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();        
        return errorBuffer;
	}

	
	public static StringBuffer configurarCampoContatosVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarCampoContatosVazio";
				
		contatos = new ArrayList<String>();
		
		
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM CAMPO CONTATOS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoContatos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoContatos";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CONTATOS! ";		
		
		contatos = new ArrayList<String>();
		contatos.add("Saulo Mendes");
		contatos.add("thiago Oliveira");
		
		List<String> novoValor = contatos;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();
	        
	        Assert.assertTrue(Comandos.compararListaElementosXpath(driver,"//*[@id=\"contatos_chosen\"]/ul",novoValor));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoContatosVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoContatosVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO PLACAS CONTATOS! ";
		
		contatos = new ArrayList<String>();
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();
	        
	        if(Comandos.listarListaElementosXpath(driver, "//*[@id=\"contatos_chosen\"]/ul").size() > 0) {
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
	        }
	        else
	        	errorBuffer.append(Comandos.printTesteSucesso(teste));		
            
		}
		catch(Exception e) {
            e.printStackTrace();            
		}
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();        
        return errorBuffer;
	}
	
	public static StringBuffer configurarCampoDataValidadeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarCampoDataValidadeVazio";
				
		dataValidade = "";
		
		
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM CAMPO DATA DE DATA DE VALIDADE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer configurarDataValidadeInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarDataValidadeInvalida";
				
		dataValidade = "abc!";
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM DATA DE DATA DE VALIDADE INV�LIDA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDataValidade(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataValidade";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO DATA DA VALIDADE! ";		
		
		dataValidade = "02/04/2020";
		String novoValor = dataValidade;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("validade")));
	        
	        Assert.assertEquals(novoValor,Variaveis.getFormatodata().parse(elemento.getText()));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDataValidadeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataValidadeVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO DATA DA VALIDADE VAZIO! ";
		
		dataValidade = "";
		String novoValor = dataValidade;
		editar(driver);
		
		try {
			
	        WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("validade")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
        return errorBuffer;
	}
	
	public static StringBuffer editarDataValidadeInvalida(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDataValidadeVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM DATA DA VALIDADE INV�LIDA! ";
		
		dataValidade = "abc!";
		String novoValor = dataValidade;
		editar(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.name("validade")));
	        
        	
	        if(!novoValor.equals(elemento.getAttribute("value")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoGrupo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoGrupo";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO GRUPO! ";		
		
		indexTipo = 2;
		indexGrupo = 2;
		int  novoValor = indexGrupo;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("grupo")));
	        Select select = new Select(elemento);
	        select.getFirstSelectedOption();
	        
	        Assert.assertEquals(select.getFirstSelectedOption(),select.getOptions().get(novoValor));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoPeriodo(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoPeriodo";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO PER�ODO! ";		
		
		indexPeriodo = 2;
		int novoValor = indexPeriodo;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("periodo")));
	        Select select = new Select(elemento);
	        select.getFirstSelectedOption();
	        
	        Assert.assertEquals(select.getFirstSelectedOption(),select.getOptions().get(novoValor));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoRelatorio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoRelatorio";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO RELAT�RIO! ";		
		
		indexRelatorio = 2;
		int novoValor = indexRelatorio;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"listaAgend\"]/tbody/tr[1]/td[6]/a[2]")));
	        editar.click();

	        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("relatorio")));
	        Select select = new Select(elemento);
	        select.getFirstSelectedOption();
	        
	        Assert.assertEquals(select.getFirstSelectedOption(),select.getOptions().get(novoValor));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModal\"]/div/div/div/button")));
        fechar.click();
        return errorBuffer;
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		ConfiguracaoAgendamentoRelatorios.dadosArquivo = dadosArquivo;
	}
}
