package tests.gestorfrotas.menuresponsivo.configuracoes;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class ConfiguracaoNotificacoes extends Configuracoes{
	private static final String placaTela = "Configura��o de Notifica��es";
	
	private static String nome = "teste do teste testado";
	private static ArrayList<String> contatos = new ArrayList<String>();

	private static boolean excessoV = false;
	private static boolean violacaoC = false;
	private static boolean freadaB = false;
	private static boolean violacaoR = false;
	private static boolean nomeM = false;
	private static boolean aceleracaoB = false;
	private static boolean movimentosI = false;
	private static boolean movimentosIH = false;
	private static boolean excessoVR = false;
	private static boolean desvioR = false;
	private static boolean temperatura = false;
	private static boolean motorO = false;
	private static boolean semC = false;
	private static boolean cnhV = false;
	private static boolean alertaB = false;
	private static boolean editarCheckbox = false;
	
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		nome = "teste do teste testado";
		
		contatos = new ArrayList<String>();
		contatos.add("thiago Oliveira");
		contatos.add("Saulo Mendes");
		
		excessoV = false;
		violacaoC = false;
		freadaB = false;
		violacaoR = false;
		nomeM = false;
		aceleracaoB = false;
		movimentosI = false;
		movimentosIH = false;
		excessoVR = false;
		desvioR = false;
		temperatura = false;
		motorO = false;
		semC = false;
		cnhV = false;
		alertaB = false;
	}
	
	public static void clearEdicao() {
		nome = null;
		
		contatos = null;
		
		excessoV = false;
		violacaoC = false;
		freadaB = false;
		violacaoR = false;
		nomeM = false;
		aceleracaoB = false;
		movimentosI = false;
		movimentosIH = false;
		excessoVR = false;
		desvioR = false;
		temperatura = false;
		motorO = false;
		semC = false;
		cnhV = false;
		alertaB = false;
		editarCheckbox = false;
	}
	
	public StringBuffer testarConfiguracao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		
		clear();
		
		errorBuffer.append("\nCONFIGURA��O\n");
		escolherConfiguracao(driver);
		
		errorBuffer.append(configurarPerfeitoNenhumCheckBox(driver)+"\n"); //teste perfeito sem nenhum checkbox
		errorBuffer.append(configurarPerfeitoTodosCheckBox(driver)+"\n"); //teste perfeito com todos os checkbox

		errorBuffer.append(configurarCampoContatosVazio(driver)+"\n"); //teste com campo contatos vazio
		
		errorBuffer.append(configurarCampoNomeVazio(driver)+"\n"); //teste com campo data de nome vazio
		
		return errorBuffer;
	}
	
	public StringBuffer testarEdicao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		
		clearEdicao();
		
		errorBuffer.append("\nEDI��O\n");
		escolherConfiguracao(driver);
		
		errorBuffer.append(editarCampoContatos(driver)+"\n"); //teste com campo contatos
		errorBuffer.append(editarCampoContatosVazio(driver)+"\n"); //teste com campo contatos vazio
		
		errorBuffer.append(editarCampoNome(driver)+"\n"); //teste com campo data de nome
		errorBuffer.append(editarCampoNomeVazio(driver)+"\n"); //teste com campo data de nome vazio
		
		errorBuffer.append(editarCampoExcessoV(driver)+"\n"); //teste com campo data de excessoV
		errorBuffer.append(editarCampoViolacaoC(driver)+"\n"); //teste com campo data de violacaoC
		errorBuffer.append(editarCampoFreadaB(driver)+"\n"); //teste com campo data de freadaB
		errorBuffer.append(editarCampoViolacaoR(driver)+"\n"); //teste com campo data de violacaoR
		errorBuffer.append(editarCampoNomeM(driver)+"\n"); //teste com campo data de nomeM
		errorBuffer.append(editarCampoAceleracaoB(driver)+"\n"); //teste com campo data de aceleracaoB
		errorBuffer.append(editarCampoMovimentosI(driver)+"\n"); //teste com campo data de movimentosI
		errorBuffer.append(editarCampoMovimentosIH(driver)+"\n"); //teste com campo data de movimentosIH
		errorBuffer.append(editarCampoDesvioR(driver)+"\n"); //teste com campo data de desvioR
		errorBuffer.append(editarCampoExcessoVR(driver)+"\n"); //teste com campo data de excessoVR
		errorBuffer.append(editarCampoTemperatura(driver)+"\n"); //teste com campo data de temperatura
		errorBuffer.append(editarCampoMotorO(driver)+"\n"); //teste com campo data de motorO
		errorBuffer.append(editarCampoSemC(driver)+"\n"); //teste com campo data de semC
		errorBuffer.append(editarCampoCnhV(driver)+"\n"); //teste com campo data de cnhV
		errorBuffer.append(editarCampoAlertaB(driver)+"\n"); //teste com campo data de alertaB
		
		return errorBuffer;
	}

	public ConfiguracaoNotificacoes(WebDriver driver) {
		wait = new WebDriverWait(driver, 60);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+placaTela+"\n");
		
		//errorBuffer.append(testarConfiguracao(driver));
		errorBuffer.append(testarEdicao(driver));

		setDadosArquivo(errorBuffer);

	}
	
	public static void escolherConfiguracao(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirConfiguracao(driver)+"ul/li[4]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
        
        //Clica na aba desejada
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"exTab2\"]/div/div/ul/li[1]/a")));
        novo.click();
	}
	
	public static boolean configurar(WebDriver driver) {
		boolean tudoCerto = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em configurar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"1\"]/button")));
        novo.click();
                
        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nomeconfigedit")));
        js.executeScript("arguments[0].value=arguments[1]", nomeE,nome);
                
        try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        WebElement contatosE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"contatosedit_chosen\"]/ul/li/input")));
        if(contatos.size()>0) {
        	contatosE.clear();
        }
        for(String o:contatos) {
        	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	contatosE.sendKeys(o,Keys.ENTER);
        }
        
        Comandos.clicarCheckboxNome(driver, "speedingedit", excessoV);
        Comandos.clicarCheckboxNome(driver, "brakeedit", freadaB);
        Comandos.clicarCheckboxNome(driver, "fenceedit", violacaoC);
        Comandos.clicarCheckboxNome(driver, "routeedit", violacaoR);
        Comandos.clicarCheckboxNome(driver, "maintenanceedit", nomeM);
        Comandos.clicarCheckboxNome(driver, "movementedit", movimentosI);
        Comandos.clicarCheckboxNome(driver, "accelerationedit", aceleracaoB);
        Comandos.clicarCheckboxNome(driver, "movementscheduleedit", movimentosIH);
        Comandos.clicarCheckboxNome(driver, "limit_speed_routeedit", excessoVR);
        Comandos.clicarCheckboxNome(driver, "temperatureedit", temperatura);
        Comandos.clicarCheckboxNome(driver, "route_deviationedit", desvioR);
        Comandos.clicarCheckboxNome(driver, "motor_ociosoedit", motorO);
        Comandos.clicarCheckboxNome(driver, "offComedit", semC);
        Comandos.clicarCheckboxNome(driver, "cnh", cnhV);
        Comandos.clicarCheckboxNome(driver, "batteryedit", alertaB);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
                
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        if(alert.getText().equals("Dados inseridos com sucesso!"))
	        	tudoCerto = true;
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button")));
            fechar.click();
        	e.printStackTrace();
        }
        
        return tudoCerto;
	}
	
	public static void editar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em configurar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
        novo.click();
                
        if(nome != null) {
	        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nomeconfigedit")));
	        js.executeScript("arguments[0].value=arguments[1]", nomeE,nome);
        }
        
        if(contatos != null) {
	        WebElement contatosE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"contatosedit_chosen\"]/ul/li/input")));
	        if(contatos.size()>0) {
	        	contatosE.clear();
	        }
	        for(String o:contatos) {
	        	try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	contatosE.sendKeys("         ",o,"         ",Keys.ENTER);
	        }
        }
        
        if(editarCheckbox) {
	        Comandos.clicarCheckboxNome(driver, "speedingedit", excessoV);
	        Comandos.clicarCheckboxNome(driver, "brakeedit", freadaB);
	        Comandos.clicarCheckboxNome(driver, "fenceedit", violacaoC);
	        Comandos.clicarCheckboxNome(driver, "routeedit", violacaoR);
	        Comandos.clicarCheckboxNome(driver, "maintenanceedit", nomeM);
	        Comandos.clicarCheckboxNome(driver, "movementedit", movimentosI);
	        Comandos.clicarCheckboxNome(driver, "accelerationedit", aceleracaoB);
	        Comandos.clicarCheckboxNome(driver, "movementscheduleedit", movimentosIH);
	        Comandos.clicarCheckboxNome(driver, "limit_speed_routeedit", excessoVR);
	        Comandos.clicarCheckboxNome(driver, "temperatureedit", temperatura);
	        Comandos.clicarCheckboxNome(driver, "route_deviationedit", desvioR);
	        Comandos.clicarCheckboxNome(driver, "motor_ociosoedit", motorO);
	        Comandos.clicarCheckboxNome(driver, "offComedit", semC);
	        Comandos.clicarCheckboxNome(driver, "cnh", cnhV);
	        Comandos.clicarCheckboxNome(driver, "batteryedit", alertaB);
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
                
        clearEdicao();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	
        	e.printStackTrace();
        }
        
	}
	
	public static StringBuffer configurarPerfeitoNenhumCheckBox(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarPerfeitoNenhumCheckBox";
		
		
				
		try {
			Assert.assertTrue(configurar(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "CONFIGURA��O N�O EFETUADA MESMO COM DADOS CORRETOS SEM NENHUM CHECKBOX";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer configurarPerfeitoTodosCheckBox(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarPerfeitoTodosCheckBox";
		
		excessoV = true;
		violacaoC = true;
		freadaB = true;
		violacaoR = true;
		nomeM = true;
		aceleracaoB = true;
		movimentosI = true;
		movimentosIH = true;
		excessoVR = true;
		desvioR = true;
		temperatura = true;
		motorO = true;
		semC = true;
		cnhV = true;
		alertaB = true;
				
		try {
			Assert.assertTrue(configurar(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "CONFIGURA��O N�O EFETUADA MESMO COM DADOS CORRETOS COM TODOS OS CHECKBOX";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
        return errorBuffer;
        
	}
		
	public static StringBuffer configurarCampoContatosVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarCampoContatosVazio";
				
		contatos = new ArrayList<String>();
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM CAMPO CONTATOS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoContatos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoContatos";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CONTATOS! ";		
		
		contatos = new ArrayList<String>();
		contatos.add("Saulo Mendes");
		contatos.add("thiago Oliveira");
		
		List<String> novoValor = contatos;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"1\"]/button")));
	        editar.click();
	        
	        Assert.assertTrue(Comandos.compararListaElementosXpath(driver,"//*[@id=\"contatosedit_chosen\"]/ul",novoValor));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button")));
        fechar.click();
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoContatosVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoContatosVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO PLACAS CONTATOS! ";
		
		contatos = new ArrayList<String>();
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"1\"]/button")));
	        editar.click();
	        
	        if(Comandos.listarListaElementosXpath(driver, "//*[@id=\"contatosedit_chosen\"]/ul").size() > 0) {
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
	        }
	        else
	        	errorBuffer.append(Comandos.printTesteSucesso(teste));		
            
		}
		catch(Exception e) {
            e.printStackTrace();            
		}
		WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button")));
        fechar.click();        
        return errorBuffer;
	}
	
	public static StringBuffer configurarCampoNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarCampoNomeVazio";
				
		nome = "";
		
		
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM CAMPO DATA DE NOME VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoNome(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoNome";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO NOME! ";		
		
		nome = "(aaaaaaaa)";
		String novoValor = nome;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));

	        Assert.assertEquals(novoValor,editar.getAttribute("data-nome"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoNomeVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO NOME VAZIO! ";
		
		nome = "";
		String novoValor = nome;
		editar(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        
	        if(!novoValor.equals(editar.getAttribute("data-nome")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoViolacaoC(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoViolacaoC";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO VIOLA��O DE CERCA! ";		
		
		violacaoC = true;
		editarCheckbox = true;
		boolean novoValor = violacaoC;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "fenceedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoFreadaB(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoFreadaB";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO FREADA BRUSCA! ";		
		
		freadaB = true;
		editarCheckbox = true;
		boolean novoValor = freadaB;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "brakeedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoViolacaoR(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoViolacaoR";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO VIOLA��O DE ROTA! ";		
		
		violacaoR = true;
		editarCheckbox = true;
		boolean novoValor = violacaoR;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "routeedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoNomeM(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoNomeM";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO NOME DE MANUTEN��O! ";		
		
		nomeM = true;
		editarCheckbox = true;
		boolean novoValor = nomeM;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "maintenanceedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAceleracaoB(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAceleracaoB";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO ACELERA��O BRUSCA! ";		
		
		aceleracaoB = true;
		editarCheckbox = true;
		boolean novoValor = aceleracaoB;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "accelerationedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoMovimentosI(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoMovimentosI";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO MOVIMENTOS INDEVIDOS! ";		
		
		movimentosI = true;
		editarCheckbox = true;
		boolean novoValor = movimentosI;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "movementedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoMovimentosIH(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoMovimentosIH";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO MOVIMENTOS INDEVIDOS POR HOR�RIO! ";		
		
		movimentosIH = true;
		editarCheckbox = true;
		boolean novoValor = movimentosIH;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "movementscheduleedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoDesvioR(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoDesvioR";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO DESVIO DA ROTA! ";		
		
		desvioR = true;
		editarCheckbox = true;
		boolean novoValor = desvioR;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "route_deviationedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoExcessoVR(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoExcessoVR";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO EXCESSO DE VELOCIDADE - ROTA! ";		
		
		excessoVR = true;
		editarCheckbox = true;
		boolean novoValor = excessoVR;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "limit_speed_routeedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTemperatura(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTemperatura";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO TEMPERATURA! ";		
		
		temperatura = true;
		editarCheckbox = true;
		boolean novoValor = temperatura;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "temperatureedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoMotorO(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoMotorO";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO MOTOR OCIOSO! ";		
		
		motorO = true;
		editarCheckbox = true;
		boolean novoValor = motorO;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "motor_ociosoedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoSemC(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoSemC";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO SEM COMUNICA��O! ";		
		
		semC = true;
		editarCheckbox = true;
		boolean novoValor = semC;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "offComedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoCnhV(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoCnhV";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO CNH VENCIDA! ";		
		
		cnhV = true;
		editarCheckbox = true;
		boolean novoValor = cnhV;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "cnh");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoAlertaB(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoAlertaB";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO ALERTA DE BATERIA! ";		
		
		alertaB = true;
		editarCheckbox = true;
		boolean novoValor = alertaB;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "batteryedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoExcessoV(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoExcessoV";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO EXCESSO DE VELOCIDADE! ";		
		
		excessoV = true;
		editarCheckbox = true;
		boolean novoValor = excessoV;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/section[2]/div[4]/div[3]/div/div/div/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/a[1]")));
	        editar.click();

	        boolean valor = Comandos.testarCheckboxNome(driver, "speedingedit");		
	        
	        Assert.assertEquals(novoValor,valor);
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEdit\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		ConfiguracaoNotificacoes.dadosArquivo = dadosArquivo;
	}
}
