package tests.gestorfrotas.menuresponsivo.configuracoes;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class ConfiguracaoNotificacoesContatos extends Configuracoes{
	private static final String placaTela = "Configura��o de Notifica��es - Contatos";
	
	private static String nome = "teste do teste testado";
	private static String email = "teste@teste.com";
	private static String telefone = "(99) 99999-9999";
	
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		nome = "teste do teste testado";
		email = "teste@teste.com";
		telefone = "(99) 99999-9999";
	}
	
	public static void clearEdicao() {
		nome = null;
		email = null;
		telefone = null;
	}
	
	public StringBuffer testarConfiguracao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		clear();
		
		errorBuffer.append("\nCONFIGURA��O\n");errorBuffer.append("\nCONFIGURA��O\n");
		
		escolherConfiguracao(driver);
		
		errorBuffer.append(configurarPerfeito(driver)+"\n"); //teste perfeito sem nenhum checkbox
		
		errorBuffer.append(configurarCampoNomeVazio(driver)+"\n"); //teste com campo nome vazio
		errorBuffer.append(configurarCampoEmailVazio(driver)+"\n"); //teste com campo email vazio
		errorBuffer.append(configurarEmailInvalido(driver)+"\n"); //teste com email invalido
		errorBuffer.append(configurarCampoTelefoneVazio(driver)+"\n"); //teste com campo telefone vazio
		errorBuffer.append(configurarTelefoneInvalido(driver)+"\n"); //teste com telefone invalido
		
		return errorBuffer;
	}
	
	public StringBuffer testarEdicao(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		clearEdicao();
		
		errorBuffer.append("\nEDI��O\n");
		
		escolherConfiguracao(driver);

		errorBuffer.append(editarCampoNome(driver)+"\n"); //teste com campo nome
		errorBuffer.append(editarCampoNomeVazio(driver)+"\n"); //teste com campo nome vazio
		
		errorBuffer.append(editarCampoEmail(driver)+"\n"); //teste com campo email
		errorBuffer.append(editarCampoEmailVazio(driver)+"\n"); //teste com campo email vazio
		errorBuffer.append(editarEmailInvalido(driver)+"\n"); //teste com email invalido
		
		errorBuffer.append(editarCampoTelefone(driver)+"\n"); //teste com campo telefone
		errorBuffer.append(editarCampoTelefoneVazio(driver)+"\n"); //teste com campo telefone vazio
		errorBuffer.append(editarTelefoneInvalido(driver)+"\n"); //teste com telefone invalido
		
		return errorBuffer;
	}

	public ConfiguracaoNotificacoesContatos(WebDriver driver) {
		wait = new WebDriverWait(driver, 60);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+placaTela+"\n");
		
		//errorBuffer.append(testarConfiguracao(driver));
		errorBuffer.append(testarEdicao(driver));
		
		setDadosArquivo(errorBuffer);

	}
	
	public static void escolherConfiguracao(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
        //Abre menu de cadastros
        String xpath = abrirConfiguracao(driver)+"ul/li[4]/";
        
        //Abre o cadastro escolhido
        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
        js.executeScript("arguments[0].click();", cadastro);  
        
        //Clica na aba desejada
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"exTab2\"]/div/div/ul/li[2]/a")));
        novo.click();
	}
	
	public static boolean configurar(WebDriver driver) {
		boolean tudoCerto = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em configurar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"3\"]/button")));
        novo.click();
                
        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nomeeditc")));
        js.executeScript("arguments[0].value=arguments[1];", nomeE,nome);

        WebElement emailE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("emaileditc")));
        js.executeScript("arguments[0].value=arguments[1];", emailE,email);
                
        WebElement telefoneE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("telefoneeditc")));
        js.executeScript("arguments[0].value=arguments[1];", telefoneE,telefone);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmitEditContato")));
        botaoSubmit.click();
                
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        if(alert.getText().equals("Dados atualizados com sucesso!"))
	        	tudoCerto = true;
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEditContato\"]/div/div/div[1]/button")));
            fechar.click();
        	e.printStackTrace();
        }
        
        return tudoCerto;
	}
	
	public static void editar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        //Clica em configurar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"217\"]")));
        novo.click();
                
        if(nome != null) {
	        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nomeeditc")));
	        js.executeScript("arguments[0].value=arguments[1];", nomeE,nome);
        }
        
        if(email != null) {
		    WebElement emailE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("emaileditc")));
		    js.executeScript("arguments[0].value=arguments[1];", emailE,email);
        }
        
        if(telefone != null) {
	        WebElement telefoneE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("telefoneeditc")));
	        js.executeScript("arguments[0].value=arguments[1];", telefoneE,telefone);
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmitEditContato")));
        botaoSubmit.click();
                
        clearEdicao();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	
        	e.printStackTrace();
        }
        
	}
	
	public static StringBuffer configurarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarPerfeito";
			
		try {
			Assert.assertTrue(configurar(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "CONFIGURA��O N�O EFETUADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
        return errorBuffer;
        
	}
	
	public static StringBuffer configurarCampoNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarCampoNomeVazio";
				
		nome = "";
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM CAMPO NOME VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoNome(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoNome";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO NOME! ";		
		
		nome = "(aaaaaaaa)";
		String novoValor = nome;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"217\"]")));
	        editar.click();

	        Assert.assertEquals(novoValor,editar.getAttribute("data-nome"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEditContato\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoNomeVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO NOME VAZIO! ";
		
		nome = "";
		String novoValor = nome;
		editar(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"217\"]")));
	        editar.click();
	        
	        if(!novoValor.equals(editar.getAttribute("data-nome")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEditContato\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer configurarCampoEmailVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarCampoEmailVazio";
				
		email = "";
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM CAMPO EMAIL VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer configurarEmailInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarEmailInvalido";
				
		email = "abc!";
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM EMAIL INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoEmail(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoEmail";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO EMAIL! ";		
		
		email = "AAAA@hhhh.com";
		String novoValor = email;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"217\"]")));
	        editar.click();

	        Assert.assertEquals(novoValor,editar.getAttribute("data-email"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEditContato\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoEmailVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoEmailVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO EMAIL VAZIO! ";
		
		email = "";
		String novoValor = email;
		editar(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"217\"]")));
	        editar.click();
	        
	        if(!novoValor.equals(editar.getAttribute("data-email")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEditContato\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarEmailInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarEmailInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO EMAIL VAZIO! ";
		
		email = "abc!";
		String novoValor = email;
		editar(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"217\"]")));
	        editar.click();
	        
	        if(!novoValor.equals(editar.getAttribute("data-email")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEditContato\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer configurarCampoTelefoneVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarCampoTelefoneVazio";
				
		telefone = "";
		
		
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM CAMPO TELEFONE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer configurarTelefoneInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "configurarTelefoneInvalido";
				
		telefone = "abc!";
		
		
		
		try {
			Assert.assertTrue(configurar(driver));
			
			String motivo = "CONFIGURA��O � EFETUADA MESMO COM TELEFONE INV�LIDO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            
		}
        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTelefone(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTelefone";
		String motivo = "EDI��O N�O � EFETUADA NO CAMPO TELEFONE! ";		
		
		telefone = "(00) 00000-0000";
		String novoValor = telefone;
		editar(driver);
				
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"217\"]")));
	        editar.click();

	        Assert.assertEquals(novoValor,editar.getAttribute("data-telefone"));
	        errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError e) {
			motivo += "O VALOR N�O FOI EDITADO";
	    	errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEditContato\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarCampoTelefoneVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarCampoTelefoneVazio";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO TELEFONE VAZIO! ";
		
		telefone = "";
		String novoValor = telefone;
		editar(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"217\"]")));
	        editar.click();
	        
	        if(!novoValor.equals(editar.getAttribute("data-telefone")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEditContato\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public static StringBuffer editarTelefoneInvalido(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "editarTelefoneInvalido";
		String motivo = "EDI��O � EFETUADA MESMO COM CAMPO TELEFONE VAZIO! ";
		
		telefone = "abc!";
		String novoValor = telefone;
		editar(driver);
		
		try {
			WebElement editar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"217\"]")));
	        editar.click();
	        
	        if(!novoValor.equals(editar.getAttribute("data-telefone")))
	        	motivo += "O VALOR N�O FOI EDITADO PARA VAZIO";
	        else
	        	motivo += "O VALOR FOI EDITADO PARA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(Exception e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));		

		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myModalEditContato\"]/div/div/div[1]/button"))).click();

        return errorBuffer;
	}
	
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		ConfiguracaoNotificacoesContatos.dadosArquivo = dadosArquivo;
	}
}
