package tests.gestorfrotas.menuresponsivo.configuracoes;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class Configuracoes {
	private static WebDriverWait wait;
	private static String xpath = "//*[@id=\"sidebar-items\"]/li[5]/";
	
	public static String abrirConfiguracao(WebDriver driver) {
		String xpath = Configuracoes.xpath;
        wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		Comandos.menuResponsivo(driver); //abre menu responsivo

        WebElement cadastros = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath+"a")));
        cadastros.click();
        return xpath;
	}
}
