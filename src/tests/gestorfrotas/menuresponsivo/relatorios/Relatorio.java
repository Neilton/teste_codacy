package tests.gestorfrotas.menuresponsivo.relatorios;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class Relatorio {
	private static WebDriverWait wait;
	private static StringBuilder dadosArquivo = null;

	private static ArrayList<String> placas;
	private static ArrayList<String> grupos;
	private static String dataInicial;
	private static String dataFinal;
	private static String horaInicial;
	private static String horaFinal;
	private static boolean todosOsVeiculos;
	
	private static WebElement dataInicialE;
	private static WebElement dataFinalE;
	private static WebElement horaInicialE;
	private static WebElement horaFinalE;
	private static WebElement placasE;
	private static WebElement gruposE;
	private static WebElement todosOsVeiculosE;
	private static WebElement botaoSubmit;
	
	private static StringBuilder variaveis;
	private static String alerta;
	private static int count = 0;
		
	@SuppressWarnings("deprecation")
	public static void abrirRelatorio(WebDriver driver) {
		setWait(new WebDriverWait(driver, Comandos.getTime())); //Espera para elementos de outras p�ginas
		Comandos.menuResponsivo(driver); //abre menu responsivo

        WebElement relatorios = getWait().until(ExpectedConditions.elementToBeClickable(By.id("menuLateralEsquerdoRelatorios")));
        relatorios.click();
	}
	
	public static void abrirRelatorioSaver(WebDriver driver) {
		abrirRelatorio(driver);
		WebElement saver = getWait().until(ExpectedConditions.elementToBeClickable(By.id("menuLateralEsquerdoRelatoriosSaver")));
		saver.click();
	}
	
	public static void abrirRelatorioSaverEventosOcorridos(WebDriver driver) {
		abrirRelatorioSaver(driver);
		WebElement violacaoBateria = getWait().until(ExpectedConditions.elementToBeClickable(By.id("menuLateralEsquerdoRelatoriosSaverEventosOcorridos")));
		violacaoBateria.click();
	}

	public static WebDriverWait getWait() {
		return wait;
	}

	public static void setWait(WebDriverWait wait) {
		Relatorio.wait = wait;
	}

	public StringBuilder getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuilder errorBuffer) {
		Relatorio.dadosArquivo = errorBuffer;
	}

	public static List<String> getGrupos() {
		return grupos;
	}

	public static void setGrupos(List<String> grupos) {
		Relatorio.grupos = (ArrayList<String>) grupos;
	}

	public static List<String> getPlacas() {
		return placas;
	}

	public static void setPlacas(List<String> monitorados) {
		Relatorio.placas = (ArrayList<String>) monitorados;
	}

	public static String getDataInicial() {
		return dataInicial;
	}

	public static void setDataInicial(String dataInicial) {
		Relatorio.dataInicial = dataInicial;
	}

	public static String getDataFinal() {
		return dataFinal;
	}

	public static void setDataFinal(String dataFinal) {
		Relatorio.dataFinal = dataFinal;
	}

	public static String getHoraFinal() {
		return horaFinal;
	}

	public static void setHoraFinal(String horaFinal) {
		Relatorio.horaFinal = horaFinal;
	}

	public static StringBuilder getVariaveis() {
		return variaveis;
	}

	public static void setVariaveis(StringBuilder variaveis) {
		Relatorio.variaveis = variaveis;
	}

	public static int getCount() {
		return count;
	}

	public static void setCount(int count) {
		Relatorio.count = count;
	}

	public static String getAlerta() {
		return alerta;
	}

	public static void setAlerta(String alerta) {
		Relatorio.alerta = alerta;
	}

	public static String getHoraInicial() {
		return horaInicial;
	}

	public static void setHoraInicial(String horaInicial) {
		Relatorio.horaInicial = horaInicial;
	}

	public static WebElement getDataInicialE() {
		return dataInicialE;
	}

	public static void setDataInicialE(WebElement dataInicialE) {
		Relatorio.dataInicialE = dataInicialE;
	}

	public static WebElement getDataFinalE() {
		return dataFinalE;
	}

	public static void setDataFinalE(WebElement dataFinalE) {
		Relatorio.dataFinalE = dataFinalE;
	}

	public static WebElement getHoraInicialE() {
		return horaInicialE;
	}

	public static void setHoraInicialE(WebElement horaInicialE) {
		Relatorio.horaInicialE = horaInicialE;
	}

	public static WebElement getHoraFinalE() {
		return horaFinalE;
	}

	public static void setHoraFinalE(WebElement horaFinalE) {
		Relatorio.horaFinalE = horaFinalE;
	}

	public static WebElement getPlacasE() {
		return placasE;
	}

	public static void setPlacasE(WebElement monitoradosE) {
		Relatorio.placasE = monitoradosE;
	}

	public static WebElement getGruposE() {
		return gruposE;
	}

	public static void setGruposE(WebElement gruposE) {
		Relatorio.gruposE = gruposE;
	}

	public static WebElement getBotaoSubmit() {
		return botaoSubmit;
	}

	public static void setBotaoSubmit(WebElement botaoSubmit) {
		Relatorio.botaoSubmit = botaoSubmit;
	}

	public static boolean isTodosOsVeiculos() {
		return todosOsVeiculos;
	}
	
	public static String isTodosOsVeiculosCheck() {
		if(isTodosOsVeiculos())
			return "SELECIONADO";
		else
			return "N�O SELECIONADO";
	}

	public static void setTodosOsVeiculos(boolean todosOsVeiculos) {
		Relatorio.todosOsVeiculos = todosOsVeiculos;
	}

	public static WebElement getTodosOsVeiculosE() {
		return todosOsVeiculosE;
	}

	public static void setTodosOsVeiculosE(WebElement todosOsVeiculosE) {
		Relatorio.todosOsVeiculosE = todosOsVeiculosE;
	}
	
}
