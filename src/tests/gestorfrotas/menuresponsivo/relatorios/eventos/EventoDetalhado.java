package tests.gestorfrotas.menuresponsivo.relatorios.eventos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorfrotas.menuresponsivo.relatorios.Relatorios;
import tests.util.Comandos;

public class EventoDetalhado extends Relatorios{
	private static final String nomeTela = "Relat�rio Eventos Detalhados";
	
	private static ArrayList<String> placas = new ArrayList<String>();      
	private static String dataInicial = "01/01/2020";
	private static String horaInicial = "00:00:01";
	private static String dataFinal = "07/01/2020";
	private static String horaFinal = "23:59:59";
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public EventoDetalhado(WebDriver driver) {
		wait = new WebDriverWait(driver, 2);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
		
		errorBuffer.append(gerarRelatorioHoraInicialCharInvalidos(driver)+"\n"); //teste com hora inicial com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioHoraFinalCharInvalidos(driver)+"\n"); //teste com hora final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataInicialCharInvalidos(driver)+"\n"); //teste com data inicial com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataFinalCharInvalidos(driver)+"\n"); //teste com data final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioHoraInicialVazia(driver)+"\n"); //teste com hora inicial vazia
		
		errorBuffer.append(gerarRelatorioHoraFinalVazia(driver)+"\n"); //teste com hora final vazia

		errorBuffer.append(gerarRelatorioDataInicialVazia(driver)+"\n"); //teste com data incial vazia

		errorBuffer.append(gerarRelatorioDataFinalVazia(driver)+"\n"); //teste com data final vazia

		errorBuffer.append(gerarRelatorioCampoPlacasVazio(driver)+"\n"); //teste com campo placas vazio
		        
		errorBuffer.append(gerarRelatorioPeriodoMaiorQueLimite(driver)+"\n"); //teste com diferen�a entre datas maioir que o limite
        
		errorBuffer.append(gerarRelatorioHoraFinalMaiorQueLimite(driver)+"\n"); //teste com hora final maior do que deveria
		 
		errorBuffer.append(gerarRelatorioHoraInicialMaiorQueLimite(driver)+"\n"); //teste com hora inicial maior do que um dia
		
		errorBuffer.append(gerarRelatorioDataFinalMaiorQueAtual(driver)+"\n"); //teste com data final maior do que data atual
		
		errorBuffer.append(gerarRelatorioDataInicialMaiorQueFinal(driver)+"\n"); //teste com data final maior do que data inicial

		errorBuffer.append(gerarRelatorioPlacasInvalidas(driver)+"\n"); //teste com placas as quais o sistema n�o possui
		
		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
		placas = new ArrayList<String>();
		dataInicial = "01/01/2020";
	    horaInicial = "00:00:01";
	    dataFinal = "07/01/2020";
	    horaFinal = "23:59:59";
	}
	
	public static void gerarRelatorio(WebDriver driver, String dataInicial,String dataFinal,String horaInicial,String horaFinal,ArrayList<String> placas) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
        
        //abre menu de relat�rios de eventos
        abrirRelatorioEventos(driver);
        
        //Abre o relat�rio escolhido
        WebElement eventoDetalhado = driver.findElement(By.xpath("//*[@id=\"sidebar-items\"]/li[2]/ul/li[1]/ul/li[3]/a"));
        js.executeScript("arguments[0].scrollIntoView(true);", eventoDetalhado);
        js.executeScript("arguments[0].click();", eventoDetalhado);
        
        WebElement dataInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("init")));
        dataInicialE.clear();
        dataInicialE.sendKeys(dataInicial);
        
        WebElement horaInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hr_ini")));
        horaInicialE.clear();
        horaInicialE.sendKeys(horaInicial);
        
        WebElement dataFinalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("end")));
        dataFinalE.clear();
        dataFinalE.sendKeys(dataFinal);
        
        WebElement horaFinalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hr_end")));
        horaFinalE.clear();
        horaFinalE.sendKeys(horaFinal);
        
        WebElement placasE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placas_chosen\"]/ul/li/input")));
        placasE.clear();
        for(String o:placas) {
        	try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	placasE.sendKeys(o,Keys.ENTER);
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("btnSubmit")));
        botaoSubmit.click();
        
        clear();
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static StringBuffer gerarRelatorioPlacasInvalidas(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPlacasInvalidas";
		
		placas.add("GAGASGAS");
        placas.add("HSAFAFASF");
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO PLACAS INV�LIDAS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialCharInvalidos";
		
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        horaInicial = "abc!";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalCharInvalidos";
		
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        horaFinal = "abc!";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                       
        try{
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialCharInvalidos";
		
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "abc!";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalCharInvalidos";
		
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataFinal = "abc!";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialVazia";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "01/01/2020";
        horaInicial = "";
        dataFinal = "07/01/2020";
        horaFinal = "23:59:59";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalVazia";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "01/01/2020";
        horaInicial = "00:00:01";
        dataFinal = "07/01/2020";
        horaFinal = "";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalVazia";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "01/01/2020";
        horaInicial = "00:00:01";
        dataFinal = "";
        horaFinal = "23:59:59";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialVazia";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "";
        horaInicial = "00:00:01";
        dataFinal = "07/01/2020";
        horaFinal = "23:59:59";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioCampoPlacasVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioCampoPlacasVazio";
		
		placas = new ArrayList<String>();
        
        dataInicial = "01/01/2020";
        horaInicial = "00:00:01";
        dataFinal = "07/01/2020";
        horaFinal = "23:59:59";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM CAMPO PLACAS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioPeriodoMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPeriodoMaiorQueLimite";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "01/01/2020";
        horaInicial = "00:00:01";
        dataFinal = "09/01/2020";
        horaFinal = "23:59:59";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM PER�ODO ENTRE DATAS MAIOR QUE LIMITE";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalMaiorQueLimite";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "01/01/2020";
        horaInicial = "00:00:01";
        dataFinal = "07/01/2020";
        horaFinal = "24:00:01";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}

	public static StringBuffer gerarRelatorioDataInicialMaiorQueFinal(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialMaiorQueFinal";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "06/01/2020";
        horaInicial = "00:00:01";
        dataFinal = "01/01/2020";
        horaFinal = "23:59:59";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL MAIOR QUE FINAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialMaiorQueLimite";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "01/01/2020";
        horaInicial = "50:00:01";
        dataFinal = "06/01/2020";
        horaFinal = "23:59:59";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
                        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalMaiorQueAtual(WebDriver driver) {
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
		
		String data = formatoData.format(calendario.getTime());
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalMaiorQueAtual";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "01/01/2020";
        horaInicial = "00:00:01";
        dataFinal = data;
        horaFinal = "23:59:59";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL MAIOR QUE ATUAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
        
	}
	
	public static StringBuffer gerarRelatorioPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPerfeito";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "01/01/2020";
        horaInicial = "00:00:01";
        dataFinal = "06/01/2020";
        horaFinal = "23:59:59";
		
        gerarRelatorio(driver, dataInicial,dataFinal,horaInicial,horaFinal, placas);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[5]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
            String motivo = "TABELA N�O � GERADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        
        return errorBuffer;
        
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		EventoDetalhado.dadosArquivo = dadosArquivo;
	}
	
}
