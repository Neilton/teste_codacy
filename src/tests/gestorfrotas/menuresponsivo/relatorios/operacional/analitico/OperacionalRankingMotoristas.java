package tests.gestorfrotas.menuresponsivo.relatorios.operacional.analitico;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorfrotas.menuresponsivo.relatorios.Relatorios;
import tests.util.Comandos;

public class OperacionalRankingMotoristas extends Relatorios{
	private static final String nomeTela = "Relat�rio Operacional de Ranking dos Motoristas";
	
	private static String dataInicial = "01/01/2020";
	private static String dataFinal = "07/01/2020";
	private static String horaInicial = "00:00:01";
	private static String horaFinal = "23:59:59";
	private static ArrayList<String> motoristas = new ArrayList<String>();
	private static String excVelo = "0";
	private static String aceBrus = "0";
	private static String freBrus = "0";
	private static String motOcio = "0";
	private static String kmRodado = "0";

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public OperacionalRankingMotoristas(WebDriver driver) {
		wait = new WebDriverWait(driver, 15);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
			
		errorBuffer.append(gerarRelatorioExcVelCharInvalidos(driver)+"\n"); //teste com excesso de velocidade com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioAceBruCharInvalidos(driver)+"\n"); //teste com acelera��o brusca com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioFreBruCharInvalidos(driver)+"\n"); //teste com freada brusca com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioMotOciCharInvalidos(driver)+"\n"); //teste com motor ocioso com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioKmRodCharInvalidos(driver)+"\n"); //teste com km rodado com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioExcVelVazia(driver)+"\n"); //teste com excesso de velocidade com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioAceBruVazia(driver)+"\n"); //teste com acelera��o brusca com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioFreBruVazia(driver)+"\n"); //teste com freada brusca com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioMotOciVazia(driver)+"\n"); //teste com motor ocioso com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioKmRodVazia(driver)+"\n"); //teste com km rodado com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioHoraInicialCharInvalidos(driver)+"\n"); //teste com hora inicial com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioHoraFinalCharInvalidos(driver)+"\n"); //teste com hora final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioHoraInicialVazia(driver)+"\n"); //teste com hora inicial vazia
		
		errorBuffer.append(gerarRelatorioHoraFinalVazia(driver)+"\n"); //teste com hora final vazia

		errorBuffer.append(gerarRelatorioHoraFinalMaiorQueLimite(driver)+"\n"); //teste com hora final maior do que deveria
		 
		errorBuffer.append(gerarRelatorioHoraInicialMaiorQueLimite(driver)+"\n"); //teste com hora inicial maior do que um dia
		
		errorBuffer.append(gerarRelatorioDataInicialCharInvalidos(driver)+"\n"); //teste com data inicial com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataFinalCharInvalidos(driver)+"\n"); //teste com data final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataInicialVazia(driver)+"\n"); //teste com data incial vazia

		errorBuffer.append(gerarRelatorioDataFinalVazia(driver)+"\n"); //teste com data final vazia

		errorBuffer.append(gerarRelatorioCampoMotoristasVazio(driver)+"\n"); //teste com campo motoristas vazio
		        
		errorBuffer.append(gerarRelatorioPeriodoMaiorQueLimite(driver)+"\n"); //teste com diferen�a entre datas maioir que o limite
       
		errorBuffer.append(gerarRelatorioDataFinalMaiorQueAtual(driver)+"\n"); //teste com data final maior do que data atual
		
		errorBuffer.append(gerarRelatorioDataInicialMaiorQueFinal(driver)+"\n"); //teste com data final maior do que data inicial
				
		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
	    dataFinal = "07/01/2020";
	    dataInicial = "01/01/2020";
	    horaInicial = "00:00:01";
	    horaFinal = "23:59:59";
	    motoristas = new ArrayList<String>();
		excVelo = "0";
		aceBrus = "0";
		freBrus = "0";
		motOcio = "0";
		kmRodado = "0";
	}
	
	public static void gerarRelatorio(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
        
        //abre menu de relat�rios operacionais
        abrirRelatorioOperacionalAnalitico(driver);
        
        //Abre o relat�rio escolhido
        WebElement eventoDetalhado = driver.findElement(By.xpath("//*[@id=\"sidebar-items\"]/li[2]/ul/li[2]/ul/li[1]/ul/li[9]/a"));
        js.executeScript("arguments[0].scrollIntoView(true);", eventoDetalhado);
        js.executeScript("arguments[0].click();", eventoDetalhado);
        
        WebElement dataInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("init")));
        dataInicialE.clear();
        dataInicialE.sendKeys(dataInicial);
        
        WebElement dataFinalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("end")));
        dataFinalE.clear();
        dataFinalE.sendKeys(dataFinal);
        
        WebElement horaInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora_ini")));
        horaInicialE.clear();
        horaInicialE.sendKeys(horaInicial);
        
        WebElement horaFinalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora_fim")));
        horaFinalE.clear();
        horaFinalE.sendKeys(horaFinal);
        
        WebElement motoristasE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"motoristas_chosen\"]/ul/li/input")));
        motoristasE.clear();
        for(String o:motoristas) {
        	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	motoristasE.sendKeys(o,Keys.ENTER);
        }
        
        WebElement velocidadeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("velocidade")));
        velocidadeE.clear();
        velocidadeE.sendKeys(excVelo);
        
        WebElement aceleracaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("aceleracao")));
        aceleracaoE.clear();
        aceleracaoE.sendKeys(aceBrus);
        
        WebElement freadaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("freada")));
        freadaE.clear();
        freadaE.sendKeys(freBrus);
        
        WebElement motorOscE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("motor_ocioso")));
        motorOscE.clear();
        motorOscE.sendKeys(motOcio);
        
        WebElement kmRodadoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("km_rodado")));
        kmRodadoE.clear();
        kmRodadoE.sendKeys(kmRodado);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
        
        clear();
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		
	public static StringBuffer gerarRelatorioExcVelCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioExcVelCharInvalidos";
		
		motoristas.add("teste gustavo");
        
        
        excVelo = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM EXCESSO DE VELOCIDADE COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioAceBruCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioAceBruCharInvalidos";
		
		motoristas.add("teste gustavo");
        
        
        aceBrus = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM ACELERA��O BRUSCA COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioFreBruCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialCharInvalidos";
		
		motoristas.add("teste gustavo");
        
        
        freBrus = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM FREADA BRUSCA COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioMotOciCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioMotOciCharInvalidos";
		
		motoristas.add("teste gustavo");
        
        
        motOcio = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM MOTOR OCIOSO COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioKmRodCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioKmRodCharInvalidos";
		
		motoristas.add("teste gustavo");
        
        
        kmRodado = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM KM RODADOS COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
		
	public static StringBuffer gerarRelatorioHoraInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialCharInvalidos";
		
		motoristas.add("teste gustavo");
        
        
        horaInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalCharInvalidos";
		
		motoristas.add("teste gustavo");
        
        
        horaFinal = "abc!";
		
        gerarRelatorio(driver);
                       
        try{
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioExcVelVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioExcVelVazia";
		
		motoristas.add("teste gustavo");
        
        
        excVelo = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM EXCESSO DE VELOCIDADE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioAceBruVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioAceBruVazia";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        aceBrus = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM ACELERA��O BRUSCA VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioFreBruVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioFreBruVazia";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        freBrus = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM FREADA BRUSCA VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioMotOciVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioMotOciVazia";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        motOcio = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM MOTOR OCIOSO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioKmRodVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatoriokmRodVazia";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        kmRodado = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM KM RODADO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialVazia";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        horaInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalVazia";
		
		motoristas.add("teste gustavo");
        
        
        horaFinal = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalMaiorQueLimite";
		
		motoristas.add("teste gustavo");
        
        
        horaFinal = "24:00:01";
		
        gerarRelatorio(driver);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialMaiorQueLimite";
		
		motoristas.add("teste gustavo");
        
        
        horaInicial = "50:00:01";
		
        gerarRelatorio(driver);
                        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialCharInvalidos";
		
		motoristas.add("teste gustavo");
        
        
        dataInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalCharInvalidos";
		
		motoristas.add("teste gustavo");
        
        
        dataFinal = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalVazia";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        dataFinal = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialVazia";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        dataInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioCampoMotoristasVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioCampoMotoristasVazio";
				
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM CAMPO MOTORISTAS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioPeriodoMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPeriodoMaiorQueLimite";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        dataInicial = "01/01/2020";
        dataFinal = "02/02/2020";
		
        gerarRelatorio(driver);
       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM PER�ODO ENTRE DATAS MAIOR QUE LIMITE";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	
	public static StringBuffer gerarRelatorioDataInicialMaiorQueFinal(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialMaiorQueFinal";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        dataInicial = "06/01/2020";
        dataFinal = "01/01/2020";
		
        gerarRelatorio(driver);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL MAIOR QUE FINAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
		
	public static StringBuffer gerarRelatorioDataFinalMaiorQueAtual(WebDriver driver) {
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
		
		String data = formatoData.format(calendario.getTime());
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalMaiorQueAtual";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        
        dataFinal = data;
		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL MAIOR QUE ATUAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
        
	}
	
	public static StringBuffer gerarRelatorioPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPerfeito";
		
		motoristas = new ArrayList<String>();
		motoristas.add("teste gustavo");
        
        		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[6]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
            String motivo = "TABELA N�O � GERADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        
        return errorBuffer;
        
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		OperacionalRankingMotoristas.dadosArquivo = dadosArquivo;
	}
	
}
