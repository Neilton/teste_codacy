package tests.gestorfrotas.menuresponsivo.relatorios.operacional.jornada;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorfrotas.menuresponsivo.relatorios.Relatorios;
import tests.util.Comandos;

public class OperacionalJornadaTrabalho extends Relatorios{
	private static final String nomeTela = "Relat�rio Operacional de Jornada de Trabalho";
	
	private static String dataInicial = "01/01/2020";
	private static String dataFinal = "07/01/2020";
	private static String horaInicial = "00:00:01";
	private static String horaFinal = "23:59:59";
	private static String intervalo = "00:00:01";
	private static String placa = "ACK0588"; 

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public OperacionalJornadaTrabalho(WebDriver driver) {
		wait = new WebDriverWait(driver, 30);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
			
		errorBuffer.append(gerarRelatorioIntervaloCharInvalidos(driver)+"\n"); //teste com intervalo com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioIntervaloCharVazio(driver)+"\n"); //teste com intervalo vazia

		errorBuffer.append(gerarRelatorioHoraInicialCharInvalidos(driver)+"\n"); //teste com hora inicial com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioHoraFinalCharInvalidos(driver)+"\n"); //teste com hora final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioHoraInicialVazia(driver)+"\n"); //teste com hora inicial vazia
		
		errorBuffer.append(gerarRelatorioHoraFinalVazia(driver)+"\n"); //teste com hora final vazia

		errorBuffer.append(gerarRelatorioHoraFinalMaiorQueLimite(driver)+"\n"); //teste com hora final maior do que deveria
		 
		errorBuffer.append(gerarRelatorioHoraInicialMaiorQueLimite(driver)+"\n"); //teste com hora inicial maior do que um dia
		
		errorBuffer.append(gerarRelatorioDataInicialCharInvalidos(driver)+"\n"); //teste com data inicial com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataFinalCharInvalidos(driver)+"\n"); //teste com data final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataInicialVazia(driver)+"\n"); //teste com data incial vazia

		errorBuffer.append(gerarRelatorioDataFinalVazia(driver)+"\n"); //teste com data final vazia

		errorBuffer.append(gerarRelatorioCampoPlacasVazio(driver)+"\n"); //teste com campo placas vazio
		        
		errorBuffer.append(gerarRelatorioPeriodoMaiorQueLimite(driver)+"\n"); //teste com diferen�a entre datas maioir que o limite
       
		errorBuffer.append(gerarRelatorioDataFinalMaiorQueAtual(driver)+"\n"); //teste com data final maior do que data atual
		
		errorBuffer.append(gerarRelatorioDataInicialMaiorQueFinal(driver)+"\n"); //teste com data final maior do que data inicial
				
		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
	    dataFinal = "07/01/2020";
	    dataInicial = "01/01/2020";
	    horaInicial = "00:00:01";
	    horaFinal = "23:59:59";
	    intervalo = "00:00:01";
	    placa = "ACK0588"; 
	}
	
	public static void gerarRelatorio(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
        
        //abre menu de relat�rios operacionais
        abrirRelatorioOperacionalJornada(driver);
        
        //Abre o relat�rio escolhido
        int count = 0;
        int maxTries = 10;
        while(true) {
	        try {
	        	WebElement eventoDetalhado = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"sidebar-items\"]/li[2]/ul/li[2]/ul/li[2]/ul/li[4]/a")));
	            js.executeScript("arguments[0].scrollIntoView(true);", eventoDetalhado);
	            js.executeScript("arguments[0].click();", eventoDetalhado);
	             	     		
	            break;
	        }catch(TimeoutException e) {
	        	if(++count == maxTries) throw e;
	        }catch(UnhandledAlertException e) {
	        	try {
	        	Alert alert = driver.switchTo().alert();
     	        alert.accept();
	        	}catch(Exception e1) {
	        		e1.printStackTrace();
	        	}
	        	
     	        WebElement eventoDetalhado = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"sidebar-items\"]/li[2]/ul/li[2]/ul/li[2]/ul/li[4]/a")));
	            js.executeScript("arguments[0].scrollIntoView(true);", eventoDetalhado);
	            js.executeScript("arguments[0].click();", eventoDetalhado);
	        }
        }
        
        WebElement dataInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data-inicio")));
        dataInicialE.clear();
        dataInicialE.sendKeys(dataInicial);
        
        WebElement dataFinalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data-fim")));
        dataFinalE.clear();
        dataFinalE.sendKeys(dataFinal);
        
        WebElement horaInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora-inicio")));
        horaInicialE.clear();
        horaInicialE.sendKeys(horaInicial);
        
        WebElement horaFinalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora-fim")));
        horaFinalE.clear();
        horaFinalE.sendKeys(horaFinal);
        
        WebElement intervaloE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("intervalo")));
        intervaloE.clear();
        intervaloE.sendKeys(intervalo);
                  
        WebElement placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"seletor_placa_chosen\"]/div/div/input")));
        placaE.clear();
        placaE.sendKeys(placa,Keys.ENTER);
        
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("submit-relatorio")));
        botaoSubmit.click();
        
        clear();
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		
	public static StringBuffer gerarRelatorioHoraInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialCharInvalidos";
		
        horaInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioIntervaloCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioIntervaloCharInvalidos";
		
        intervalo = "abc!";
		
        gerarRelatorio(driver);
        
                       
        try{ 
            WebElement intervaloE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("intervalo")));
            Assert.assertTrue(intervaloE.getText().equals("abc!"));

            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM INTERVALO COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(AssertionError e) {
        	e.printStackTrace();
        	errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}	
	
	public static StringBuffer gerarRelatorioIntervaloCharVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioIntervaloCharVazio";
		
        intervalo = "";
		
        gerarRelatorio(driver);
                  
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
        	String motivo = "TABELA N�O � GERADA COM INTERVALO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        return errorBuffer;
	}	
		
	public static StringBuffer gerarRelatorioHoraFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalCharInvalidos";
		
        horaFinal = "abc!";
		
        gerarRelatorio(driver);
                       
        try{
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	
	public static StringBuffer gerarRelatorioHoraInicialVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialVazia";
		
        horaInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalVazia";
		
        horaFinal = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalMaiorQueLimite";
		
        horaFinal = "24:00:01";
		
        gerarRelatorio(driver);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialMaiorQueLimite";
		
		
        
        
        horaInicial = "50:00:01";
		
        gerarRelatorio(driver);
                        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialCharInvalidos";
		
		
        
        
        dataInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalCharInvalidos";
		
		
        
        
        dataFinal = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalVazia";
		
		
		
        
        
        dataFinal = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialVazia";
		
		
		
        
        
        dataInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioCampoPlacasVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioCampoPlacasVazio";
				
        gerarRelatorio(driver);
        placa = "";
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM CAMPO PLACAS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioPeriodoMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPeriodoMaiorQueLimite";
		
		
		
        
        
        dataInicial = "01/01/2020";
        dataFinal = "02/02/2020";
		
        gerarRelatorio(driver);
       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM PER�ODO ENTRE DATAS MAIOR QUE LIMITE";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	
	public static StringBuffer gerarRelatorioDataInicialMaiorQueFinal(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialMaiorQueFinal";
		
		
		
        
        
        dataInicial = "06/01/2020";
        dataFinal = "01/01/2020";
		
        gerarRelatorio(driver);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL MAIOR QUE FINAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
		
	public static StringBuffer gerarRelatorioDataFinalMaiorQueAtual(WebDriver driver) {
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
		
		String data = formatoData.format(calendario.getTime());
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalMaiorQueAtual";
		
		
		
        
        
        dataFinal = data;
		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL MAIOR QUE ATUAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
        
	}
	
	public static StringBuffer gerarRelatorioPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPerfeito";
		
		
		
        
        		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
            String motivo = "TABELA N�O � GERADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        
        return errorBuffer;
        
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		OperacionalJornadaTrabalho.dadosArquivo = dadosArquivo;
	}
	
}
