package tests.gestorfrotas.menuresponsivo.relatorios.operacional.jornada;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorfrotas.menuresponsivo.relatorios.Relatorios;
import tests.util.Comandos;

public class OperacionalPontoMotorista extends Relatorios{
	private static final String nomeTela = "Relat�rio Operacional de Ponto de Motorista";
	
	private static String dataInicial = "01/01/2020";
	private static String dataFinal = "07/01/2020";
	private static String motorista = "teste gustavo";
	
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public OperacionalPontoMotorista(WebDriver driver) {
		wait = new WebDriverWait(driver, 15);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
			
		errorBuffer.append(gerarRelatorioDataInicialCharInvalidos(driver)+"\n"); //teste com data inicial com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataFinalCharInvalidos(driver)+"\n"); //teste com data final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataInicialVazia(driver)+"\n"); //teste com data incial vazia

		errorBuffer.append(gerarRelatorioDataFinalVazia(driver)+"\n"); //teste com data final vazia
		        
		errorBuffer.append(gerarRelatorioPeriodoMaiorQueLimite(driver)+"\n"); //teste com diferen�a entre datas maioir que o limite
       
		errorBuffer.append(gerarRelatorioDataFinalMaiorQueAtual(driver)+"\n"); //teste com data final maior do que data atual
		
		errorBuffer.append(gerarRelatorioDataInicialMaiorQueFinal(driver)+"\n"); //teste com data final maior do que data inicial
				
		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
		motorista = "teste gustavo";
	    dataFinal = "07/01/2020";
	    dataInicial = "01/01/2020";
	}
	
	public static void gerarRelatorio(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
        
        //abre menu de relat�rios operacionais
        String xpath = abrirRelatorioOperacionalJornada(driver) + "ul/li[6]/";
        
        //Abre o relat�rio escolhido
        WebElement eventoDetalhado = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", eventoDetalhado);
        js.executeScript("arguments[0].click();", eventoDetalhado);
        
        WebElement dataInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("dt_ini")));
        dataInicialE.clear();
        dataInicialE.sendKeys(dataInicial);
        
        WebElement dataFinalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("dt_fim")));
        dataFinalE.clear();
        dataFinalE.sendKeys(dataFinal);
                      
        WebElement baseE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"motoristas_chosen\"]/div/div/input")));
        baseE.clear();
        baseE.sendKeys(motorista,Keys.ENTER);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
        
        clear();
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		
	public static StringBuffer gerarRelatorioDataInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialCharInvalidos";
		
		
        
        
        dataInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[3]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalCharInvalidos";
		
		
        
        
        dataFinal = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[3]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalVazia";
		
		
		
        
        
        dataFinal = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[3]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialVazia";
		
		
		
        
        
        dataInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[3]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioPeriodoMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPeriodoMaiorQueLimite";
		
		
		
        
        
        dataInicial = "01/01/2020";
        dataFinal = "02/02/2020";
		
        gerarRelatorio(driver);
       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[3]"))));                
            String motivo = "TABELA � GERADA MESMO COM PER�ODO ENTRE DATAS MAIOR QUE LIMITE";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	
	public static StringBuffer gerarRelatorioDataInicialMaiorQueFinal(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialMaiorQueFinal";
		
		
		
        
        
        dataInicial = "06/01/2020";
        dataFinal = "01/01/2020";
		
        gerarRelatorio(driver);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[3]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL MAIOR QUE FINAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
		
	public static StringBuffer gerarRelatorioDataFinalMaiorQueAtual(WebDriver driver) {
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
		
		String data = formatoData.format(calendario.getTime());
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalMaiorQueAtual";
		
		
		
        
        
        dataFinal = data;
		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[3]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL MAIOR QUE ATUAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
        
	}
	
	public static StringBuffer gerarRelatorioPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPerfeito";
		
		
		
        		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[3]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
            String motivo = "TABELA N�O � GERADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        
        return errorBuffer;
        
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		OperacionalPontoMotorista.dadosArquivo = dadosArquivo;
	}
	
}
