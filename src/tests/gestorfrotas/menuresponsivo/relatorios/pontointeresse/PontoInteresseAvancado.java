package tests.gestorfrotas.menuresponsivo.relatorios.pontointeresse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorfrotas.menuresponsivo.relatorios.Relatorios;
import tests.util.Comandos;

public class PontoInteresseAvancado extends Relatorios{
	private static final String nomeTela = "Relat�rio Ponto de Interesse Avan�ado";
	
	private static ArrayList<String> placas = new ArrayList<String>();
	private static String dataInicial = "01/01/2020";
	private static String raio = "100";
	private static int indexBase = 0;
	private static int indexPonto = 0;

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public PontoInteresseAvancado(WebDriver driver) {
		wait = new WebDriverWait(driver, 60);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
			
		errorBuffer.append(gerarRelatorioRaioCharInvalidos(driver)+"\n"); //teste com raio com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioRaioVazia(driver)+"\n"); //teste com raio vazio

		errorBuffer.append(gerarRelatorioRaioMaiorQueLimite(driver)+"\n"); //teste com raio maior que limite

		errorBuffer.append(gerarRelatorioRaioMenorQueLimite(driver)+"\n"); //teste com raio menor que limite

		errorBuffer.append(gerarRelatorioDataInicialCharInvalidos(driver)+"\n"); //teste com data inicial com caracteres inv�lidos
				
		errorBuffer.append(gerarRelatorioDataInicialVazia(driver)+"\n"); //teste com data incial vazia

		errorBuffer.append(gerarRelatorioCampoPlacasVazio(driver)+"\n"); //teste com campo placas vazio
		        
		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
		placas = new ArrayList<String>();
	    dataInicial = "01/01/2020";
	    indexBase = 0;
	    raio = "100";
	}
	
	public static void gerarRelatorio(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
        
        //abre menu de relat�rios ponto de interesse
        String xpath = abrirRelatorioPontoInteresse(driver)+"ul/li[1]/";
        
        //Abre o relat�rio escolhido
        WebElement eventoDetalhado = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", eventoDetalhado);
        js.executeScript("arguments[0].click();", eventoDetalhado);
        
        WebElement dataInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("dt_ini")));
        dataInicialE.clear();
        dataInicialE.sendKeys(dataInicial);
                
        WebElement raioE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("raio")));
        raioE.clear();
        raioE.sendKeys(raio);
        
        WebElement sensoresE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("grupo")));
        if(indexBase >= 0) {
		    Select selectBaseE = new Select(sensoresE);
		    selectBaseE.selectByIndex(indexBase);
        }
        
        WebElement pontoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("ponto")));
        if(indexPonto >= 0) {
		    Select selectBaseE = new Select(pontoE);
		    selectBaseE.selectByIndex(indexPonto);
        }
               
        WebElement placasE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placas_chosen\"]/ul/li/input 	")));
        placasE.clear();
        for(String o:placas) {
        	try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	placasE.sendKeys(o,Keys.ENTER);
        }
        
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
        	Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		
	public static StringBuffer gerarRelatorioDataInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialCharInvalidos";
		
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioRaioCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioRaioCharInvalidos";
		
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        raio = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM RAIO COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioRaioVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioRaioVazia";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        raio = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM RAIO VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioRaioMaiorQueLimite(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioRaioMaiorQueLimite";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        raio = "501";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM RAIO MAIOR QUE LIMITE DE 500";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioRaioMenorQueLimite(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioRaioMenorQueLimite";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        raio = "49";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM RAIO MENOR QUE LIMITE DE 50";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialVazia";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        
        dataInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioCampoPlacasVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioCampoPlacasVazio";
				
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
        	String motivo = "TABELA N�O � GERADA COM CAMPO PLACAS VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPerfeito";
		
		placas = new ArrayList<String>();
		placas.add("ACK0588");
        placas.add("AEA3183");
        		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
            String motivo = "TABELA N�O � GERADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        
        return errorBuffer;
        
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		PontoInteresseAvancado.dadosArquivo = dadosArquivo;
	}
	
}
