package tests.gestorfrotas.menuresponsivo.relatorios.pontointeresse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorfrotas.menuresponsivo.relatorios.Relatorios;
import tests.util.Comandos;

public class PontoInteresseBasico extends Relatorios{
	private static final String nomeTela = "Relat�rio Ponto de Interesse B�sico";
	
	private static String dataInicial = "01/01/2020";
	private static String raio = "100";
	private static String horaInicial = "00:00:01";
	private static String horaFinal = "23:59:59";

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public PontoInteresseBasico(WebDriver driver) {
		wait = new WebDriverWait(driver, 10);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
		
		errorBuffer.append(gerarRelatorioHoraInicialCharInvalidos(driver)+"\n"); //teste com hora inicial com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioHoraFinalCharInvalidos(driver)+"\n"); //teste com hora final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioHoraInicialVazia(driver)+"\n"); //teste com hora inicial vazia

		errorBuffer.append(gerarRelatorioHoraFinalVazia(driver)+"\n"); //teste com hora final vazia

		errorBuffer.append(gerarRelatorioHoraFinalMaiorQueLimite(driver)+"\n"); //teste com hora final maior do que deveria
		 
		errorBuffer.append(gerarRelatorioHoraInicialMaiorQueLimite(driver)+"\n"); //teste com hora inicial maior do que um dia
		
		errorBuffer.append(gerarRelatorioRaioCharInvalidos(driver)+"\n"); //teste com raio com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioRaioVazia(driver)+"\n"); //teste com raio vazio

		errorBuffer.append(gerarRelatorioDataInicialCharInvalidos(driver)+"\n"); //teste com data inicial com caracteres inv�lidos
				
		errorBuffer.append(gerarRelatorioDataInicialVazia(driver)+"\n"); //teste com data incial vazia
		        
		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
	    dataInicial = "01/01/2020";
	    raio = "100";
	    horaInicial = "00:00:01";
		horaFinal = "23:59:59";
	}
	
	public static void gerarRelatorio(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
        
        //abre menu de relat�rios ponto de interesse
        String xpath = abrirRelatorioPontoInteresse(driver)+"ul/li[2]/";
        
        //Abre o relat�rio escolhido
        WebElement eventoDetalhado = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", eventoDetalhado);
        js.executeScript("arguments[0].click();", eventoDetalhado);
        
        WebElement dataInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("data")));
        dataInicialE.clear();
        dataInicialE.sendKeys(dataInicial);
                
        WebElement horaInicialE = wait.until(ExpectedConditions.elementToBeClickable(By.name("hora_ini")));
        horaInicialE.clear();
        horaInicialE.sendKeys(horaInicial);
        
        WebElement horaFinalE = wait.until(ExpectedConditions.elementToBeClickable(By.name("hora_fim")));
        horaFinalE.clear();
        horaFinalE.sendKeys(horaFinal);
        
        WebElement raioE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("raio")));
        raioE.clear();
        raioE.sendKeys(raio);
        
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
        	Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	public static StringBuffer gerarRelatorioHoraInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialCharInvalidos";
		
		
        
        
        horaInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalCharInvalidos";
		
		
        
        
        horaFinal = "abc!";
		
        gerarRelatorio(driver);
                       
        try{
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialVazia";
        
        horaInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	

	public static StringBuffer gerarRelatorioHoraFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalVazia";
		
		
        
        
        horaFinal = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalMaiorQueLimite";
		
		
        
        
        horaFinal = "24:00:01";
		
        gerarRelatorio(driver);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialMaiorQueLimite";
		
		
        
        
        horaInicial = "50:00:01";
		
        gerarRelatorio(driver);
                        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
		
	public static StringBuffer gerarRelatorioDataInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialCharInvalidos";
		
        dataInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioRaioCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioRaioCharInvalidos";
		
        raio = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM RAIO COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioRaioVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioRaioVazia";
		
        raio = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM RAIO VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
		
	public static StringBuffer gerarRelatorioDataInicialVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialVazia";
		
        dataInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPerfeito";
			
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
            String motivo = "TABELA N�O � GERADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        
        return errorBuffer;
        
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		PontoInteresseBasico.dadosArquivo = dadosArquivo;
	}
	
}
