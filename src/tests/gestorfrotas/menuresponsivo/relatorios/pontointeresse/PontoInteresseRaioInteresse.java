package tests.gestorfrotas.menuresponsivo.relatorios.pontointeresse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorfrotas.menuresponsivo.relatorios.Relatorios;
import tests.util.Comandos;

public class PontoInteresseRaioInteresse extends Relatorios{
	private static final String nomeTela = "Relat�rio Raio de Interesse";
	
	private static ArrayList<String> pontos = new ArrayList<String>();
	private static String raio = "100";

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public PontoInteresseRaioInteresse(WebDriver driver) {
		wait = new WebDriverWait(driver, 6);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
			
		errorBuffer.append(gerarRelatorioRaioCharInvalidos(driver)+"\n"); //teste com raio com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioRaioVazia(driver)+"\n"); //teste com raio vazio

		errorBuffer.append(gerarRelatorioRaioMaiorQueLimite(driver)+"\n"); //teste com raio maior que limite

		errorBuffer.append(gerarRelatorioCampoPontosVazio(driver)+"\n"); //teste com campo pontos vazio
		        
		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
		pontos = new ArrayList<String>();
	    raio = "100";
	}
	
	public static void gerarRelatorio(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
        
        //abre menu de relat�rios ponto de interesse
        String xpath = abrirRelatorioPontoInteresse(driver)+"ul/li[3]/";
        
        //Abre o relat�rio escolhido
        WebElement eventoDetalhado = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", eventoDetalhado);
        js.executeScript("arguments[0].click();", eventoDetalhado);
        
        WebElement raioE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("raio")));
        raioE.clear();
        raioE.sendKeys(raio);
                       
        WebElement pontosE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"myForm\"]/div[1]/div/div/ul/li/input")));
        pontosE.clear();
        for(String o:pontos) {
        	try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	pontosE.sendKeys(o,Keys.ENTER);
        }
        
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
        	Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		
	public static StringBuffer gerarRelatorioRaioCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioRaioCharInvalidos";
		
		pontos.add("Teste");
        
        
        raio = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM RAIO COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioRaioVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioRaioVazia";
		
		pontos = new ArrayList<String>();
		pontos.add("Teste");
        
        
        raio = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM RAIO VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioRaioMaiorQueLimite(WebDriver driver) throws NoAlertPresentException{
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioRaioMaiorQueLimite";
		
		pontos = new ArrayList<String>();
		pontos.add("Teste");
        
        
        raio = "501";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            String motivo = "TABELA � GERADA MESMO COM RAIO MAIOR QUE LIMITE DE 500";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioCampoPontosVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioCampoPontosVazio";
				
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
        	String motivo = "TABELA N�O � GERADA COM CAMPO PONTO DE INTERESSES VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPerfeito";
		
		pontos = new ArrayList<String>();
		pontos.add("Teste");
        
        		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/section[2]/div[2]/div[4]"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException e) {
            String motivo = "TABELA N�O � GERADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        
        return errorBuffer;
        
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		PontoInteresseRaioInteresse.dadosArquivo = dadosArquivo;
	}
	
}
