package tests.gestorfrotas.menuresponsivo.relatorios.rotas;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorfrotas.menuresponsivo.relatorios.Relatorios;
import tests.util.Comandos;
import tests.util.Variaveis;

public class MapaRota extends Relatorios{
	private static final String nomeTela = "Relat�rio Mapa Rota";
	
	private static String placa;
	private static String motorista;
	private static String dataInicial;
	private static String dataFinal;
	private static String horaInicial;
	private static String horaFinal;
	private static String intervalo;
	private static int indexRotas;
	private static boolean paradas;
	private static boolean pontosInteresse;
	
	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public MapaRota(WebDriver driver) {
		wait = new WebDriverWait(driver, 20);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+nomeTela+"\n");
		
		clear();
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
		
		errorBuffer.append(gerarRelatorioHoraInicialVazia(driver)+"\n"); //teste com hora inicial vazia
		errorBuffer.append(gerarRelatorioHoraInicialCharInvalidos(driver)+"\n"); //teste com hora inicial com caracteres inv�lidos
		errorBuffer.append(gerarRelatorioHoraInicialMaiorQueLimite(driver)+"\n"); //teste com hora inicial maior do que um dia

		errorBuffer.append(gerarRelatorioHoraFinalCharInvalidos(driver)+"\n"); //teste com hora final com caracteres inv�lidos
		errorBuffer.append(gerarRelatorioHoraFinalVazia(driver)+"\n"); //teste com hora final vazia
		errorBuffer.append(gerarRelatorioHoraFinalMaiorQueLimite(driver)+"\n"); //teste com hora final maior do que deveria

		errorBuffer.append(gerarRelatorioIntervaloVazia(driver)+"\n"); //teste com intervalo vazia
		errorBuffer.append(gerarRelatorioIntervaloCharInvalidos(driver)+"\n"); //teste com intervalo com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataInicialVazia(driver)+"\n"); //teste com data incial vazia
		errorBuffer.append(gerarRelatorioDataInicialCharInvalidos(driver)+"\n"); //teste com data inicial com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioDataFinalVazia(driver)+"\n"); //teste com data final vazia
		errorBuffer.append(gerarRelatorioDataFinalCharInvalidos(driver)+"\n"); //teste com data final com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioPeriodoMaiorQueLimite(driver)+"\n"); //teste com diferen�a entre datas maioir que o limite
		errorBuffer.append(gerarRelatorioDataFinalMaiorQueAtual(driver)+"\n"); //teste com data final maior do que data atual
		errorBuffer.append(gerarRelatorioDataInicialMaiorQueFinal(driver)+"\n"); //teste com data final maior do que data inicial

		errorBuffer.append(gerarRelatorioCampoPlacaVazio(driver)+"\n"); //teste com campo placa vazio
	
		errorBuffer.append(gerarRelatorioCampoMotoristaVazio(driver)+"\n"); //teste com campo motorista vazio

		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
		placa = Variaveis.getPlaca1();
	    dataFinal = Variaveis.getDatafinal();
	    dataInicial = Variaveis.getDatainicial();
	    horaInicial = Variaveis.getHorainicial();
	    horaFinal = Variaveis.getHorafinal();
	    motorista = "Jo o da Silva - 0304022";
	    intervalo = "10";
		indexRotas = 1;
		paradas = true;
		pontosInteresse = true;
	}
	
	public static void gerarRelatorio(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
        
        //abre menu de relat�rios mapaRota
        String xpath = abrirRelatorioRotas(driver) + "ul/li[1]/";
        
        //Abre o relat�rio escolhido
        WebElement eventoDetalhado = driver.findElement(By.xpath(xpath+"a"));
        js.executeScript("arguments[0].scrollIntoView(true);", eventoDetalhado);
        js.executeScript("arguments[0].click();", eventoDetalhado);
        
        WebElement dataInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("inicio")));
        dataInicialE.clear();
        dataInicialE.sendKeys(dataInicial);
        
        WebElement dataFinalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("fim")));
        dataFinalE.clear();
        dataFinalE.sendKeys(dataFinal);
        
        WebElement horaInicialE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora_ini")));
        horaInicialE.clear();
        horaInicialE.sendKeys(horaInicial);
        
        WebElement horaFinalE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("hora_fim")));
        horaFinalE.clear();
        horaFinalE.sendKeys(horaFinal);
               
        if(!placa.equals("")) {
	        WebElement placaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"placas_chosen\"]/ul/li/input")));
	        placaE.clear();
	        placaE.sendKeys(placa,Keys.ENTER);
        }
        
        if(!motorista.equals("")) {
		    WebElement motoristaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"motoristas_chosen\"]/ul/li/input")));
		    motoristaE.clear();
		    motoristaE.sendKeys(motorista,Keys.ENTER);
        }
        
        WebElement intervaloE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("intervalo")));
        intervaloE.clear();
        intervaloE.sendKeys(intervalo);
        
        Comandos.escolherRadioButtonNome(driver, "rota", indexRotas);
        
        Comandos.clicarCheckboxNome(driver, "paradas", paradas);
        Comandos.clicarCheckboxNome(driver, "interesses", pontosInteresse);
        
        try {
        	Thread.sleep(500);
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("gerar_relatorio")));
        botaoSubmit.click();
        
        clear();
        try {
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
        }
        catch(NoAlertPresentException e) {
        	e.printStackTrace();
        } catch (TimeoutException | NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		
	public static StringBuffer gerarRelatorioHoraInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialCharInvalidos";
		
		
        
        
        horaInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioIntervaloCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioIntervaloCharInvalidos";
		
		
        
        
        intervalo = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM INTERVALO COM CHAR INVALIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalCharInvalidos";
		
		
        
        
        horaFinal = "abc!";
		
        gerarRelatorio(driver);
                       
        try{
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialVazia";
		
		
		
        
        
        horaInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}

	public static StringBuffer gerarRelatorioIntervaloVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioIntervaloVazia";
		
		
		
        
        
        intervalo = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM CAMPO INTERVALO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalVazia";
		
		
        
        
        horaFinal = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraFinalMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraFinalMaiorQueLimite";
		
		
        
        
        horaFinal = "24:00:01";
		
        gerarRelatorio(driver);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA FINAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioHoraInicialMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioHoraInicialMaiorQueLimite";
		
		
        
        
        horaInicial = "50:00:01";
		
        gerarRelatorio(driver);
                        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM HORA INICIAL MAIOR QUE LIMITE DE 24";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialCharInvalidos";
		
		
        
        
        dataInicial = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalCharInvalidos(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalCharInvalidos";
		
		
        
        
        dataFinal = "abc!";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL COM CHAR INV�LIDOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataFinalVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalVazia";
		
		
		
        
        
        dataFinal = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioDataInicialVazia(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialVazia";
		
		
		
        
        
        dataInicial = "";
		
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL VAZIA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
		
	public static StringBuffer gerarRelatorioCampoPlacaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioCampoPlacaVazio";
		placa = "";
	
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM CAMPO PLACA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioCampoMotoristaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioCampoMotoristaVazio";
		motorista = "";
				
        gerarRelatorio(driver);
                       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM CAMPO MOTORISTA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        return errorBuffer;
	}
	
	public static StringBuffer gerarRelatorioPeriodoMaiorQueLimite(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPeriodoMaiorQueLimite";
		
		
		
        
        
        dataInicial = Variaveis.getDatainicial();
        dataFinal = "02/02/2020";
		
        gerarRelatorio(driver);
       
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM PER�ODO ENTRE DATAS MAIOR QUE LIMITE";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
	
	
	public static StringBuffer gerarRelatorioDataInicialMaiorQueFinal(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataInicialMaiorQueFinal";
		
		
		
        
        
        dataInicial = Variaveis.getDatafinal();
        dataFinal = Variaveis.getDatainicial();
		
        gerarRelatorio(driver);
                
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA INICIAL MAIOR QUE FINAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
	}
		
	public static StringBuffer gerarRelatorioDataFinalMaiorQueAtual(WebDriver driver) {
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
		
		String data = formatoData.format(calendario.getTime());
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioDataFinalMaiorQueAtual";
		
		
		
        
        
        dataFinal = data;
		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            String motivo = "TABELA � GERADA MESMO COM DATA FINAL MAIOR QUE ATUAL";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }catch(TimeoutException | NoSuchElementException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }
        
        return errorBuffer;
        
	}
	
	public static StringBuffer gerarRelatorioPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "gerarRelatorioPerfeito";
		
		
		
        
        		
        gerarRelatorio(driver);
        
        try{ 
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"container_home\"]/table"))));                
            errorBuffer.append(Comandos.printTesteSucesso(teste));
        }catch(TimeoutException | NoSuchElementException e) {
            String motivo = "TABELA N�O � GERADA MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
        }
        
        return errorBuffer;
        
	}

	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		MapaRota.dadosArquivo = dadosArquivo;
	}
	
}
