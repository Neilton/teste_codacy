package tests.gestorfrotas.menuresponsivo.relatorios.saver;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorfrotas.menuresponsivo.relatorios.Relatorio;
import tests.util.Comandos;
import tests.util.Variaveis;

public class FrotasEventosOcorridos extends Relatorio{
	private static final String TITULO = "Relat�rio Eventos Ocorridos";
	private static final String FALHAREQUISICAO = "Falha na requisi��o!";
	private static final String TABLE_RELATORIO = "tableRelatorio";
	private static final Logger LOGGER = Logger.getLogger( FrotasEventosOcorridos.class.getName() );	
	
	@SuppressWarnings("deprecation")
	public FrotasEventosOcorridos(WebDriver driver) {
		setWait(new WebDriverWait(driver, Variaveis.getTime()));
		//abre menu de relat�rios saver
        abrirRelatorioSaverEventosOcorridos(driver);
		clear();
			
		StringBuilder errorBuffer = new StringBuilder();
		
		errorBuffer.append("\n"+TITULO+"\n\n");
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
		
		errorBuffer.append(gerarRelatorioPerfeitoTodosOsVeiculos(driver)+"\n"); //teste perfeito utilizando checkbox Todos os Veiculos
			
		errorBuffer.append(gerarRelatorioPerfeitoGrupo(driver)+"\n"); //teste perfeito utilizando grupos
		
		errorBuffer.append(gerarRelatorioDataInicialCharInvalidos(driver)+"\n"); //teste com data inicial com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataFinalCharInvalidos(driver)+"\n"); //teste com data final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataInicialVazia(driver)+"\n"); //teste com data incial vazia

		errorBuffer.append(gerarRelatorioDataFinalVazia(driver)+"\n"); //teste com data final vazia

		errorBuffer.append(gerarRelatorioCampoPlacasVazio(driver)+"\n"); //teste com campo placas vazio
		        
		errorBuffer.append(gerarRelatorioPeriodoMaiorQueLimite(driver)+"\n"); //teste com diferen�a entre datas maioir que o limite
       
		errorBuffer.append(gerarRelatorioDataFinalMaiorQueAtual(driver)+"\n"); //teste com data final maior do que data atual
		
		errorBuffer.append(gerarRelatorioDataInicialMaiorQueFinal(driver)+"\n"); //teste com data final maior do que data inicial
				
		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
		setPlacas(new ArrayList<>());
		getPlacas().add(Variaveis.getPlaca1());
		getPlacas().add(Variaveis.getPlaca2());
		setGrupos(new ArrayList<>());
	    setDataFinal(Variaveis.getDatafinal());
	    setDataInicial(Variaveis.getDatainicial());
	    setTodosOsVeiculos(false);
	    setAlerta(null);
	}
	
	public static void inicializaCampos() {
        setDataInicialE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.id("inputDataInicial"))));
        setDataFinalE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.id("inputDataFinal"))));
        setPlacasE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"selectPlaca_chosen\"]/ul/li/input"))));
        setGruposE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"selectGrupo_chosen\"]/div/div/input"))));
        setTodosOsVeiculosE(getWait().until(ExpectedConditions.elementToBeClickable(By.id("checkboxTodasAsPlacas"))));
        setBotaoSubmit(getWait().until(ExpectedConditions.elementToBeClickable(By.id("buttonGerarRelatorio"))));
	}
	
	// variaveis que ser�o usadas no arquivo resultando dos testes
	public static void setVariaveis() {
		setVariaveis(new StringBuilder());
		for(String m:getPlacas()) {
			getVariaveis().append("- Placa: "+m+"\n");
		}
		for(String g:getGrupos()) {
			getVariaveis().append("- Grupo: "+g+"\n");
		}
		if(getDataInicial() != null)
			getVariaveis().append("- Data Inicial: "+getDataInicial()+"\n");
		getVariaveis().append("- Data Final: "+getDataFinal()+"\n");
		getVariaveis().append("- Todos os ve�culos: "+isTodosOsVeiculosCheck()+"\n");
	}
		
	@SuppressWarnings({ "deprecation" })
	public static void gerarRelatorio(WebDriver driver) throws NoAlertPresentException,TimeoutException{
        setWait(new WebDriverWait(driver, Comandos.getTime())); //Espera para elementos de outras p�ginas
                
        // atualiza a p�gina
        driver.navigate().refresh();
        
        // contador de casos te teste
        setCount(getCount() + 1);
                
        //configura todos os campos utiliz�veis
        inicializaCampos();
    	
        getDataInicialE().clear();
        getDataInicialE().sendKeys(getDataInicial());
                       
        getDataFinalE().clear();
        getDataFinalE().sendKeys(getDataFinal());
        
        Comandos.clicarCheckboxId(driver, getTodosOsVeiculosE(), isTodosOsVeiculos());

        for(String o:getPlacas()) {
        	try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
        	getPlacasE().sendKeys(o,Keys.ENTER);
        }
        
        if(isTodosOsVeiculos()) {
        	try {
            	WebDriverWait wait = new WebDriverWait(driver, 10);
            	wait.until(ExpectedConditions.alertIsPresent());
    	        Alert alert = driver.switchTo().alert();
    	        alert.accept();
            	
            }
            catch(NoAlertPresentException | TimeoutException e) {
            	e.printStackTrace();
            }
        }
        	
        // procura elemento grupo, retorna erro caso o mesmo esteja desabilitado
        try {
            getGruposE().clear();
            WebElement gruposEClick = getWait().until(ExpectedConditions.presenceOfElementLocated(By.id("selectGrupo_chosen")));
            gruposEClick.click();
		} catch (InvalidElementStateException e) {
			LOGGER.log(Level.WARNING, "O elemento grupo foi desabilitado");
		}
        
        for(String o:getGrupos()) {
        	try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
        	getGruposE().sendKeys(o,Keys.ENTER);
        }
        
        getBotaoSubmit().click();
        
        setVariaveis();   
        clear();
        
        try {
        	WebDriverWait wait = new WebDriverWait(driver, 10);
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        setAlerta(alert.getText());
	        getVariaveis().append("ALERTA GERADO: "+getAlerta()+"\n");
	        alert.accept();
        	
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	e.printStackTrace();
        }
        

	}
	
	public static StringBuilder gerarRelatorioDataInicialCharInvalidos(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Data Inicial com caracteres inv�lidos";
		        
        setDataInicial("abc!");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioDataFinalCharInvalidos(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Data Final com caracteres inv�lidos";
		
        setDataFinal("abc!");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioDataFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Data Final vazio";
		
        setDataFinal("");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioDataInicialVazia(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Data Inicial vazio";
		
        setDataInicial("");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioCampoPlacasVazio(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Placas e Grupo vazios";
		
		setPlacas(new ArrayList<>());
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioPeriodoMaiorQueLimite(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com per�odo maior que limite";
		
		Calendar calendario = Calendar.getInstance();
		
		try {
			calendario.setTime(Variaveis.getInstance().getFormatodata().parse(Variaveis.getDatainicial()));
			calendario.add(Calendar.DATE, 31);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
        setDataFinal(Variaveis.getInstance().getFormatodata().format(calendario.getTime()));
		
        gerarRelatorio(driver);
       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        
        return errorBuffer;
	}
	
	
	public static StringBuilder gerarRelatorioDataInicialMaiorQueFinal(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com data inicial maior que data final";
		
		Calendar calendario = Calendar.getInstance();
		
		try {
			calendario.setTime(Variaveis.getInstance().getFormatodata().parse(Variaveis.getDatafinal()));
			calendario.add(Calendar.DATE, 1);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
        setDataInicial(Variaveis.getInstance().getFormatodata().format(calendario.getTime()));
		
        gerarRelatorio(driver);
                
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        
        return errorBuffer;
	}
		
	public static StringBuilder gerarRelatorioDataFinalMaiorQueAtual(WebDriver driver) {
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
				
		String data = Variaveis.getInstance().getFormatodata().format(calendario.getTime());
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com data final maior que data atual";
		
		setDataInicial(data);
        setDataFinal(data);
		
        gerarRelatorio(driver);
        
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        
        return errorBuffer;
        
	}
	
	public static StringBuilder gerarRelatorioPerfeito(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com dados perfeitos usando placas";
		
        gerarRelatorio(driver);
        
        try{ 
            getWait().until(ExpectedConditions.visibilityOf(driver.findElement(By.id(TABLE_RELATORIO))));                
            errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsonegativo()));
        }
        
        return errorBuffer;
        
	}
	
	public static StringBuilder gerarRelatorioPerfeitoTodosOsVeiculos(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com dados perfeitos usando todos os ve�culos";
		
		setPlacas(new ArrayList<>());
		setGrupos(new ArrayList<>());
		
		setTodosOsVeiculos(true);
		
        gerarRelatorio(driver);
        
        try{ 
            getWait().until(ExpectedConditions.visibilityOf(driver.findElement(By.id(TABLE_RELATORIO))));                
            errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsonegativo()));
        }
        
        return errorBuffer;
        
	}
	
	public static StringBuilder gerarRelatorioPerfeitoGrupo(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com dados perfeitos usando grupo";
				
		setPlacas(new ArrayList<>());
		getGrupos().add(Variaveis.getGrupoFrotas());
	
        gerarRelatorio(driver);
        
        try{ 
            getWait().until(ExpectedConditions.visibilityOf(driver.findElement(By.id(TABLE_RELATORIO))));                
            errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsonegativo()));
        }
        
        return errorBuffer;
        
	}
	
}
