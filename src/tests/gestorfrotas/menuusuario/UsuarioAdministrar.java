package tests.gestorfrotas.menuusuario;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class UsuarioAdministrar {
	private static final String senhaTela = "Administrar Usu�rios";
	private static int countEmail = 0;
	private static int count = 0;
	
	private static String nome = "teste do teste testado";
	private static String email = "aloha@ohanaB"+countEmail+".com";
	private static String senha = "123456";
	private static ArrayList<String> grupo = new ArrayList<String>();
	private static String base = "teste do teste testado testando o teste";
	private static ArrayList<String> arrayIp = new ArrayList<String>();
	private static ArrayList<String> arrayDescricaoIp = new ArrayList<String>();
	private static boolean filtragemIp = true;
	private static int indexFuncao = 1;
	
	private static StringBuffer variaveis;

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	// variaveis que ser�o usadas no arquivo resultando dos testes
		public static void setVariaveis() {
			setVariaveis(new StringBuffer());
			for(String m:getMonitorados()) {
				getVariaveis().append("- Monitorado: "+m+"\n");
			}
			for(String g:getGrupos()) {
				getVariaveis().append("- Grupo: "+g+"\n");
			}
			if(getDataInicial() != null)
			getVariaveis().append("- Data Inicial: "+getDataInicial()+"\n");
			getVariaveis().append("- Data Final: "+getDataFinal()+"\n");
			getVariaveis().append("- Hora Inicial: "+getHoraInicial()+"\n");
			getVariaveis().append("- Hora Final: "+getHoraFinal()+"\n");
		}
	
	public static void clear() {
		nome = "teste do teste testado";
		email = "aloha@ohanaB"+countEmail+".com";
		senha = "123456";
		indexFuncao = 1;
		grupo = new ArrayList<String>();
		grupo.add("MASTER");
		base = "teste do teste testado testando o teste";
		filtragemIp = true;
		
		arrayIp = new ArrayList<String>();
		arrayDescricaoIp = new ArrayList<String>();
		arrayIp.add("111.111.111.111");
		arrayDescricaoIp.add("teste do teste testado testando o teste");
	}

	public UsuarioAdministrar(WebDriver driver) {
		wait = new WebDriverWait(driver, 60);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+senhaTela+"\n");
		
		clear();
		
		entrarAdministrar(driver);
				
		errorBuffer.append(administrarPerfeito(driver)+"\n"); //teste perfeito sem nenhum checkbox		
		errorBuffer.append(administrarCampoNomeVazio(driver)+"\n"); //teste com campo nome vazio
		errorBuffer.append(administrarCampoEmailVazio(driver)+"\n"); //teste com campo email vazio
		errorBuffer.append(administrarCampoFuncaoVazio(driver)+"\n"); //teste com campo prioridade vazio
		errorBuffer.append(administrarCampoSenhaVazio(driver)+"\n"); //teste com campo senha vazio
		errorBuffer.append(administrarCampoGrupoVazio(driver)+"\n"); //teste com campo descri��o vazio
		errorBuffer.append(administrarCampoBaseVazio(driver)+"\n"); //teste com campo base vazio
		errorBuffer.append(administrarCampoIpVazio(driver)+"\n"); //teste com campo id vazio
		errorBuffer.append(administrarCampoIpDescricaoVazio(driver)+"\n"); //teste com campo id e descricao de id vazio
		
		setDadosArquivo(errorBuffer);

	}
	
	public static boolean entrarAdministrar(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
		try {
	        //Abre menu de usuario
	        String xpath = Comandos.menuUsuario(driver)+"ul/li[4]/";
	        
	        //Abre o ferramenta de administrar
	        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
	        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
	        js.executeScript("arguments[0].click();", cadastro);  
	        
	        //Clica na aba dejejada
	        WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/ul/li[1]/a")));
	        aba.click();
	       
			
		}
		catch(Exception e) {
			return false;
		}
        return true;   
	}
	
	public static boolean administrar(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

		//Clica em administrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"usuarios\"]/div/button")));
        novo.click();
		
        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nome_usuario")));
        nomeE.clear();
        nomeE.sendKeys(nome);
        
        WebElement emailE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("usuario")));
        emailE.clear();
        emailE.sendKeys(email);    
        
        WebElement funcaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tipo_usuario")));
        if(indexFuncao >= 0) {
		    Select selectBaseE = new Select(funcaoE);
		    selectBaseE.selectByIndex(indexFuncao);
        }
        
        Comandos.clicarCheckboxNome(driver, "habilitarip", filtragemIp);
        
        Comandos.elementoTipoChosenSearch(driver, grupo, "//*[@id=\"grupo_chosen\"]/ul/li/input");
        
        WebElement senhaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("senha")));
        senhaE.clear();
        senhaE.sendKeys(senha);
        
        WebElement baseE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("base")));
        baseE.clear();
        baseE.sendKeys(base);
        
        for(String ip: arrayIp) {
        	int index = arrayIp.indexOf(ip);
        	List<WebElement> ipE = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("ips[]")));
        	ipE.get(index).clear();
        	ipE.get(index).sendKeys(ip);
        	
        	List<WebElement> descricaoIpE = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("descricao[]")));
        	descricaoIpE.get(index).clear();
        	descricaoIpE.get(index).sendKeys(arrayDescricaoIp.get(index));
        	
        	WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("add")));
            botaoSubmit.click();
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("saveUser")));
        botaoSubmit.click();
        
        while(true) {
		    try {
		    	wait.until(ExpectedConditions.alertIsPresent());
		        Alert alert = driver.switchTo().alert();
		        alert.accept();
		    }
		    catch(NoAlertPresentException | TimeoutException e) {
		    	
		    	e.printStackTrace();
		    	break;
		    }
        }
        
        WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.id("closeModal")));
        fechar.click();
        count++;
        
        return administrarConfirmacao(driver);
	}
	
	public static boolean administrarConfirmacao(WebDriver driver) {
		boolean tudoCerto = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
		
		//Clica em administrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"usuarios\"]/div/button")));
        novo.click();
		
        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nome_usuario")));
        nomeE.clear();
        nomeE.sendKeys(nome);
        
        WebElement emailE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("usuario")));
        emailE.clear();
        emailE.sendKeys(email);    
        
        WebElement funcaoE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tipo_usuario")));
        if(indexFuncao >= 0) {
		    Select selectBaseE = new Select(funcaoE);
		    selectBaseE.selectByIndex(indexFuncao);
        }
        
        Comandos.clicarCheckboxNome(driver, "habilitarip", filtragemIp);
        
        Comandos.elementoTipoChosenSearch(driver, grupo, "//*[@id=\"grupo_chosen\"]/ul/li/input");
        
        WebElement senhaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("senha")));
        senhaE.clear();
        senhaE.sendKeys(senha);
        
        WebElement baseE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("base")));
        baseE.clear();
        baseE.sendKeys(base);
        
        for(String ip: arrayIp) {
        	int index = arrayIp.indexOf(ip);
        	List<WebElement> ipE = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("ips[]")));
        	ipE.get(index).clear();
        	ipE.get(index).sendKeys(ip);
        	
        	List<WebElement> descricaoIpE = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("descricao[]")));
        	descricaoIpE.get(index).clear();
        	descricaoIpE.get(index).sendKeys(arrayDescricaoIp.get(index));
        	
        	WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("add")));
            botaoSubmit.click();
        }
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("saveUser")));
        botaoSubmit.click();
                
        countEmail++;
        clear();
        while(true) {
		    try {
		    	wait.until(ExpectedConditions.alertIsPresent());
		        Alert alert = driver.switchTo().alert();
		        if(alert.getText().contains("Perfil atualizado com sucesso!") 
		        		|| alert.getText().contains("O email que est� tentando cadastrar j� est� em uso. Tente utilizar outro."))
		        	tudoCerto = true;
		        alert.accept();
		    }
		    catch(NoAlertPresentException | TimeoutException e) {
		    	
		    	e.printStackTrace();
		    	break;
		    }
        }
        
        WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.id("closeModal")));
        fechar.click();
        return tudoCerto;
	}
	
	public static StringBuffer administrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarPerfeito";
		
		
				
		try {
			Assert.assertTrue(administrar(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(count,teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));

	    	
		}
		
        return errorBuffer;
        
	}
	
	public static StringBuffer administrarCampoBaseVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarCampoBaseVazio";
		
		base = "";
				
		try {
			Assert.assertTrue(administrar(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO BASE VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	
	        
		}
		
        return errorBuffer;
        
	}
	
	public static StringBuffer administrarCampoFuncaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarCampoFuncaoVazio";
				
		indexFuncao = 0;
				
		try {
			Assert.assertTrue(administrar(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO FUN��O VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	
	        
		}
        return errorBuffer;
	}
	
	public static StringBuffer administrarCampoNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarCampoNomeVazio";
				
		nome = "";
		
		
		
		try {
			Assert.assertTrue(administrar(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO NOME VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	
	        
		}
        return errorBuffer;
	}
	
	public static StringBuffer administrarCampoEmailVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarCampoEmailVazio";
				
		email = "";
		
		
		
		try {
			Assert.assertTrue(administrar(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO EMAIL VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	
	        
		}
        return errorBuffer;
	}
	
	public static StringBuffer administrarCampoSenhaVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarCampoSenhaVazio";
				
		senha = "";
		
		
		
		try {
			Assert.assertTrue(administrar(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO SENHA VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	
	        
		}
        return errorBuffer;
	}
	
	public static StringBuffer administrarCampoGrupoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarCampoGrupoVazio";
				
		grupo = new ArrayList<String>();
		
		
		
		try {
			Assert.assertTrue(administrar(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO GRUPO VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	
	        
		}
        return errorBuffer;
	}
	
	public static StringBuffer administrarCampoIpVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarCampoIpVazio";
				
		arrayIp = new ArrayList<String>();
		
		
		
		try {
			Assert.assertTrue(administrar(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO IP VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	
	        
		}
        return errorBuffer;
	}
	
	public static StringBuffer administrarCampoIpDescricaoVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarCampoIpDescricaoVazio";
				
		arrayIp = new ArrayList<String>();
		arrayDescricaoIp = new ArrayList<String>();
		
		
		try {
			Assert.assertTrue(administrar(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
            String motivo = "CADASTRO N�O � EFETUADO MESMO COM CAMPOS IP E DESCRI��O VAZIOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            
	    	
	        
		}
        return errorBuffer;
	}
	
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		UsuarioAdministrar.dadosArquivo = dadosArquivo;
	}

	public static StringBuffer getVariaveis() {
		return variaveis;
	}

	public static void setVariaveis(StringBuffer variaveis) {
		UsuarioAdministrar.variaveis = variaveis;
	}
}
