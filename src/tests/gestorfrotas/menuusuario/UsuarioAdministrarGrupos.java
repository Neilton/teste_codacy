package tests.gestorfrotas.menuusuario;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class UsuarioAdministrarGrupos {
	private static final String senhaTela = "Administrar Grupos";
	private static int count = 0;
	
	private static String nome;

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		nome = "teste testado"+count;
	}

	public UsuarioAdministrarGrupos(WebDriver driver) {
		wait = new WebDriverWait(driver, 60);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+senhaTela+"\n");
		
		clear();
		
		entrarAdministrar(driver);
				
		errorBuffer.append(administrarPerfeito(driver)+"\n"); //teste perfeito sem nenhum checkbox		
		errorBuffer.append(administrarCampoNomeVazio(driver)+"\n"); //teste com campo nome vazio
		
		System.out.println(errorBuffer);
		setDadosArquivo(errorBuffer);

	}
	
	public static boolean entrarAdministrar(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
		try {
	        //Abre menu de usuario
	        String xpath = Comandos.menuUsuario(driver)+"ul/li[4]/";
	        
	        //Abre o ferramenta de administrar
	        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
	        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
	        js.executeScript("arguments[0].click();", cadastro);  
	        
	        //Clica na aba dejejada
	        WebElement aba = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/ul/li[2]/a")));
	        aba.click();
	       
			
		}
		catch(Exception e) {
			return false;
		}
        return true;   
	}
	
	public static boolean administrar(WebDriver driver) {
		boolean tudoCerto = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

		//Clica em administrar novo elemento
        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"grupos\"]/div/button")));
        novo.click();
		
        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nome")));
        js.executeScript("arguments[0].value=arguments[1]",nomeE,nome);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("saveGroup")));
        botaoSubmit.click();
        
        count++;
        clear();
        while(true) {
		    try {
		    	wait.until(ExpectedConditions.alertIsPresent());
		        Alert alert = driver.switchTo().alert();
		        System.out.println(alert.getText());
		        if(alert.getText().equals("Grupo cadastrado com sucesso"))
		        	tudoCerto = true;
		        alert.accept();
		    }
		    catch(NoAlertPresentException | TimeoutException e) {
		    	
		    	e.printStackTrace();
		    	break;
		    }
        }
        
        WebElement fechar = wait.until(ExpectedConditions.elementToBeClickable(By.id("closeModal")));
        fechar.click();
        return tudoCerto;
	}
	
	public static StringBuffer administrarPerfeito(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarPerfeito";
			
		try {
			Assert.assertTrue(administrar(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "CADASTRO N�O EFETUADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));

		}
		
        return errorBuffer;
        
	}
	
	public static StringBuffer administrarCampoNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "administrarCampoNomeVazio";
				
		nome = "";
		
		
		
		try {
			Assert.assertTrue(administrar(driver));
			
			String motivo = "CADASTRO � EFETUADO MESMO COM CAMPO NOME VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
		}
		catch(AssertionError | TimeoutException e) {
            errorBuffer.append(Comandos.printTesteSucesso(teste));

	    	
	        
		}
        return errorBuffer;
	}
	
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		UsuarioAdministrarGrupos.dadosArquivo = dadosArquivo;
	}
}
