package tests.gestorfrotas.menuusuario;

import java.util.ArrayList;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;
import tests.util.Variaveis;

public class UsuarioPerfil{
	private static final String senhaTela = "Perfil do Usuario";
	
	private static String nome = "TESTE SHOW";
	private static String celular = "(00)00000-000";
	private static String senha = "1234";
	private static int indexIdioma = 0;
	private static String imagem = "";

	private static WebDriverWait wait = null; //Espera para elementos de outras p�ginas
	
	private static StringBuffer dadosArquivo = null;
	
	public static void clear() {
		nome = "TESTE SHOW";
		celular = "(00)00000-000";
		senha = "1234";
		indexIdioma = 0;
		imagem = "";
	}

	public UsuarioPerfil(WebDriver driver) {
		wait = new WebDriverWait(driver, 15);
		
		StringBuffer errorBuffer = new StringBuffer();
		
		errorBuffer.append("\n"+senhaTela+"\n");
		
		clear();
		
		entrarPerfil(driver);
				
		errorBuffer.append(alterarPerfilDefault(driver)+"\n"); //teste default	
		errorBuffer.append(alterarPerfilNome(driver)+"\n"); //teste alterando nome
		errorBuffer.append(alterarPerfilNomeVazio(driver)+"\n"); //teste alterando nome vazio
		errorBuffer.append(alterarPerfilSenha(driver)+"\n"); //teste alterando senha
		errorBuffer.append(alterarPerfilCelular(driver)+"\n"); //teste alterando celular
		errorBuffer.append(alterarPerfilCelularVazio(driver)+"\n"); //teste alterando celular vazio
		errorBuffer.append(alterarPerfilIdioma(driver)+"\n"); //teste alterando idioma

		System.out.println(errorBuffer);
		
		setDadosArquivo(errorBuffer);

	}
	
	public static boolean entrarPerfil(WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
        
		try {
	        //Abre menu de usuario
	        String xpath = Comandos.menuUsuario(driver)+"ul/li[3]/";
	        
	        //Abre o perfil do usu�rio
	        WebElement cadastro = driver.findElement(By.xpath(xpath+"a"));
	        js.executeScript("arguments[0].scrollIntoView(true);", cadastro);
	        js.executeScript("arguments[0].click();", cadastro);  
	        
	        //Clica na aba dejejada
	        WebElement novo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"container_home\"]/div[2]/ul/li[1]/a")));
	        novo.click();
		}
		catch(Exception e) {
			return false;
		}
        return true;   
	}
	
	public static boolean alterarPerfil(WebDriver driver) {
		boolean tudoCerto = false;
        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
		JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript

        WebElement nomeE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nome")));
        nomeE.clear();
        nomeE.sendKeys(nome);
        
        WebElement celularE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("celular")));
        celularE.clear();
        celularE.sendKeys(celular);    
        
        WebElement baseE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("idioma")));
        if(indexIdioma >= 0) {
		    Select selectBaseE = new Select(baseE);
		    selectBaseE.selectByIndex(indexIdioma);
        }
        
        WebElement senhaE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("senha")));
        senhaE.clear();
        senhaE.sendKeys(senha);
        
        WebElement imagemE = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("imagem")));
        if(!imagem.equals(""))
        	imagemE.sendKeys(imagem);
        
        WebElement botaoSubmit = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSubmit")));
        botaoSubmit.click();
                
        clear();
        while(true) {
		    try {
		    	wait.until(ExpectedConditions.alertIsPresent());
		        Alert alert = driver.switchTo().alert();
		        if(alert.getText().contains("Perfil atualizado com sucesso!") 
		        		|| alert.getText().contains("Profile updated successfully!"))
		        	tudoCerto = true;
		        alert.accept();
		    }
		    catch(NoAlertPresentException | TimeoutException e) {
		    	
		    	e.printStackTrace();
		    	break;
		    }
        }
        return tudoCerto;
	}
	
	public static StringBuffer alterarPerfilDefault(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "alterarPerfilDefault";
		
		
				
		try {
			Assert.assertTrue(alterarPerfil(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "PERFIL N�O ALTERADO MESMO COM DADOS CORRETOS";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));

		}
		
        return errorBuffer;
        
	}
	
	public static StringBuffer alterarPerfilNome(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "alterarPerfilNome";
		
		nome = "MEU AMIGO TED";
		String novoElemento = nome;
	
		try {
			Assert.assertTrue(alterarPerfil(driver));
			entrarPerfil(driver);

			WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("nome")));
			Assert.assertTrue(novoElemento.equals(elemento.getAttribute("value")));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            alterarPerfilDefault(driver);
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "PERFIL N�O ALTERADO NO CAMPO NOME";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		
        return errorBuffer;
        
	}

	public static StringBuffer alterarPerfilNomeVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "alterarPerfilNomeVazio";
		
		nome = "";
				
		try {
			Assert.assertFalse(alterarPerfil(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			alterarPerfilDefault(driver);
			String motivo = "PERFIL � ALTERADO MESMO COM CAMPO NOME VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));

		}
		
        return errorBuffer;
        
	}
	
	public static StringBuffer alterarPerfilSenha(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "alterarPerfilSenha";
		String motivo = "PERFIL N�O ALTERADO NO CAMPO SENHA";

		senha = "123456";
		String novoElemento = senha;
	
		try {
			Assert.assertTrue(alterarPerfil(driver));
			
			Comandos.logout(driver);
			Comandos.login(driver, Variaveis.getDefaultuserfrotas(), novoElemento);
			
			try {
				Assert.assertTrue(entrarPerfil(driver));
			}
			catch(AssertionError e) {
				Comandos.login(driver, Variaveis.getDefaultuserfrotas(), Variaveis.getDefaultpasswordfrotas());
				errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
				return errorBuffer;
			}
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            alterarPerfilDefault(driver);
		}
		catch(AssertionError | TimeoutException e) {
			errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
            
		}
				
        return errorBuffer;
        
	}
	
	public static StringBuffer alterarPerfilCelular(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "alterarPerfilCelular";
		
		
		celular = "(00)99999-000";
		String novoElemento = celular;
	
		try {
			Assert.assertTrue(alterarPerfil(driver));
			entrarPerfil(driver);

			WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("celular")));
			Assert.assertTrue(novoElemento.equals(elemento.getAttribute("value")));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            alterarPerfilDefault(driver);
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "PERFIL N�O ALTERADO NO CAMPO CELULAR";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		
        return errorBuffer;
        
	}
	
	public static StringBuffer alterarPerfilCelularVazio(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "alterarPerfilCelularVazio";
		
		celular = "";
				
		try {
			Assert.assertFalse(alterarPerfil(driver));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
		}
		catch(AssertionError | TimeoutException e) {
			alterarPerfilDefault(driver);
			String motivo = "PERFIL � ALTERADO MESMO COM CAMPO CELULAR VAZIO";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));

		}
		
        return errorBuffer;
        
	}
	
	public static StringBuffer alterarPerfilIdioma(WebDriver driver) {
		StringBuffer errorBuffer = new StringBuffer();
		String teste = "alterarPerfilIdioma";
		
		indexIdioma = 1;
		int novoIdioma = indexIdioma;
			
		try {
			Assert.assertTrue(alterarPerfil(driver));
			entrarPerfil(driver);
			
			WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("idioma")));
			Select select = new Select(elemento);
			String novoElemento = select.getOptions().get(novoIdioma).getText();
			
			Assert.assertTrue(select.getFirstSelectedOption().getText().equals(novoElemento));
			
            errorBuffer.append(Comandos.printTesteSucesso(teste));
            alterarPerfilDefault(driver);
		}
		catch(AssertionError | TimeoutException e) {
			String motivo = "PERFIL N�O ALTERADO NO CAMPO IDIOMA";
            errorBuffer.append(Comandos.printTesteFracasso(teste,motivo));
            e.printStackTrace();
		}
		
        return errorBuffer;
        
	}
		
	public StringBuffer getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuffer dadosArquivo) {
		UsuarioPerfil.dadosArquivo = dadosArquivo;
	}
}
