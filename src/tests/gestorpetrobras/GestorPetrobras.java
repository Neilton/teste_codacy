package tests.gestorpetrobras;

import java.io.FileWriter;
import java.util.ArrayList;

import javax.swing.JFileChooser;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import tests.Facade;
import tests.gestorpetrobras.menuresponsivo.cadastros.Cadastro;
import tests.gestorpetrobras.menuresponsivo.cadastros.PetrobrasCadastroMultas;
import tests.gestorsafety.menuresponsivo.relatorios.Relatorio;
import tests.gestorsafety.menuresponsivo.relatorios.operacional.SafetyViolacaoDeBateria;
import tests.util.Comandos;
import tests.util.Variaveis;

public class GestorPetrobras {
	
	private static boolean flag = true;
	private static GestorPetrobras instance;

	private GestorPetrobras() {
		
	}
	
	public static void testarCadastros(WebDriver driver) {
		// teste cadastros
		ArrayList<Cadastro> cadastros = new ArrayList<>();
		
        Facade.getInstance().setPcm(new PetrobrasCadastroMultas(driver)); //testes de cadastro de multas
        
        // salvando testes em arquivo
        for(Cadastro c: cadastros) {
            salvarCadastrosArquivo(c);
        }
        	
	}
	
	public static void salvarRelatoriosArquivo(Relatorio relatorio) {
		JFileChooser fc = new JFileChooser();
        fc.requestFocus();
        if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
        	try (FileWriter f = new FileWriter(fc.getSelectedFile()+".txt")){
				f.write(relatorio.getDadosArquivo().toString());
			} catch(Exception e) {
				e.printStackTrace();
			}
        }
	}
	
	public static void salvarCadastrosArquivo(Cadastro cadastro) {
		JFileChooser fc = new JFileChooser();
        fc.requestFocus();
        if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
        	try (FileWriter f = new FileWriter(fc.getSelectedFile()+".txt")){
				f.write(cadastro.getDadosArquivo().toString());
			} catch(Exception e) {
				e.printStackTrace();
			}
        }
	}
	
	public static void fecharChatterBot(WebDriver driver){
		new Thread() {
		    @Override
		    public void run() {
			    JavascriptExecutor js = ((JavascriptExecutor) driver);
			    
	    		while(flag){
	    			try {
		    			WebElement chat = driver.findElement((By.xpath("//*[@id=\"intercom-container\"]/div/div[1]/iframe")));
			    		js.executeScript("arguments[0].style='display: none'", chat);
	    			}
			    	catch(Exception e) {
			    		e.printStackTrace();
			    	}
		    		
	    		}
		    	
		    }
		}.start();
	 
	}
		
	public static void testarGestorPetrobras() {
		// inicia as ferramentas utilizadasa
        WebDriver driver = new ChromeDriver(); //Driver do Selenium no Chrome

        // configurações        
        driver.manage().window().maximize(); //maximiza tela
        driver.get(Variaveis.getDefaulturl()); //acessa o site do sistema
        
        // executa comandos no sistema
        Comandos.login(driver,Variaveis.getDefaultUserPetrobras(),Variaveis.getDefaultPasswordPetrobras()); //faz login
        //fecharChatterBot(driver);
        
        // testes
        try {
	        testarCadastros(driver);
	        	        	        
	        Comandos.logout(driver); //faz logout no sistema
	        driver.quit(); //fecha o browser
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
	       
	}

	public static GestorPetrobras getInstance() {
		if(instance == null)
			instance = new GestorPetrobras();
		return instance;
	}

	public static void setInstance(GestorPetrobras instance) {
		GestorPetrobras.instance = instance;
	}
	
}
