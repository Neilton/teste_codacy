package tests.gestorpetrobras.menuresponsivo.cadastros;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.util.Comandos;

public class Cadastro {
	private static WebDriverWait wait;
	private static StringBuilder dadosArquivo = null;

	private static ArrayList<String> motoristas;
	private static ArrayList<String> placas;
	private static String numeroAIT;
	private static String localInfracao;
	private static String dataInfracao;
	private static String baseLegal;
	private static String tipoInfracao;
	private static String pontos;
	private static String numeroAgenteTransito;
	private static String dataVerificacaoEquipamento;
	private static String dataEmissao;
	private static String velocidadePermitida;
	private static String velocidadeAferida;
	private static String idEquipamento;
	private static String enquadramento;
	private static String descricaoInfracao;
	private static String observacoes;

	private static WebElement numeroAITE;
	private static WebElement localInfracaoE;
	private static WebElement dataInfracaoE;
	private static WebElement baseLegalE;
	private static WebElement motoristasE;
	private static WebElement gruposE;
	private static WebElement tipoInfracaoE;
	private static WebElement pontosE;
	private static WebElement numeroAgenteTransitoE;
	private static WebElement dataVerificacaoEquipamentoE;
	private static WebElement dataEmissaoE;
	private static WebElement velocidadePermitidaE;
	private static WebElement velocidadeAferidaE;
	private static WebElement idEquipamentoE;
	private static WebElement enquadramentoE;
	private static WebElement descricaoInfracaoE;
	private static WebElement observacoesE;
	
	private static WebElement botaoSubmit;
	
	private static StringBuilder variaveis;
	private static String alerta;
	private static int count = 0;
	
	@SuppressWarnings("deprecation")
	public static void abrirCadastro(WebDriver driver) {
		setWait(new WebDriverWait(driver, Comandos.getTime())); //Espera para elementos de outras p�ginas
		Comandos.menuResponsivo(driver); //abre menu responsivo

        WebElement relatorios = getWait().until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"sidebar-items\"]/li[4]/a")));
        relatorios.click();
	}
	
	public static void abrirCadastroMultas(WebDriver driver) {
		abrirCadastro(driver);
		WebElement operacional = getWait().until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"sidebar-items\"]/li[4]/ul/li[2]/a")));
		operacional.click();
	}
	
	public static WebDriverWait getWait() {
		return wait;
	}

	public static void setWait(WebDriverWait wait) {
		Cadastro.wait = wait;
	}

	public StringBuilder getDadosArquivo() {
		return dadosArquivo;
	}

	public static void setDadosArquivo(StringBuilder errorBuffer) {
		Cadastro.dadosArquivo = errorBuffer;
	}

	public static List<String> getPlacas() {
		return placas;
	}

	public static void setPlacas(List<String> placas) {
		Cadastro.placas = (ArrayList<String>) placas;
	}

	public static List<String> getMotoristas() {
		return motoristas;
	}

	public static void setMotoristas(List<String> motoristas) {
		Cadastro.motoristas = (ArrayList<String>) motoristas;
	}

	public static String getNumeroAIT() {
		return numeroAIT;
	}

	public static void setNumeroAIT(String numeroAIT) {
		Cadastro.numeroAIT = numeroAIT;
	}

	public static String getLocalInfracao() {
		return localInfracao;
	}

	public static void setLocalInfracao(String localInfracao) {
		Cadastro.localInfracao = localInfracao;
	}

	public static String getBaseLegal() {
		return baseLegal;
	}

	public static void setBaseLegal(String baseLegal) {
		Cadastro.baseLegal = baseLegal;
	}

	public static StringBuilder getVariaveis() {
		return variaveis;
	}

	public static void setVariaveis(StringBuilder variaveis) {
		Cadastro.variaveis = variaveis;
	}

	public static int getCount() {
		return count;
	}

	public static void setCount(int count) {
		Cadastro.count = count;
	}

	public static String getAlerta() {
		return alerta;
	}

	public static void setAlerta(String alerta) {
		Cadastro.alerta = alerta;
	}

	public static String getDataInfracao() {
		return dataInfracao;
	}

	public static void setDataInfracao(String dataInfracao) {
		Cadastro.dataInfracao = dataInfracao;
	}

	public static WebElement getNumeroAITE() {
		return numeroAITE;
	}

	public static void setNumeroAITE(WebElement numeroAITE) {
		Cadastro.numeroAITE = numeroAITE;
	}

	public static WebElement getLocalInfracaoE() {
		return localInfracaoE;
	}

	public static void setLocalInfracaoE(WebElement localInfracaoE) {
		Cadastro.localInfracaoE = localInfracaoE;
	}

	public static WebElement getDataInfracaoE() {
		return dataInfracaoE;
	}

	public static void setDataInfracaoE(WebElement dataInfracaoE) {
		Cadastro.dataInfracaoE = dataInfracaoE;
	}

	public static WebElement getBaseLegalE() {
		return baseLegalE;
	}

	public static void setBaseLegalE(WebElement baseLegalE) {
		Cadastro.baseLegalE = baseLegalE;
	}

	public static WebElement getMotoristasE() {
		return motoristasE;
	}

	public static void setMotoristasE(WebElement motoristasE) {
		Cadastro.motoristasE = motoristasE;
	}

	public static WebElement getPlacasE() {
		return gruposE;
	}

	public static void setPlacasE(WebElement gruposE) {
		Cadastro.gruposE = gruposE;
	}

	public static WebElement getBotaoSubmit() {
		return botaoSubmit;
	}

	public static void setBotaoSubmit(WebElement botaoSubmit) {
		Cadastro.botaoSubmit = botaoSubmit;
	}

	public static String getVelocidadePermitida() {
		return velocidadePermitida;
	}

	public static void setVelocidadePermitida(String velocidadePermitida) {
		Cadastro.velocidadePermitida = velocidadePermitida;
	}

	public static String getTipoInfracao() {
		return tipoInfracao;
	}

	public static void setTipoInfracao(String tipoInfracao) {
		Cadastro.tipoInfracao = tipoInfracao;
	}

	public static String getPontos() {
		return pontos;
	}

	public static void setPontos(String pontos) {
		Cadastro.pontos = pontos;
	}

	public static String getNumeroAgenteTransito() {
		return numeroAgenteTransito;
	}

	public static void setNumeroAgenteTransito(String numeroAgenteTransito) {
		Cadastro.numeroAgenteTransito = numeroAgenteTransito;
	}

	public static String getDataVerificacaoEquipamento() {
		return dataVerificacaoEquipamento;
	}

	public static void setDataVerificacaoEquipamento(String dataVerificacaoEquipamento) {
		Cadastro.dataVerificacaoEquipamento = dataVerificacaoEquipamento;
	}

	public static String getDataEmissao() {
		return dataEmissao;
	}

	public static void setDataEmissao(String dataEmissao) {
		Cadastro.dataEmissao = dataEmissao;
	}

	public static String getVelocidadeAferida() {
		return velocidadeAferida;
	}

	public static void setVelocidadeAferida(String velocidadeAferida) {
		Cadastro.velocidadeAferida = velocidadeAferida;
	}

	public static String getIdEquipamento() {
		return idEquipamento;
	}

	public static void setIdEquipamento(String idEquipamento) {
		Cadastro.idEquipamento = idEquipamento;
	}

	public static String getDescricaoInfracao() {
		return descricaoInfracao;
	}

	public static void setDescricaoInfracao(String descricaoInfracao) {
		Cadastro.descricaoInfracao = descricaoInfracao;
	}

	public static String getEnquadramento() {
		return enquadramento;
	}

	public static void setEnquadramento(String enquadramento) {
		Cadastro.enquadramento = enquadramento;
	}

	public static String getObservacoes() {
		return observacoes;
	}

	public static void setObservacoes(String observacoes) {
		Cadastro.observacoes = observacoes;
	}

	public static WebElement getTipoInfracaoE() {
		return tipoInfracaoE;
	}

	public static void setTipoInfracaoE(WebElement tipoInfracaoE) {
		Cadastro.tipoInfracaoE = tipoInfracaoE;
	}

	public static WebElement getDataVerificacaoEquipamentoE() {
		return dataVerificacaoEquipamentoE;
	}

	public static void setDataVerificacaoEquipamentoE(WebElement dataVerificacaoEquipamentoE) {
		Cadastro.dataVerificacaoEquipamentoE = dataVerificacaoEquipamentoE;
	}

	public static WebElement getNumeroAgenteTransitoE() {
		return numeroAgenteTransitoE;
	}

	public static void setNumeroAgenteTransitoE(WebElement numeroAgenteTransitoE) {
		Cadastro.numeroAgenteTransitoE = numeroAgenteTransitoE;
	}

	public static WebElement getPontosE() {
		return pontosE;
	}

	public static void setPontosE(WebElement pontosE) {
		Cadastro.pontosE = pontosE;
	}

	public static WebElement getVelocidadePermitidaE() {
		return velocidadePermitidaE;
	}

	public static void setVelocidadePermitidaE(WebElement velocidadePermitidaE) {
		Cadastro.velocidadePermitidaE = velocidadePermitidaE;
	}

	public static WebElement getIdEquipamentoE() {
		return idEquipamentoE;
	}

	public static void setIdEquipamentoE(WebElement idEquipamentoE) {
		Cadastro.idEquipamentoE = idEquipamentoE;
	}

	public static WebElement getObservacoesE() {
		return observacoesE;
	}

	public static void setObservacoesE(WebElement observacoesE) {
		Cadastro.observacoesE = observacoesE;
	}

	public static WebElement getDescricaoInfracaoE() {
		return descricaoInfracaoE;
	}

	public static void setDescricaoInfracaoE(WebElement descricaoInfracaoE) {
		Cadastro.descricaoInfracaoE = descricaoInfracaoE;
	}

	public static WebElement getEnquadramentoE() {
		return enquadramentoE;
	}

	public static void setEnquadramentoE(WebElement enquadramentoE) {
		Cadastro.enquadramentoE = enquadramentoE;
	}

	public static WebElement getVelocidadeAferidaE() {
		return velocidadeAferidaE;
	}

	public static void setVelocidadeAferidaE(WebElement velocidadeAferidaE) {
		Cadastro.velocidadeAferidaE = velocidadeAferidaE;
	}

	public static WebElement getDataEmissaoE() {
		return dataEmissaoE;
	}

	public static void setDataEmissaoE(WebElement dataEmissaoE) {
		Cadastro.dataEmissaoE = dataEmissaoE;
	}
	
}
