package tests.gestorpetrobras.menuresponsivo.cadastros;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.gestorsafety.menuresponsivo.relatorios.Relatorio;
import tests.util.Comandos;
import tests.util.Variaveis;

public class PetrobrasCadastroMultas extends Cadastro{
	private static final String TITULO = "Cadastro de Multas";
	private static final String FALHAREQUISICAO = "Falha na requisi��o!";
	private static final Logger LOGGER = Logger.getLogger( PetrobrasCadastroMultas.class.getName() );	
	
	@SuppressWarnings("deprecation")
	public PetrobrasCadastroMultas(WebDriver driver) {
		setWait(new WebDriverWait(driver, Variaveis.getTime()));
		//abre menu de relat�rios operacionais
        abrirCadastroMultas(driver);
		clear();
			
		StringBuilder errorBuffer = new StringBuilder();
		
		errorBuffer.append("\n"+TITULO+"\n\n");
		
		errorBuffer.append(gerarRelatorioPerfeito(driver)+"\n"); //teste perfeito
			
		errorBuffer.append(gerarRelatorioPerfeitoGrupo(driver)+"\n"); //teste perfeito
		
		errorBuffer.append(gerarRelatorioHoraInicialCharInvalidos(driver)+"\n"); //teste com hora inicial com caracteres inv�lidos

		errorBuffer.append(gerarRelatorioHoraFinalCharInvalidos(driver)+"\n"); //teste com hora final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioHoraInicialVazia(driver)+"\n"); //teste com hora inicial vazia
		
		errorBuffer.append(gerarRelatorioHoraFinalVazia(driver)+"\n"); //teste com hora final vazia

		errorBuffer.append(gerarRelatorioHoraFinalMaiorQueLimite(driver)+"\n"); //teste com hora final maior do que deveria
		 
		errorBuffer.append(gerarRelatorioHoraInicialMaiorQueLimite(driver)+"\n"); //teste com hora inicial maior do que um dia
		
		errorBuffer.append(gerarRelatorioDataInicialCharInvalidos(driver)+"\n"); //teste com data inicial com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataFinalCharInvalidos(driver)+"\n"); //teste com data final com caracteres inv�lidos
		
		errorBuffer.append(gerarRelatorioDataInicialVazia(driver)+"\n"); //teste com data incial vazia

		errorBuffer.append(gerarRelatorioDataFinalVazia(driver)+"\n"); //teste com data final vazia

		errorBuffer.append(gerarRelatorioCampoMonitoradosVazio(driver)+"\n"); //teste com campo placas vazio
		        
		errorBuffer.append(gerarRelatorioPeriodoMaiorQueLimite(driver)+"\n"); //teste com diferen�a entre datas maioir que o limite
       
		errorBuffer.append(gerarRelatorioDataFinalMaiorQueAtual(driver)+"\n"); //teste com data final maior do que data atual
		
		errorBuffer.append(gerarRelatorioDataInicialMaiorQueFinal(driver)+"\n"); //teste com data final maior do que data inicial
				
		setDadosArquivo(errorBuffer);

	}
	
	public static void clear() {
		setMonitorados(new ArrayList<>());
		getMonitorados().add(Variaveis.getMonitorado1());
		getMonitorados().add(Variaveis.getMonitorado2());
		setGrupos(new ArrayList<>());
	    setDataFinal(Variaveis.getDatafinal());
	    setDataInicial(Variaveis.getDatainicial());
	    setHoraInicial(Variaveis.getHorainicial());
	    setHoraFinal(Variaveis.getHorafinal());
	    setAlerta(null);
	}
	
	public static void inicializaCampos() {
        setDataInicialE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.id("dataInicial"))));
        setDataFinalE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.id("dataFinal"))));
        setHoraInicialE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.id("horaInicial"))));
        setHoraFinalE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.id("horaFinal"))));
        setMonitoradosE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"selectMonitorado_chosen\"]/ul/li/input"))));
        setGruposE(getWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"selectGrupo_chosen\"]/ul/li/input"))));
        setBotaoSubmit(getWait().until(ExpectedConditions.elementToBeClickable(By.id("buttonGerarRelatorio"))));
	}
	
	// variaveis que ser�o usadas no arquivo resultando dos testes
	public static void setVariaveis() {
		setVariaveis(new StringBuilder());
		for(String m:getMonitorados()) {
			getVariaveis().append("- Monitorado: "+m+"\n");
		}
		for(String g:getGrupos()) {
			getVariaveis().append("- Grupo: "+g+"\n");
		}
		if(getDataInicial() != null)
			getVariaveis().append("- Data Inicial: "+getDataInicial()+"\n");
		getVariaveis().append("- Data Final: "+getDataFinal()+"\n");
		getVariaveis().append("- Hora Inicial: "+getHoraInicial()+"\n");
		getVariaveis().append("- Hora Final: "+getHoraFinal()+"\n");
		
	}
		
	@SuppressWarnings({ "deprecation" })
	public static void gerarRelatorio(WebDriver driver) throws NoAlertPresentException,TimeoutException{
        setWait(new WebDriverWait(driver, Comandos.getTime())); //Espera para elementos de outras p�ginas
                
        // atualiza a p�gina
        driver.navigate().refresh();
        
        // contador de casos te teste
        setCount(getCount() + 1);
                
        //configura todos os campos utiliz�veis
        inicializaCampos();
    	
        getDataInicialE().clear();
        getDataInicialE().sendKeys(getDataInicial());
                       
        getDataFinalE().clear();
        getDataFinalE().sendKeys(getDataFinal());
                       
        getHoraInicialE().clear();
        getHoraInicialE().sendKeys(getHoraInicial());   
                
        getHoraFinalE().clear();
        getHoraFinalE().sendKeys(getHoraFinal());                

        for(String o:getMonitorados()) {
        	try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
        	getMonitoradosE().sendKeys(o,Keys.ENTER);
        }
        
        // procura elemento grupo, retorna erro caso o mesmo esteja desabilitado
        try {
            getGruposE().clear();

		} catch (InvalidElementStateException e) {
			LOGGER.log(Level.WARNING, "O elemento grupo foi desabilitado");
		}
        for(String o:getGrupos()) {
        	try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
        	getGruposE().sendKeys(o,Keys.ENTER);
        }
        
        getBotaoSubmit().click();
        
        setVariaveis();   
        clear();
        
        try {
        	WebDriverWait wait = new WebDriverWait(driver, 10);
        	wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        setAlerta(alert.getText());
	        getVariaveis().append("ALERTA GERADO: "+getAlerta()+"\n");
	        alert.accept();
        	
        }
        catch(NoAlertPresentException | TimeoutException e) {
        	e.printStackTrace();
        }
        

	}
		
	public static StringBuilder gerarRelatorioHoraInicialCharInvalidos(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Hora Inicial com caracteres inv�lidos";
		
        setHoraInicial("abc!");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}

        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioHoraFinalCharInvalidos(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Hora Final com caracteres inv�lidos";
        
        setHoraFinal("abc!");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioHoraInicialVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Hora Inicial vazio";
		        
        setHoraInicial("");
		
        gerarRelatorio(driver);
        
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioHoraFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Hora Final vazio";
		        
        setHoraFinal("");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioHoraFinalMaiorQueLimite(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Hora Final maior que limite";
		        
        setHoraFinal("24:00:01");
		
        gerarRelatorio(driver);
                
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioHoraInicialMaiorQueLimite(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Hora Inicial maior que limite";
		        
        setHoraInicial("50:00:01");
		
        gerarRelatorio(driver);
                        
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioDataInicialCharInvalidos(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Data Inicial com caracteres inv�lidos";
		        
        setDataInicial("abc!");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioDataFinalCharInvalidos(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Data Final com caracteres inv�lidos";
		
        setDataFinal("abc!");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioDataFinalVazia(WebDriver driver) throws NoAlertPresentException{
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Data Final vazio";
		
        setDataFinal("");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioDataInicialVazia(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Data Inicial vazio";
		
        setDataInicial("");
		
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioCampoMonitoradosVazio(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com campo Monitorados e Grupo vazios";
				
        gerarRelatorio(driver);
                       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        return errorBuffer;
	}
	
	public static StringBuilder gerarRelatorioPeriodoMaiorQueLimite(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com per�odo maior que limite";
		
		Calendar calendario = Calendar.getInstance();
		
		try {
			calendario.setTime(Variaveis.getInstance().getFormatodata().parse(Variaveis.getDatainicial()));
			calendario.add(Calendar.DATE, 1);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
        setDataFinal(Variaveis.getInstance().getFormatodata().format(calendario.getTime()));
		
        gerarRelatorio(driver);
       
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        
        return errorBuffer;
	}
	
	
	public static StringBuilder gerarRelatorioDataInicialMaiorQueFinal(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com data inicial maior que data final";
		
		Calendar calendario = Calendar.getInstance();
		
		try {
			calendario.setTime(Variaveis.getInstance().getFormatodata().parse(Variaveis.getDatafinal()));
			calendario.add(Calendar.DATE, 1);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
        setDataInicial(Variaveis.getInstance().getFormatodata().format(calendario.getTime()));
		
        gerarRelatorio(driver);
                
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        
        return errorBuffer;
	}
		
	public static StringBuilder gerarRelatorioDataFinalMaiorQueAtual(WebDriver driver) {
		Calendar calendario = Calendar.getInstance();
		
		calendario.setTime(new Date());
		calendario.add(Calendar.DATE, 1);
				
		String data = Variaveis.getInstance().getFormatodata().format(calendario.getTime());
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com data final maior que data atual";
		
		setDataInicial(data);
        setDataFinal(data);
		
        gerarRelatorio(driver);
        
        if(getAlerta() != null) {
        	if(getAlerta().equals(FALHAREQUISICAO))
                errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),FALHAREQUISICAO.toUpperCase()));
        	else
        		errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
    	}
    	else {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsopositivo()));
    	}
        
        return errorBuffer;
        
	}
	
	public static StringBuilder gerarRelatorioPerfeito(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com dados perfeitos usando monitorados";
		
        gerarRelatorio(driver);
        
        try{ 
            getWait().until(ExpectedConditions.visibilityOf(driver.findElement(By.id("tableRelatorio"))));                
            errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsonegativo()));
        }
        
        return errorBuffer;
        
	}
	
	public static StringBuilder gerarRelatorioPerfeitoGrupo(WebDriver driver) {
		StringBuilder errorBuffer = new StringBuilder();
		String teste = "Relat�rio gerado com dados perfeitos usando grupo";
				
		setMonitorados(new ArrayList<>());
		getGrupos().add(Variaveis.getGrupoSafety());
	
        gerarRelatorio(driver);
        
        try{ 
            getWait().until(ExpectedConditions.visibilityOf(driver.findElement(By.id("tableRelatorio"))));                
            errorBuffer.append(Comandos.printTesteSucesso(getCount(),teste,getVariaveis()));
        }catch(TimeoutException e) {
            errorBuffer.append(Comandos.printTesteFracasso(getCount(),teste,getVariaveis(),Variaveis.getMotivofalsonegativo()));
        }
        
        return errorBuffer;
        
	}
	
}
