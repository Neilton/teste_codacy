package tests.gestorsafety;

import java.io.FileWriter;
import java.util.ArrayList;

import javax.swing.JFileChooser;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.Facade;
import tests.gestorsafety.menuresponsivo.relatorios.Relatorio;
import tests.gestorsafety.menuresponsivo.relatorios.operacional.SafetyViolacaoDeBateria;
import tests.util.Comandos;
import tests.util.Variaveis;

public class GestorSafety {
	
	private static boolean flag = true;
	private static GestorSafety instance;

	private GestorSafety() {
		
	}
	
	public static void testarRelatorios(WebDriver driver) {
		// testes relat�rios operacionais
		ArrayList<Relatorio> relatorios = new ArrayList<>();

		relatorios.add(Facade.getInstance().setSvdb(new SafetyViolacaoDeBateria(driver))); //testes de Viola��o de Bateria
        
		// salvando testes em arquivo
        for(Relatorio r: relatorios) {
            salvarRelatoriosArquivo(r);
        }
        	
	}
	
	public static void salvarRelatoriosArquivo(Relatorio relatorio) {
		JFileChooser fc = new JFileChooser();
        fc.requestFocus();
        if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
        	try (FileWriter f = new FileWriter(fc.getSelectedFile()+".txt")){
				f.write(relatorio.getDadosArquivo().toString());
			} catch(Exception e) {
				e.printStackTrace();
			}
        }
	}
	
	public static void fecharChatterBot(WebDriver driver){
		new Thread() {
		    @Override
		    public void run() {
			    JavascriptExecutor js = ((JavascriptExecutor) driver);
			    
	    		while(flag){
	    			try {
		    			WebElement chat = driver.findElement((By.xpath("//*[@id=\"intercom-container\"]/div/div[1]/iframe")));
			    		js.executeScript("arguments[0].style='display: none'", chat);
	    			}
			    	catch(Exception e) {
			    		e.printStackTrace();
			    	}
		    		
	    		}
		    	
		    }
		}.start();
	 
	}
	
	@SuppressWarnings("deprecation")
	public static void fecharFiltroMonitorados(WebDriver driver){
		try {
	        WebDriverWait wait = new WebDriverWait(driver, Comandos.getTime()); //Espera para elementos de outras p�ginas
			WebElement campoFechar = wait.until(ExpectedConditions.elementToBeClickable(By.id("buttonFecharModalFiltrarMonitoradosUpdate")));
			campoFechar.click();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	 
	}
	
	public static void testarGestorSafety() {
		// inicia as ferramentas utilizadasa
        WebDriver driver = new ChromeDriver(); //Driver do Selenium no Chrome

        // configura��es        
        driver.manage().window().maximize(); //maximiza tela
        driver.get(Variaveis.getDefaulturl()); //acessa o site do sistema
        
        // executa comandos no sistema
        Comandos.login(driver,Variaveis.getDefaultusersafety(),Variaveis.getDefaultpasswordsafety()); //faz login
        fecharFiltroMonitorados(driver);
        
        // testes
        try {
	        testarRelatorios(driver);
	        	        	        
	        Comandos.logout(driver); //faz logout no sistema
	        driver.quit(); //fecha o browser
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
	       
	}

	public static GestorSafety getInstance() {
		if(instance == null)
			instance = new GestorSafety();
		return instance;
	}

	public static void setInstance(GestorSafety instance) {
		GestorSafety.instance = instance;
	}
	
}
