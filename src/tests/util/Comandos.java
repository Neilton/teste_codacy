package tests.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Comandos {
	
	private static final String CLICARARGUMENTOZERO = "arguments[0].click();";
	private static final String OPTION = "option ";
	private static final String NOTFOUND = " not found";
	private static final Logger LOGGER = Logger.getLogger( Comandos.class.getName() );

	private Comandos() {
		
	}
	
	public static int getTime() {
		return Variaveis.getTime();
	}
	
	@SuppressWarnings("deprecation")
	public static void login(WebDriver driver,String user,String password) {
		try {
			WebElement campoLogin = (new WebDriverWait(driver, Variaveis.getTime()))
					.until(ExpectedConditions.elementToBeClickable(By.id("exampleInputEmail1")));
	        campoLogin.sendKeys(user);
	
	        WebElement campoSenha = driver.findElement(By.id("exampleInputPassword1"));
	        campoSenha.sendKeys(password);
	        
	        WebElement campoEntrar = driver.findElement(By.id("entrar"));
	        campoEntrar.click();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void logout(WebDriver driver) {
		try {
			WebElement myDynamicElement = (new WebDriverWait(driver, Variaveis.getTime()))
	        		.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"navbar-collapse\"]/ul/li[5]/a")));
	        myDynamicElement.click();
	                
	        WebElement campoSair = driver.findElement(By.xpath("//*[@id=\"navbar-collapse\"]/ul/li[5]/ul/li[8]/a"));
	        campoSair.click();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void begin(WebDriver driver) {
		try {
			WebElement myDynamicElement = (new WebDriverWait(driver, getTime()))
	        		.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[4]/nav/div/div[1]/a[3]")));
	        myDynamicElement.click();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void scrollIntoView(WebDriver driver,WebElement ele) {
	    ((JavascriptExecutor)driver).executeScript(String.format("window.scrollTo(%s,%s)", ele.getLocation().x, ele.getLocation().y));
	}
	
	@SuppressWarnings("deprecation")
	public static void menuResponsivo(WebDriver driver) {
		try {
		    WebDriverWait wait = new WebDriverWait(driver, getTime());
		    JavascriptExecutor js = ((JavascriptExecutor) driver);
		
			WebElement menu = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("toggle-navbar")));
			
			js.executeScript("arguments[0].scrollIntoView(true);", menu);
			js.executeScript(CLICARARGUMENTOZERO, menu);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	public static String menuUsuario(WebDriver driver) {
		String xpath = "//*[@id=\"navbar-collapse\"]/ul/li[5]/";
		try {
		    WebDriverWait wait = new WebDriverWait(driver, getTime());
		    JavascriptExecutor js = ((JavascriptExecutor) driver);
		    		    
			WebElement menu = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath+"a")));
			
			js.executeScript("arguments[0].scrollIntoView(true);", menu);
			js.executeScript(CLICARARGUMENTOZERO, menu);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return xpath;
	}
	
	public static StringBuilder printTesteFracasso(int count,String teste,StringBuilder variaveis, String motivo) {
		StringBuilder sb = new StringBuilder();
		String casoTeste = "Caso de Teste "+count+": "+teste+"\n"
				+"Vari�veis utilizadas:\n"
				+variaveis
				+"Descri��o do erro:\n"
				+ "- "+motivo+"\n"
				+"Parecer:\n"
				+ "- REPROVADO\n";
		LOGGER.log(Level.INFO,casoTeste);
		sb.append(casoTeste);
		if(teste != null)
			return sb;
		return null;
	}
	
	public static StringBuilder printTesteSucesso(int count,String teste,StringBuilder stringBuilder) {
		StringBuilder sb = new StringBuilder();
		String casoTeste = "Caso de Teste "+count+": "+teste+"\n"
		+"Vari�veis utilizadas:\n"
		+stringBuilder
		+"Parecer:\n"
		+ "- APROVADO\n";
		
		LOGGER.log(Level.INFO,casoTeste);
		sb.append(casoTeste);
		
		if(teste != null)
			return sb;
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public static void clicarCheckboxNome(WebDriver driver,String nome, boolean option) {
		try {
		    WebDriverWait wait = new WebDriverWait(driver, getTime());
	        WebElement checkbox = wait.until(ExpectedConditions.elementToBeClickable(By.name(nome)));
	        if((checkbox.isSelected() && !option) || (!checkbox.isSelected() && option)) 
	        	checkbox.click();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
    }
	
	@SuppressWarnings("deprecation")
	public static void clicarCheckboxId(WebDriver driver,WebElement checkbox, boolean option) {
		try {
	        if((checkbox.isSelected() && !option) || (!checkbox.isSelected() && option)) 
	        	checkbox.click();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
    }
	
	@SuppressWarnings("deprecation")
	public static boolean testarCheckboxNome(WebDriver driver,String nome) {
		try {
		    WebDriverWait wait = new WebDriverWait(driver, getTime());
	        WebElement checkbox = wait.until(ExpectedConditions.elementToBeClickable(By.name(nome)));
	        return checkbox.isSelected();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return false;
    }
	
	@SuppressWarnings("deprecation")
	public static void escolherRadioButton(WebDriver driver,String id, int option) {
		try {
		    WebDriverWait wait = new WebDriverWait(driver, getTime());
			JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
	        List<WebElement> radios = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id(id)));
	        if (option > 0 && option <= radios.size()) {
	        	js.executeScript(CLICARARGUMENTOZERO, radios.get(option-1));
	            //radios.get(option - 1).click();
	        } else {
	            throw new NotFoundException(OPTION + option + NOTFOUND);
	        }
		}
		catch(Exception e) {
			e.printStackTrace();
		}
    }
	
	@SuppressWarnings("deprecation")
	public static void escolherRadioButtonNome(WebDriver driver,String name, int option) {
		try {
		    WebDriverWait wait = new WebDriverWait(driver, getTime());
			JavascriptExecutor js = ((JavascriptExecutor) driver); //Executor de Javascript
	        List<WebElement> radios = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name(name)));
	        if (option > 0 && option <= radios.size()) {
	        	js.executeScript(CLICARARGUMENTOZERO, radios.get(option-1));
	            //radios.get(option - 1).click();
	        } else {
	            throw new NotFoundException(OPTION + option + NOTFOUND);
	        }
		}
		catch(Exception e) {
			e.printStackTrace();
		}
    }
	
	@SuppressWarnings("deprecation")
	public static void escolherRadioButtonXpath(WebDriver driver,String xpath, int option) {
		try {
		    WebDriverWait wait = new WebDriverWait(driver, getTime());
	        List<WebElement> radios = new ArrayList<>();
	        int i = 1;
	        while(true) {
	        	try {
		        	radios.add(wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath+"/label["+i+"]/input"))));
		        	i++;
	        	}
	        	catch(Exception e) {
	        		e.printStackTrace();
		        	break;
	        	}
	        }
	        if (option > 0 && option <= radios.size()) {
	            radios.get(option - 1).click();
	        } else {
	            throw new NotFoundException(OPTION + option + NOTFOUND);
	        }
		}
		catch(Exception e) {
			e.printStackTrace();
		}
    }
	
@SuppressWarnings("deprecation")
public static List<String> listarListaElementosXpath(WebDriver driver,String xpath) {
		
	    WebDriverWait wait = new WebDriverWait(driver, getTime());
        List<String> strings = new ArrayList<>();
        int i = 1;
        while(true) {
        	try {
	        	strings.add(wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath+"/li["+i+"]/span"))).getText());
	        	i++;
        	}
        	catch(Exception e) {
        		e.printStackTrace();
	        	break;
        	}
        }
		return strings;
    }
	
	@SuppressWarnings("deprecation")
	public static boolean compararListaElementosXpath(WebDriver driver,String xpath,List<String> lista) {
		
	    WebDriverWait wait = new WebDriverWait(driver, getTime());
        List<String> strings = new ArrayList<>();
        int i = 1;
        while(true) {
        	try {
	        	strings.add(wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath+"/li["+i+"]/span"))).getText());
	        	i++;
        	}
        	catch(Exception e) {
        		e.printStackTrace();
	        	break;
        	}
        }
        for(int j = 0;j < lista.size(); j++) {
        	if(!lista.get(j).equals(strings.get(j))) 
        		return false;
        }
		return true;
    }
	
	@SuppressWarnings("deprecation")
	public static boolean excluirListaElementosXpath(WebDriver driver,String xpath) {
		
	    WebDriverWait wait = new WebDriverWait(driver, getTime());
        int i = 1;
        while(true) {
        	try {
	        	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath+"/li["+i+"]/a"))).click();
	        	i++;
        	}
        	catch(Exception e) {
        		e.printStackTrace();
	        	break;
        	}
        }
		return true;
    }
	
	@SuppressWarnings("deprecation")
	public static void elementoTipoChosenSearch(WebDriver driver, List<String> elementos, String xpath) {
		try {
		    WebDriverWait wait = new WebDriverWait(driver, getTime());
			WebElement placasE = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		    placasE.clear();
		    for(String o:elementos) {
		    	dormir();
		    	placasE.sendKeys(o,Keys.ENTER);
		    }
		}
        catch(Exception e) {
        	e.printStackTrace();
        }
	}
	
	public static void dormir() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
		}
	}

}


