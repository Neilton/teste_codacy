package tests.util;

import java.text.SimpleDateFormat;

public class Variaveis {

	private static final int TIME = 91;
	private static final String DEFAULT_USER_FROTAS = "saulomendes25@hotmail.com";
	private static final String DEFAULT_PASSWORD_FROTAS = "1234";	
	private static final String DEFAULT_USER_PETROBRAS = "master@petrobras.com.br";
	private static final String DEFAULT_PASSWORD_PETROBRAS = "1234";	
	private static final String DEFAULT_USER_SAFETY = "atualizacaosistema@showtecnologia.com";
	private static final String DEFAULT_PASSWORD_SAFETY = "123456";
	private static final String DEFAULT_URL = "http://3.15.117.58:8084/gestor/index.php";
	
	private static final String PLACA1 = "PJS-2059";
	private static final String PLACA2 = "OKZ-4198";
	private static final String MOTIVO_FALSO_POSITIVO = "ALERTA N�O FOI GERADO OU DATATABLE FOI GERADA";
	private static final String MOTIVO_FALSO_NEGATIVO = "ALERTA FOI GERADO OU DATATABLE N�O FOI GERADA";
	private static final String GRUPO_FROTAS = "SAEB";
	private static final String GRUPO_SAFETY = "PALMAS";
	private static final String MONITORADO1 = "EDILENE";
	private static final String MONITORADO2 = "LETICIA";
	private static final String TORNOZELEIRA = "W10205986";
	private static final String DATA_FINAL = "03/02/2021";
	private static final String DATA_INICIAL = "03/02/2021";
	private static final String HORA_INICIAL = "00:00:01";
	private static final String HORA_FINAL = "23:59:59";
	
	private final SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");

	private static Variaveis instance;
	
	private Variaveis() {
		
	}
	
	public static int getTime() {
		return TIME;
	}
	public static String getDefaultuserfrotas() {
		return DEFAULT_USER_FROTAS;
	}
	public static String getDefaultpasswordfrotas() {
		return DEFAULT_PASSWORD_FROTAS;
	}
	public SimpleDateFormat getFormatodata() {
		return formatoData;
	}
	public static String getDefaulturl() {
		return DEFAULT_URL;
	}
	public static String getPlaca1() {
		return PLACA1;
	}
	public static String getPlaca2() {
		return PLACA2;
	}
	public static String getHorainicial() {
		return HORA_INICIAL;
	}
	public static String getDatafinal() {
		return DATA_FINAL;
	}
	public static String getDatainicial() {
		return DATA_INICIAL;
	}
	public static String getHorafinal() {
		return HORA_FINAL;
	}
	public static String getGrupoSafety() {
		return GRUPO_SAFETY;
	}
	public static String getMonitorado1() {
		return MONITORADO1;
	}
	public static String getTornozeleira() {
		return TORNOZELEIRA;
	}
	public static String getDefaultusersafety() {
		return DEFAULT_USER_SAFETY;
	}
	public static String getDefaultpasswordsafety() {
		return DEFAULT_PASSWORD_SAFETY;
	}
	public static String getMonitorado2() {
		return MONITORADO2;
	}
	public static String getMotivofalsopositivo() {
		return MOTIVO_FALSO_POSITIVO;
	}
	public static String getMotivofalsonegativo() {
		return MOTIVO_FALSO_NEGATIVO;
	}

	public static Variaveis getInstance() {
		if(instance == null)
			instance = new Variaveis();
		return instance;
	}

	public static void setInstance(Variaveis instance) {
		Variaveis.instance = instance;
	}

	public static String getDefaultUserPetrobras() {
		return DEFAULT_USER_PETROBRAS;
	}

	public static String getDefaultPasswordPetrobras() {
		return DEFAULT_PASSWORD_PETROBRAS;
	}

	public static String getGrupoFrotas() {
		return GRUPO_FROTAS;
	}
}
